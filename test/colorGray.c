// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/color.h>
#include<stddef.h>
#include"test.h"

void testColorGray(void){
	testStart("color gray");

	if(lexlibColorGray((struct LexlibColor){0xFF, 0x00, 0x00, 0xFF}) != 0x4C){ testEnd("1 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0x00, 0xFF, 0x00, 0xFF}) != 0x96){ testEnd("2 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0x00, 0x00, 0xFF, 0xFF}) != 0x1D){ testEnd("3 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0xFF, 0xFF, 0x00, 0xFF}) != 0xE2){ testEnd("4 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0xFF, 0x00, 0xFF, 0xFF}) != 0x69){ testEnd("5 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0x00, 0xFF, 0xFF, 0xFF}) != 0xB3){ testEnd("6 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0xFF, 0xFF, 0xFF, 0xCC}) != 0xCC){ testEnd("7 color to gray wrong"); return; }
	if(lexlibColorGray((struct LexlibColor){0x36, 0x00, 0xB2, 0xFF}) != 0x24){ testEnd("8 color to gray wrong"); return; }

	if(lexlibColor16Gray((struct LexlibColor16){0xFFFF, 0x0000, 0x0000, 0xFFFF}) != 0x4C8B){ testEnd("1 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0x0000, 0xFFFF, 0x0000, 0xFFFF}) != 0x9645){ testEnd("2 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0x0000, 0x0000, 0xFFFF, 0xFFFF}) != 0x1D2F){ testEnd("3 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0xFFFF, 0xFFFF, 0x0000, 0xFFFF}) != 0xE2D0){ testEnd("4 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0xFFFF, 0x0000, 0xFFFF, 0xFFFF}) != 0x69BA){ testEnd("5 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0x0000, 0xFFFF, 0xFFFF, 0xFFFF}) != 0xB374){ testEnd("6 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0xFFFF, 0xFFFF, 0xFFFF, 0xCCCC}) != 0xCCCC){ testEnd("7 color16 to gray wrong"); return; }
	if(lexlibColor16Gray((struct LexlibColor16){0x2E21, 0x1900, 0xB236, 0xFFFF}) != 0x30C9){ testEnd("8 color16 to gray wrong"); return; }

	if(lexlibColorFGray((struct LexlibColorF){1.0f, 0.0f, 0.0f, 1.0f}) != 0.299f){ testEnd("1 colorflt to gray wrong"); return; }
	if(lexlibColorFGray((struct LexlibColorF){0.0f, 1.0f, 0.0f, 1.0f}) != 0.587f){ testEnd("2 colorflt to gray wrong"); return; }
	if(lexlibColorFGray((struct LexlibColorF){0.0f, 0.0f, 1.0f, 1.0f}) != 0.114f){ testEnd("3 colorflt to gray wrong"); return; }
	if(lexlibColorFGray((struct LexlibColorF){0.18f, 0.142, 0.97, 1.0f}) != 0.2477540075778961181640625f){ testEnd("4 colorflt to gray wrong"); return; }

	testEnd(NULL);
}
