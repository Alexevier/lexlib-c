// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#define _POSIX_C_SOURCE 199309L
#include<lexlib/lexlib.h>
#include<inttypes.h>
#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include"test.h"

void testStart(const char *label){
	if(!label){
		printf("[error]: test without label\n");
		exit(3);
	}

	DATA.test.count++;

#ifdef __unix__
	printf("\t[\x1B[1;32mtest\x1B[0m]: %s, ", label);
#else
	printf("\t[test]: %s, ", label);
#endif
	fflush(stdout);
}

void testEnd(const char *err){
	if(err){
		DATA.test.fail++;
#	ifdef __unix__
		printf("\x1B[31mfailed\x1B[0m, %s.\n", err);
#	else
		printf("failed, %s.\n", err);
#	endif
		return;
	}

	DATA.test.success++;
#ifdef __unix__
	printf("\x1B[32msuccess\x1B[0m.\n");
#else
	printf("success.\n");
#endif
}

void benchmarkStart(const char *label){
	#ifdef __unix__
		printf("\t[\x1B[1;32mbenchmark\x1B[0m]: %s, ", label);
	#else
		printf("\t[benchmark]: %s, ", label);
	#endif
	fflush(stdout);

	DATA.benchmark.start = lexlibThrdNanos();
}

void benchmarkEnd(void){
	uint64_t time = (lexlibThrdNanos() - DATA.benchmark.start) / 1000;

#ifdef __unix__
	printf("%"PRIu64"\x1B[94mus\x1B[0m\n", time);
#else
	printf("%"PRIu64"us\n", time);
#endif
}

void printInfo(void){
#ifdef __unix__
	printf("[\x1B[1;93mrunning %s lexlib %s tests\x1B[0m]\n", DATA.os, lexlibVersion());
	#if LEXLIB_SIMD
		printf("[\x1B[1;32msimd\x1B[0m]: ");
	#else
		printf("[\x1B[1;31msimd\x1B[0m]\n");
	#endif
#else
	printf("[running %s lexlib tests %s]\n", DATA.os, lexlibVersion());
	printf("[simd]: ");
#endif
#if LEXLIB_SIMD
	#ifdef LEXLIB_MMX
		printf("MMX ");
	#endif
	#ifdef LEXLIB_SSE
		printf("SSE ");
	#endif
	#ifdef LEXLIB_SSE2
		printf("SSE2 ");
	#endif
	printf("| ");
	#if LEXLIB_SIMD_MATH
		printf("math");
	#endif
	printf("\n");
#endif

	struct timespec time;
	clock_getres(CLOCK_THREAD_CPUTIME_ID, &time);
	uint64_t res = ((uint64_t)time.tv_sec) * 1000000000 + ((uint64_t)time.tv_nsec);

#ifdef __unix__
	printf("[\x1B[1;34mclockres\x1B[0m]: %"PRIu64"\x1B[94mns\x1B[0m\n", res);
#else
	printf("[clockres]: %"PRIu64"ns\n", res);
#endif
}

struct Data DATA = {
#ifdef __linux__
	.os = "linux",
#elif __ANDROID__
	.os = "android",
#elif __unix__
	.os = "unix",
#elif _WIN64
	.os = "windows64",
#elif _WIN32
	.os = "windows32",
#else
	.os = "unknown",
#endif
};
