// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/cfile.h>
#include<lexlib/mem.h>
#include<stddef.h>
#include<stdint.h>
#include<stdio.h>
#include"test.h"

void testCFile(void){
	testStart("cfile");

	FILE *file = fopen("resources/icon.png", "rb");
	if(!file){
		testEnd("failed to open: resources/icon.png");
		return;
	}

	if(lexlibCFileSize(file) != 7207){
		testEnd("wrong file size");
		fclose(file);
		return;
	}

	if(lexlibCFileType(file) != LEXLIB_FILETYPE_PNG){
		testEnd("wrong filetype");
		fclose(file);
		return;
	}

	if(lexlibCFileMode(file) != LEXLIB_RD){
		testEnd("wrong filemode");
		fclose(file);
		return;
	}

	uint8_t *mem = lexlibCFileMap(file);
	if(!mem){
		testEnd("failed to map memory");
		fclose(file);
		return;
	}

	if(mem[1] != 0x50){
		testEnd("mem[1] is not 0x50");
		lexlibMemUnmap(mem, lexlibCFileSize(file));
		fclose(file);
		return;
	}

	lexlibMemUnmap(mem, lexlibCFileSize(file));
	fclose(file);
	testEnd(NULL);
}
