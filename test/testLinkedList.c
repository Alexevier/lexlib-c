// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<main.h>
#include<lexlib/linkedList.h>

void testLinkedList(void){
	testStart("linked list");
	struct LexlibLinkedList list = lexlibLinkedListNew();
	unsigned char err = 1;
	const char txt[] = "8bits > 16bits.";
	const char txt2[] = "16bits > 32bits.";
	char* txtm = calloc(64,sizeof(char));
	
	strcpy(txtm, "32bits > 64bits.");
	lexlibLinkedListAddCopy(&list, (void*)txt, strlen(txt)+1);
	lexlibLinkedListAddCopy(&list, (void*)txt2, strlen(txt2)+1);
	lexlibLinkedListAddCopy(&list, (void*)txtm, strlen(txtm)+1);
	free(txtm);
	if(strcmp(lexlibLinkedListGet(&list, 0), txt) != 0)
		err = 0;
	if(strcmp(lexlibLinkedListGet(&list, 1), txt2) != 0)
		err = 0;
	if(strcmp(lexlibLinkedListGet(&list, 2), "32bits > 64bits.") != 0)
		err = 0;
	lexlibLinkedListFree(&list);
	
	testEnd(err, NULL);
}
