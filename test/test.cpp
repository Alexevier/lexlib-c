// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#define LEXLIB_EXPERIMENTAL
#include<string.h>
#include<iostream>
#include<lexlib/os.h>
#include<lexlib/io.h>
#include<lexlib/lexlib.h>

typedef unsigned int uint;

static void printInfo();

namespace test {
	void msg(const char* msg);
	void success();
	void failed(const char* msg);
	void undefined();
	int end();
}

int main(void){
	printInfo();
	
	{test::msg("fileToString");
		const char* text = "The quick brown fox jumps over the lazy dog.\nThe quick brown fox jumps over the lazy dog.";
		char* textFile = lexlibFileToString("resources/textFile.txt");
		if(!textFile) test::failed("failed to open \"resources/textFile.txt\"");
		else {
			if(!strcmp(textFile, text)) test::success();
			else test::failed("text comparision failed");
			free(textFile);
		}
	}
	
	{test::msg("strcat");
		const char str1[] = "The quick brown fox";
		const char str2[] = " jumps over the lazy dog.";
		const char strt[] = "The quick brown fox jumps over the lazy dog.";
		char* str = lexStrcat(str1, str2);
		if(strcmp(str, strt) == 0){
			test::success();
			free(str);
		} else
			test::failed(NULL);
	}
	
	{test::msg("linked list");
		struct LexlibLinkedList list = lexlibLinkedListNew();
		bool error = 0;
		const char txt[] = "8bits > 16bits.";
		const char txt2[] = "16bits > 32bits.";
		char* txtm = (char*)calloc(64,sizeof(char));
		strcpy(txtm, "32bits > 64bits.");
		lexlibLinkedListAddCopy(&list, (void*)txt, strlen(txt)+1);
		lexlibLinkedListAddCopy(&list, (void*)txt2, strlen(txt2)+1);
		lexlibLinkedListAddCopy(&list, (void*)txtm, strlen(txtm)+1);
		free(txtm);
		if(strcmp((char*)lexlibLinkedListGet(&list, 0), txt) != 0)
			error = 1;
		if(strcmp((char*)lexlibLinkedListGet(&list, 1), txt2) != 0)
			error = 1;
		if(strcmp((char*)lexlibLinkedListGet(&list, 2), "32bits > 64bits.") != 0)
			error = 1;
		lexlibLinkedListFree(&list);
		if(error)
			test::failed(NULL);
		else
			test::success();
	}
	
	{test::msg("stPath");
	#ifdef _WIN32
		char patht[] = "yes\\ i exist\\path\\sub\\dir\\directowy\\protected\\important thing.txt";
	#else
		char patht[] = "yes/ i exist/path/sub/dir/directowy/protected/important thing.txt";
	#endif
		char* path = lexStrPathNew("yes/ i exist");
		lexStrPathPush(&path, "path");
		lexStrPathPush(&path, "sub/");
		lexStrPathPush(&path, "/dir");
		lexStrPathPush(&path, "/directowy/");
		lexStrPathPush(&path, "protected");
		lexStrPathAsDir(&path);
		lexStrPathPush(&path, "important thing.txt/");
		lexStrPathAsFile(&path);
		
		if(strcmp(path, patht))
			test::failed(NULL);
		else
			test::success();
		
		free(path);
	}
	
	return test::end();
}

static void printInfo(){
	#ifdef __unix__
	std::cout << "[\e[1;33mrunning C++ unix Tests\e[0m]\n";
	#elif _WIN64
	std::cout << "[running C++ windows64 tests]\n";
	#elif _WIN32
	std::cout << "[running C++ windows32 tests]\n";
	#else
	std::cout << "[running C++ _unknown platform_ tests]\n";
	#endif
	
	std::cout << "[lexlib version]: " << lexlibVersion() << '\n';
	std::cout << "[NOTE]: this are not all the tests, just a few.\n";
}

namespace test {
	uint count = 0;
	uint countSuccess = 0;
	uint countFailed = 0;

	void msg(const char* msg){
		#ifdef __unix__
		std::cout << "\t\e[0;32mtest\e[0m " << msg << "\e[1m:\e[0m ";
		#else 
		std::cout << "\ttest " << msg << ": ";
		#endif
		count++;
	}

	void success(){
		#ifdef __unix__
		std::cout << "\e[0;32msucceded\e[0m.\n";
		#else
		std::cout << "succeded.\n";
		#endif
		countSuccess++;
	}

	void failed(const char* msg){
		std::cout << "failed";
		if(msg)
			std::cout << ", " << msg;
		std::cout << ".\n";
		countFailed++;
	}

	void undefined(){
		std::cout << "undefined.\n";
		countFailed++;
	}

	int end(){
		uint total = countSuccess + countFailed;
		
		if(count != total){
			std::cout << "test results are mis-aligned, there are " << count << " testMsg and " << total << " total tests.\n";
			return -1;
		}
		
		if(countFailed){
			std::cout << countFailed << " from " << count << " tests failed\n";
			return countFailed;
		}
		
		std::cout << "All tests succeded.\n";
		return 0;
	}
}

// LEXLIB_LINKED_LIST_IMPL(int, int)
// LEXLIB_LINKED_LIST_IMPL(float, float)
