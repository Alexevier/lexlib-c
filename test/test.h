// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef test_h
#define test_h

#include<lexlib/common.h>
#include<stdint.h>

LEXLIBFN void testStart(const char *label);
LEXLIBFN void testEnd(const char *err);

LEXLIBFN void benchmarkStart(const char *label);
LEXLIBFN void benchmarkEnd(void);

LEXLIBFN void printInfo(void);

void testCFile(void);
void testColorBlend(void);
void testColorGray(void);
void testImage(void);
void testImageBmp(void);
void testImagePng(void);
void testIO(void);
void testMem(void);
void testPath(void);
void testStr(void);
void testString(void);
void testVec(void);
void testVec4(void);

#define LOOP 10240000 /* ten million two hundred forty thousand */

struct Data {
	const char *os;
	uint64_t start;
	struct Test {
		uint32_t count;
		uint32_t fail;
		uint32_t success;
	} test;
	struct Benchmark {
		uint64_t start;
		uint8_t yes;
	} benchmark;
};

LEXLIBFN struct Data DATA;

#endif
