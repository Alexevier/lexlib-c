/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexlib/mem.h>
#include"test.h"

void testMem(void){
	testStart("mem");
	const char* error = NULL;

	uint16_t val16 = 0xF35B;
	uint16_t new16 = lexlibMemReverse16(val16);
	if(new16 != 0x5BF3){
		error = "reverse 16";
		goto end;
	}

	uint32_t val32 = 0xC0FFEE11;
	uint32_t new32 = lexlibMemReverse32(val32);
	if(new32 != 0x11EEFFC0){
		error = "reverse 32";
		goto end;
	}

	uint64_t val64 = 0x1122AABBCCDDEEFF;
	uint64_t new64 = lexlibMemReverse64(val64);
	if(new64 != 0xFFEEDDCCBBAA2211){
		error = "reverse 64";
		goto end;
	}

	{
		uint8_t mem1[] = {0x11, 0x22, 0x33, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00};
		uint8_t mem2[] = {0x11, 0x22, 0x33, 0xAA, 0xBB, 0xCC, 0xDD, 0xEE, 0xFF, 0x00};

		if(!lexlibMemCompare(mem1, mem2, sizeof(mem1))){
			error = "mem1 != mem2";
			goto end;
		}
	}


	end:
	testEnd(error);
}
