// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<main.h>
#include<lexlib/color.h>
#include<lexlib/os.h>

void benchmarkColorBlend(void){
	{benchmarkMessage("color blend (add)");
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColor colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorBlend(color, colori, LEXLIB_ADD);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("color blend (sub)");
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColor colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorBlend(color, colori, LEXLIB_SUB);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("color blend (mul)");
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColor colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorBlend(color, colori, LEXLIB_MUL);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("color blend (mod)");
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColor colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorBlend(color, colori, LEXLIB_MOD);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("color blend (bld)");
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColor colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorBlend(color, colori, LEXLIB_BLEND);
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("colorflt blend (add)");
		LexlibColorFlt color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColorFlt colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorFltBlend(color, colori, LEXLIB_ADD);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("colorflt blend (sub)");
		LexlibColorFlt color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColorFlt colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorFltBlend(color, colori, LEXLIB_SUB);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("colorflt blend (mod)");
		LexlibColorFlt color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColorFlt colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorFltBlend(color, colori, LEXLIB_MOD);
		}
		benchmarkResult(benchmarkEnd());
	}
	{benchmarkMessage("colorflt blend (bld)");
		LexlibColorFlt color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		LexlibColorFlt colori = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; i++){
			color = lexlibColorFltBlend(color, colori, LEXLIB_BLEND);
		}
		benchmarkResult(benchmarkEnd());
	}
}
