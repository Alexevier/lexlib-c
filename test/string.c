/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include"test.h"
#include<lexlib/string.h>
#include<stddef.h>
#include<string.h>

#define ERROR(E) do {error = E; goto end;} while(0)

static const char strImnew[] = "i am a new string ❤ 𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭";
static const char strImnewDuped[] = "i am a new string ❤ 𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭i am a new string ❤ 𝗔𝗕𝗖𝗗𝗘𝗙𝗚𝗛𝗜𝗝𝗞𝗟𝗠𝗡𝗢𝗣𝗤𝗥𝗦𝗧𝗨𝗩𝗪𝗫𝗬𝗭";

void testString(void){
	testStart("string");

	const char *error = NULL;
	LexlibString string = NULL;
	LexlibString string2 = NULL;
	LexlibString stringCopy = NULL;
	LexlibString stringVec = NULL;
	LexlibString stringAppend = NULL;

	string = lexlibStringNew(strImnew);
	if(!string)
		ERROR("new");

	string2 = lexlibStringFromC(strImnew);
	if(!string2)
		ERROR("string from c");

	LexlibVec(char) vec = lexlibVecNew(0, *vec);
	for(size_t i = 0; i < strlen(strImnew); i++)
		lexlibVecPush(vec, &strImnew[i]);
	stringVec = lexlibStringFromVec(vec);
	lexlibVecDelete(vec);
	if(!stringVec)
		ERROR("string from vec");

	stringCopy = lexlibStringCopy(string);
	if(!stringCopy)
		ERROR("string copy");

	if(!lexlibStringValid(string))
		ERROR("invalid string");

	if(!lexlibStringCompare(string, string2))
		ERROR("compare");

	if(!lexlibStringCompare(string, stringCopy))
		ERROR("compare copy");

	if(!lexlibStringCompareStr(string, strImnew))
		ERROR("compare str");

	if(!lexlibStringCompareStr(string, lexlibStringRaw(string2)))
		ERROR("compare string raw");

	if(!lexlibStringCompare(string, stringVec))
		ERROR("compare string vec");

	if(lexlibStringLen(string) != 46)
		ERROR("len != 46");

	if(lexlibStringSize(string) != 127)
		ERROR("size != 127");

	if(lexlibStringChar(string, 0) != lexlibCharNew("i"))
		ERROR("char[0]");
	if(lexlibStringChar(string, 4) != lexlibCharNew(" "))
		ERROR("char[4]");
	if(lexlibStringChar(string, 8) != lexlibCharNew("e"))
		ERROR("char[8]");
	if(lexlibStringChar(string, 9) != lexlibCharNew("w"))
		ERROR("char[9]");
	if(lexlibStringChar(string, 16) != lexlibCharNew("g"))
		ERROR("char[16]");
	if(lexlibStringChar(string, 18) != lexlibCharNew("❤"))
		ERROR("char[18]");
	if(lexlibStringChar(string, 21) != lexlibCharNew("𝗕"))
		ERROR("char[21]");
	if(lexlibStringChar(string, 24) != lexlibCharNew("𝗘"))
		ERROR("char[24]");
	if(lexlibStringChar(string, 25) != lexlibCharNew("𝗙"))
		ERROR("char[25]");
	if(lexlibStringChar(string, 28) != lexlibCharNew("𝗜"))
		ERROR("char[28]");
	if(lexlibStringChar(string, 31) != lexlibCharNew("𝗟"))
		ERROR("char[31]");
	if(lexlibStringChar(string, 36) != lexlibCharNew("𝗤"))
		ERROR("char[36]");
	if(lexlibStringChar(string, 45) != lexlibCharNew("𝗭"))
		ERROR("char[45]");
	if(lexlibStringChar(string, 121) != LEXLIB_CHAR_NULL)
		ERROR("char[121]");

	stringAppend = lexlibStringAppend(string, string2);
	if(!stringAppend)
		ERROR("string append");
	string = NULL;

	if(!lexlibStringCompareStr(stringAppend, strImnewDuped))
		ERROR("append compare str");

	if(lexlibStringIsAscii(string2))
		ERROR("string is ascii");

	string = lexlibStringNew("abcdefgh i forgot what follows :(");
	if(!lexlibStringIsAscii(string))
		ERROR("string is not ascii");

	end:
	lexlibStringDelete(string);
	lexlibStringDelete(string2);
	lexlibStringDelete(stringCopy);
	lexlibStringDelete(stringVec);
	lexlibStringDelete(stringAppend);
	testEnd(error);
}
