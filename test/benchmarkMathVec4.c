// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/math/vec4.h>
#include<lexlib/os.h>
#include"main.h"

void benchmarkVec4(void){
	struct LexlVec4 vec1 = {lexlRandom(), lexlRandom(), lexlRandom(), lexlRandom()};
	struct LexlVec4 vec2 = {lexlRandom(), lexlRandom(), lexlRandom(), lexlRandom()};
	volatile float oflt = 0.0f;
	
	{benchmarkMessage("vec4Add");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Add((struct LexlVec4*)&vec2, &vec1, &vec2);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Sub");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Sub((struct LexlVec4*)&vec2, &vec1, &vec2);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Mul");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Mul((struct LexlVec4*)&vec2, &vec1, &vec2);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Div");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Div((struct LexlVec4*)&vec2, &vec1, &vec2);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Scl");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Scale((struct LexlVec4*)&vec2, &vec1, 5.7f);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Dot");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			oflt = lexlVec4Dot(&vec1, &vec2);
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Mag");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			oflt = lexlVec4Mag(&vec1);
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Nrm");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Norm((struct LexlVec4*)&vec2, &vec1);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Neg");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Neg((struct LexlVec4*)&vec2, &vec1);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	{benchmarkMessage("vec4Divs");
		benchmarkStart();
		for(uint32_t i = 0; i < LOOP; ++i){
			lexlVec4Divs((struct LexlVec4*)&vec2, &vec1, 3.7f);
			oflt = vec2.x;
			oflt = vec2.z;
		}
		benchmarkResult(benchmarkEnd());
	}
	
	// eliminate warning about oflt set but not used
	vec2.z = oflt;
}
