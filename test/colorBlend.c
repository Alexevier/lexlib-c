// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/color.h>
#include<stddef.h>
#include"test.h"

void testColorBlend(void){
	testStart("color blend");

	struct LexlibColor color;
	const char* errmsg = NULL;

	color = lexlibColorBlend((struct LexlibColor){0x8C, 0x32, 0x95, 0xFF}, (struct LexlibColor){0xE0, 0x2F, 0xF0, 0xCC}, LEXLIB_ADD);
	if(color.r != 0xFF) errmsg = "red add is wrong";
	if(color.g != 0x57) errmsg = "green add is wrong";
	if(color.b != 0xFF) errmsg = "blue add is wrong";
	if(color.a != 0xFF) errmsg = "alpha add is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0x36, 0xC9, 0x3D, 0xCC}, LEXLIB_ADD);
	if(color.r != 0xFF) errmsg = "red add is wrong";
	if(color.g != 0xF7) errmsg = "green add is wrong";
	if(color.b != 0xFF) errmsg = "blue add is wrong";
	if(color.a != 0xFF) errmsg = "alpha add is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x79, 0x8F, 0xAf, 0xFF}, (struct LexlibColor){0x38, 0x1A, 0x9E, 0xCC}, LEXLIB_ADD);
	if(color.r != 0xA5) errmsg = "red add is wrong";
	if(color.g != 0xA3) errmsg = "green add is wrong";
	if(color.b != 0xFF) errmsg = "blue add is wrong";
	if(color.a != 0xFF) errmsg = "alpha add is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0x21, 0x89, 0x11, 0xCC}, LEXLIB_ADD);
	if(color.r != 0xBF) errmsg = "red add is wrong";
	if(color.g != 0xFF) errmsg = "green add is wrong";
	if(color.b != 0xFF) errmsg = "blue add is wrong";
	if(color.a != 0xFF) errmsg = "alpha add is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x44, 0x20, 0x3A, 0xFF}, (struct LexlibColor){0x00, 0x08, 0x31, 0xCC}, LEXLIB_MUL);
	if(color.r != 0x0D) errmsg = "red mul is wrong";
	if(color.g != 0x07) errmsg = "green mul is wrong";
	if(color.b != 0x16) errmsg = "blue mul is wrong";
	if(color.a != 0xFF) errmsg = "alpha mul is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0xB5, 0xA9, 0x4C, 0xCC}, LEXLIB_MUL);
	if(color.r != 0x0B) errmsg = "red mul is wrong";
	if(color.g != 0x06) errmsg = "green mul is wrong";
	if(color.b != 0x0B) errmsg = "blue mul is wrong";
	if(color.a != 0xFF) errmsg = "alpha mul is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x5E, 0x29, 0xAB, 0xFF}, (struct LexlibColor){0xEA, 0x07, 0xE4, 0xCC}, LEXLIB_MUL);
	if(color.r != 0x69) errmsg = "red mul is wrong";
	if(color.g != 0x09) errmsg = "green mul is wrong";
	if(color.b != 0xBB) errmsg = "blue mul is wrong";
	if(color.a != 0xFF) errmsg = "alpha mul is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0xE3, 0xCE, 0x48, 0xCC}, LEXLIB_MUL);
	if(color.r != 0x72) errmsg = "red mul is wrong";
	if(color.g != 0x09) errmsg = "green mul is wrong";
	if(color.b != 0x5A) errmsg = "blue mul is wrong";
	if(color.a != 0xFF) errmsg = "alpha mul is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x03, 0x38, 0x5E, 0xFF}, (struct LexlibColor){0x58, 0x1A, 0x40, 0xCC}, LEXLIB_MOD);
	if(color.r != 0x01) errmsg = "red mod is wrong";
	if(color.g != 0x05) errmsg = "green mod is wrong";
	if(color.b != 0x17) errmsg = "blue mod is wrong";
	if(color.a != 0xFF) errmsg = "alpha mod is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0xFB, 0x64, 0xAE, 0xCC}, LEXLIB_MOD);
	if(color.r != 0x00) errmsg = "red mod is wrong";
	if(color.g != 0x01) errmsg = "green mod is wrong";
	if(color.b != 0x0F) errmsg = "blue mod is wrong";
	if(color.a != 0xFF) errmsg = "alpha mod is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x1A, 0x4D, 0x89, 0xFF}, (struct LexlibColor){0x96, 0xB7, 0x58, 0xCC}, LEXLIB_MOD);
	if(color.r != 0x0F) errmsg = "red mod is wrong";
	if(color.g != 0x37) errmsg = "green mod is wrong";
	if(color.b != 0x2F) errmsg = "blue mod is wrong";
	if(color.a != 0xFF) errmsg = "alpha mod is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0x4D, 0x98, 0xDF, 0xCC}, LEXLIB_MOD);
	if(color.r != 0x04) errmsg = "red mod is wrong";
	if(color.g != 0x20) errmsg = "green mod is wrong";
	if(color.b != 0x28) errmsg = "blue mod is wrong";
	if(color.a != 0xFF) errmsg = "alpha mod is wrong";

	color = lexlibColorBlend((struct LexlibColor){0xB5, 0x90, 0x0F, 0xFF}, (struct LexlibColor){0x21, 0x9D, 0x37, 0xCC}, LEXLIB_BLEND);
	if(color.r != 0x3F) errmsg = "red blend is wrong";
	if(color.g != 0x9A) errmsg = "green blend is wrong";
	if(color.b != 0x2F) errmsg = "blue blend is wrong";
	if(color.a != 0xFF) errmsg = "alpha blend is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0x7A, 0xDC, 0x7E, 0xCC}, LEXLIB_BLEND);
	if(color.r != 0x6E) errmsg = "red blend is wrong";
	if(color.g != 0xCF) errmsg = "green blend is wrong";
	if(color.b != 0x6E) errmsg = "blue blend is wrong";
	if(color.a != 0xFF) errmsg = "alpha blend is wrong";

	color = lexlibColorBlend((struct LexlibColor){0x8D, 0xCC, 0x6A, 0xFF}, (struct LexlibColor){0x16, 0xEB, 0xB8, 0xCC}, LEXLIB_BLEND);
	if(color.r != 0x2E) errmsg = "red blend is wrong";
	if(color.g != 0xE5) errmsg = "green blend is wrong";
	if(color.b != 0xA8) errmsg = "blue blend is wrong";
	if(color.a != 0xFF) errmsg = "alpha blend is wrong";
	color = lexlibColorBlend(color, (struct LexlibColor){0xFA, 0x85, 0x52, 0xCC}, LEXLIB_BLEND);
	if(color.r != 0xD1) errmsg = "red blend is wrong";
	if(color.g != 0x99) errmsg = "green blend is wrong";
	if(color.b != 0x63) errmsg = "blue blend is wrong";
	if(color.a != 0xFF) errmsg = "alpha blend is wrong";

	testEnd(errmsg);
}
