// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/lexlib.h>
#include<inttypes.h>
#include<string.h>
#include"test.h"

int main(int argc, char **argv){
	DATA.start = lexlibTimeMillis();
	printInfo();

	for(int i = 0; i < argc; i++){
		if(strcmp(argv[i], "benchmark") == 0)
			DATA.benchmark.yes = 1;
	}

	testCFile();
	testColorBlend();
	testColorGray();
	testImage();
	testImageBmp();
	testImagePng();
	testIO();
	testMem();
	testPath();
	testStr();
	testString();
	testVec();
	testVec4();

	if(DATA.benchmark.yes){
	}

	unsigned int total = DATA.test.fail + DATA.test.success;
	if(total != DATA.test.count){
		printf("[error]: the total of tests results (%u) and tests started (%u) are not the same.\n", total, DATA.test.count);
		return 2;
	}
	if(DATA.test.fail){
		printf("%u from %u tests failed.\n", DATA.test.fail, DATA.test.count);
		return 1;
	}

	printf("all tests succeed.\n");
	printf("real time: %"PRIu64"ms\n", lexlibTimeMillis() - DATA.start);
	printf("cpu  time: %"PRIu64"ms\n", lexlibProcMillis());
	return 0;
}
