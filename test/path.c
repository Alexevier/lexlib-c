#include"test.h"
#include<lexlib/path.h>
#include<lexlib/str.h>
#include<stddef.h>
#include<stdlib.h>

#define fail(X) {error = X; goto end;}

void testPath(void){
	testStart("path");
	const char *error = NULL;
	const char *patht;
	char *path = NULL;
#ifdef _WIN32
	patht = "C:\\user\\ i exist\\path\\sub\\dir\\directowy\\protected\\important thing.txt";
	#define HEAD "C:\\"
	#define SEP "\\"
#else
	patht = "/home/user/ i exist/path/sub/dir/directowy/protected/important thing.txt";
	#define HEAD "/home/"
	#define SEP "/"
#endif

	path = lexlibPathNew(HEAD"user");
	if(!path)
		fail("lexlibPathNew");

	if(!lexlibPathAbsolute(path))
		fail("lexlibPathAbsolute");

	if(lexlibPathAdd(&path, " i exist"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"))
		fail("compare 1");

	if(lexlibPathAdd(&path, "path/sub/"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP))
		fail("compare 2");

	if(lexlibPathAdd(&path, "dir"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"))
		fail("compare 3");

	if(lexlibPathAdd(&path, "/directowy/"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP))
		fail("compare 4");

	if(lexlibPathAdd(&path, "protected"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"))
		fail("compare 5");

	if(lexlibPathAdd(&path, "/junk"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"SEP"junk"))
		fail("compare 6");

	if(lexlibPathAsDir(&path))
		fail("lexlibPathAsDir");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"SEP"junk"SEP))
		fail("compare 7");

	if(lexlibPathPop(&path))
		fail("lexlibPathPop");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"SEP))
		fail("compare 8");

	if(lexlibPathAdd(&path, "important thing.txt/"))
		fail("lexlibPathAdd");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"SEP"important thing.txt"SEP))
		fail("compare 9");

	if(lexlibPathAsFile(&path))
		fail("lexlibPathAsFile");
	if(!lexlibStrCompare(path, HEAD"user"SEP" i exist"SEP"path"SEP"sub"SEP"dir"SEP"directowy"SEP"protected"SEP"important thing.txt"))
		fail("compare 10");

	if(!lexlibStrCompare(path, patht))
		fail("compare final");

	end:
	free(path);
	testEnd(error);
}
