// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/math/vec4.h>
#include<lexlib/math.h>
#include<lexlib/os.h>
#include<math.h>
#include"test.h"

void testVec4(void){
	testStart("vec4");
	char* errMsg = NULL;

	{//add
		struct LexlibVec4 v1 = {78.9f, 8.96f, 89.58f, 7.5f};
		struct LexlibVec4 v2 = {9.5f, 5.59f, 8.88f, 4.6f};
		struct LexlibVec4 res;

		lexlibVec4Add(&res, &v1, &v2);

		if(!lexlibFloatCmp(res.x, 88.4f, FLT_EPSILON))
			errMsg = "add.x";
		if(!lexlibFloatCmp(res.y, 14.55f, FLT_EPSILON))
			errMsg = "add.y";
		if(!lexlibFloatCmp(res.z, 98.46f, FLT_EPSILON))
			errMsg = "add.z";
		if(!lexlibFloatCmp(res.w, 12.1f, FLT_EPSILON))
			errMsg = "add.w";
	}

	{//sub
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 v2 = {9.5f, 5.59f, 89.88f, 4.6f};
		struct LexlibVec4 res;

		lexlibVec4Sub(&res, &v1, &v2);

		if(!lexlibFloatCmp(res.x, 69.4f, FLT_EPSILON))
			errMsg = "sub.x";
		if(!lexlibFloatCmp(res.y, 3.37f, FLT_EPSILON))
			errMsg = "sub.y";
		if(!lexlibFloatCmp(res.z, -80.3f, FLT_EPSILON))
			errMsg = "sub.z";
		if(!lexlibFloatCmp(res.w, 2.9f, FLT_EPSILON))
			errMsg = "sub.w";
	}

	{//mul
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 v2 = {9.5f, 5.59f, 89.88f, 4.6f};
		struct LexlibVec4 res;

		lexlibVec4Mul(&res, &v1, &v2);

		if(!lexlibFloatCmp(res.x, 749.55f, FLT_EPSILON))
			errMsg = "mul.x";
		if(!lexlibFloatCmp(res.y, 50.0864f, FLT_EPSILON))
			errMsg = "mul.y";
		if(!lexlibFloatCmp(res.z, 861.0504f, FLT_EPSILON))
			errMsg = "mul.z";
		if(!lexlibFloatCmp(res.w, 34.5f, FLT_EPSILON))
			errMsg = "mul.w";
	}

	{//div
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 v2 = {9.5f, 5.59f, 89.88f, 4.6f};
		struct LexlibVec4 res;

		lexlibVec4Div(&res, &v1, &v2);

		if(!lexlibFloatCmp(res.x, 8.305263157894736842105f, FLT_EPSILON))
			errMsg = "div.x";
		if(!lexlibFloatCmp(res.y, 1.602862254025044722719f, FLT_EPSILON))
			errMsg = "div.y";
		if(!lexlibFloatCmp(res.z, 0.1065865598575878949711f, FLT_EPSILON))
			errMsg = "div.z";
		if(!lexlibFloatCmp(res.w, 1.630434782608695652174f, FLT_EPSILON))
			errMsg = "div.w";
	}

	{//dot
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 v2 = {9.5f, 5.59f, 89.88f, 4.6f};
		float res = 0.0f;
		res = lexlibVec4Dot(&v1, &v2);

		if(!lexlibFloatCmp(res, 1695.1868f, FLT_EPSILON))
			errMsg = "dot";
	}

	{//mag
		struct LexlibVec4 vec = {78.9f, 8.96f, 9.58f, 7.5f};
		float res = 0.0f;
		res = lexlibVec4Mag(&vec);

		if(!lexlibFloatCmp(res, 80.33379114668994631712f, FLT_EPSILON))
			errMsg = "mag";
	}

	{//normalize
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 res;

		lexlibVec4Norm(&res, &v1);

		if(!lexlibFloatCmp(res.x, 0.9821520791409951540263f, FLT_EPSILON))
			errMsg = "nrm.x";
		if(!lexlibFloatCmp(res.y, 0.1115346340824247982266f, FLT_EPSILON))
			errMsg = "nrm.y";
		if(!lexlibFloatCmp(res.z, 0.119252432422949728461f, FLT_EPSILON))
			errMsg = "nrm.z";
		if(!lexlibFloatCmp(res.w, 0.09336046379667254315839f, FLT_EPSILON))
			errMsg = "nrm.w";
	}

	{//negate
		struct LexlibVec4 v1 = {78.9f, 8.96f, 9.58f, 7.5f};
		struct LexlibVec4 res;

		lexlibVec4Neg(&res, &v1);

		if(!lexlibFloatCmp(res.x, -78.9f, FLT_EPSILON))
			errMsg = "neg.x";
		if(!lexlibFloatCmp(res.y, -8.96f, FLT_EPSILON))
			errMsg = "neg.y";
		if(!lexlibFloatCmp(res.z, -9.58f, FLT_EPSILON))
			errMsg = "neg.z";
		if(!lexlibFloatCmp(res.w, -7.5f, FLT_EPSILON))
			errMsg = "neg.w";
	}

	testEnd(errMsg);
}
