// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/vec.h>
#include"test.h"

static void lexlibVecExample(void);

void testVec(void){
	testStart("vec");
	const char* err = NULL;

	LexlibVec(int) vec = lexlibVecNew(0, *vec);
	if(!vec){
		testEnd("out of memory");
		return;
	}

	if(lexlibVecPush(vec, &(int){777})){
		err = "push 1";
		goto end;
	}

	if(lexlibVecInsert(vec, 0, &(int){874})){
		err = "insert 1";
		goto end;
	}

	if(lexlibVecPush(vec, &(int){-999})){
		err = "push 2";
		goto end;
	};

	if(lexlibVecPush(vec, &(int){5})){
		err = "push 3";
		goto end;
	};

	lexlibVecPop(vec);

	for(int i = 0; i < 5; i++)
		if(lexlibVecPush(vec, &i)){
			err = "push loop";
			goto end;
		}

	if(lexlibVecRemove(vec, 4)){
		err = "remove";
		goto end;
	};

	if(lexlibVecSwap(vec, 3, 0)){
		err = "swap";
		goto end;
	}

	if(lexlibVecInsert(vec, 4, &(int){69})){
		err = "insert 2";
		goto end;
	}

	if(vec[0] != 0){
		err = "vec[0]";
		goto end;
	}

	if(vec[1] != 777){
		err = "vec[1]";
		goto end;
	}

	if(vec[2] != -999){
		err = "vec[2]";
		goto end;
	}

	if(vec[3] != 874){
		err = "vec[3]";
		goto end;
	}

	if(vec[4] != 69){
		err = "vec[4]";
		goto end;
	}

	if(vec[5] != 2){
		err = "vec[5]";
		goto end;
	}

	if(vec[6] != 3){
		err = "vec[6]";
		goto end;
	}

	if(vec[7] != 4){
		err = "vec[7]";
		goto end;
	}

	if(lexlibVecGet(vec, 8) != NULL){
		err = "get(8)";
		goto end;
	}

	end:
	lexlibVecDelete(vec);
	testEnd(err);

	lexlibVecExample();
}

#include<stdlib.h>
#include<stdio.h>

void lexlibVecExample(void){
	// create a new vector for floats.
	// can aslo be declared as "float *vec = ...;"
	LexlibVec(float) vec = lexlibVecNew(0, *vec);
	float value = 777.0f;
	float pi = 3.14f;

	// push new values to the vector
	lexlibVecPush(vec, &value);
	lexlibVecPush(vec, &(float){1.234f});

	// insert value
	lexlibVecInsert(vec, 0, &pi);

	// remove
	lexlibVecRemove(vec, 1);

	// access
	if(vec[0] != 3.14f){
		printf("vec[0] is not 3.14\n");
		abort();
	}
	if(vec[1] != 1.234f){
		printf("vec[1] is not 1.234\n");
		abort();
	}

	// delete/free the vector
	lexlibVecDelete(vec);
}
