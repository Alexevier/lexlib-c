// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<main.h>
#include<lexlib/image.h>
#include<lexlib/os.h>

void benchmarkImage(void){
	LexlibImage image = lexlibImageNew(3200, 3200, LEXLIB_RGBA, 8);
	
	// fill with a initial noise
	srand(lexlibRandom());
	for(uint32_t y = 0; y < 3200; y++){
		for(uint32_t x = 0; x < 3200; x++){
			LexlibColor color = {rand(), rand(), rand(), 0xFF};
			lexlibImagePixelSet(&image, x, y, color, LEXLIB_NONE);
		}
	}
	
	benchmarkMessage("image pixelSet");
	for(uint32_t y = 0; y < 3200; y++){
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		for(uint32_t x = 0; x < 3200; x++){
			lexlibImagePixelSet(&image, x, y, color, LEXLIB_BLEND);
		}
	}
	
	for(uint32_t x = 0; x < 3200; x++){
		LexlibColor color = {lexlibRandom(), lexlibRandom(), lexlibRandom(), lexlibRandom()};
		for(uint32_t y = 0; y < 3200; y++){
			lexlibImagePixelSet(&image, x, y, color, LEXLIB_BLEND);
		}
	}
	benchmarkResult(benchmarkEnd());
	
	benchmarkMessage("image bmp save");
	lexlibImageSaveBmpEx(&image, "resources/out/benchmark.bmp", LEXLIB_RGB565, LEXLIB_BMP_INFO);
	benchmarkResult(benchmarkEnd());
	lexlibImageDelete(&image);
}
