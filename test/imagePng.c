// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/image.h>
#include<stdio.h>
#include"test.h"

void testImagePng(void){
	testStart("image png");

	struct LexlibImage image;
	if(lexlibImageLoadPng(&image, "resources/image/pixelDucks.png")){
		switch(image.flags){
			case LEXLIB_OK:
				break;
			case LEXLIB_CANT_OPEN:
				testEnd("can't open the file");
				return;
			case LEXLIB_INVALID_OPERATION:
				testEnd("file is not a png");
				return;
			case LEXLIB_OUT_OF_MEMORY:
				testEnd("OOM");
				return;
			default:
				testEnd("unknown load error");
				return;
		}
	}

	switch(lexlibImageFlip(&image, LEXLIB_FLIP_Y | LEXLIB_FLIP_X)){
		case LEXLIB_OK:
			break;
		case LEXLIB_INVALID_VALUE:
			testEnd("flipping was invalid");
			return;
		case LEXLIB_OUT_OF_MEMORY:
			testEnd("OOM");
			return;
		default:
			testEnd("unknown flipping error");
			return;
	}

	switch(lexlibImageSave(&image, "resources/out/pixelDucks.png")){
		case LEXLIB_OK:
			break;
		case LEXLIB_INVALID_VALUE:
			testEnd("saving was invalid");
			return;
		case LEXLIB_CANT_WRITE:
			testEnd("can't create/write file");
			return;
		case LEXLIB_OUT_OF_MEMORY:
			testEnd("OOM");
			return;
		default:
			testEnd("unknown saving error");
			return;
	}

	lexlibImageDelete(&image);
	testEnd(NULL);
}
