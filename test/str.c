// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/str.h>
#include<string.h>
#include<stdlib.h>
#include"test.h"

void testStr(void){
	testStart("str");

	const char str1[] = "The quick brown fox";
	const char str2[] = " jumps over the lazy dog.";
	const char strt[] = "The quick brown fox jumps over the lazy dog.";
	const char text[] = "The quick brown fox jumps over the lazy dog.\nThe quick brown fox jumps over the lazy dog.";

	char *str = lexlibStrCat(str1, str2);
	if(!str){
		testEnd("lexlibStrCat returned NULL");
		return;
	}

	char *strc = lexlibStrCopy(str1);
	if(!strc){
		testEnd("lexlibStrCopy returned NULL");
		return;
	}

	char *txt = lexlibStrFile("resources/textFile.txt");
	if(!txt){
		testEnd("failed to read: resources/textFile.txt");
		return;
	}

	char *txtUtf8 = lexlibStrFile("resources/textUTF8.txt");
	if(!txtUtf8){
		testEnd("failed to read: resources/textUTF8.txt");
		return;
	}

	const char *error = NULL;

	if(strcmp(str, strt) != 0)
		error = "str is wrong";

	if(strcmp(strc, str1) != 0)
		error = "strc is wrong";

	if(strcmp(txt, text) != 0)
		error = "txt is incorrect";

	if(lexlibStrLen(txtUtf8) != 2940)
		error = "txtUtf8 has incorrect length";

	if(lexlibStrSize(txtUtf8) != 8659)
		error = "txtUtf8 has wrong size";

	if(!lexlibStrCompare(txt, text))
		error = "compare failed";

	testEnd(error);
	free(str);
	free(strc);
	free(txt);
	free(txtUtf8);
}
