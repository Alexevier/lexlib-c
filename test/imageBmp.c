// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/image.h>
#include<stdio.h>
#include"test.h"

static uint16_t fileCnt = 7;
static char *file[] = {
	"resources/image/plasma16.bmp",
	"resources/image/plasma24.bmp",
	"resources/image/plasma32.bmp",
	"resources/image/macintosh.bmp",
	"resources/image/macintosh_alpha70.bmp",
	"resources/image/macintosh_8bpp.bmp",
	"resources/image/puro.bmp",
	// "resources/image/floppy.bmp",
};

static char *ofile[] = {
	"resources/out/plasma16.bmp",
	"resources/out/plasma24.bmp",
	"resources/out/plasma32.bmp",
	"resources/out/macintosh.bmp",
	"resources/out/macintosh_alpha70.bmp",
	"resources/out/macintosh_8bpp.bmp",
	"resources/out/puro.bmp",
	// "resources/out/floppy.bmp",
};

void testImageBmp(void){
	testStart("image bmp");

	uint8_t err;
	for(uint16_t i = 0; i < fileCnt; i++){
		struct LexlibImage image;
		if((err = lexlibImageLoad(&image, file[i]))){
			printf("0x%X, %s, ", err, file[i]);
			testEnd("load");
			return;
		}
		if((err = lexlibImageSave(&image, ofile[i]))){
			printf("0x%X, %s, ", err, ofile[i]);
			testEnd("save");
			return;
		}
		lexlibImageDelete(&image);
	}

	testEnd(NULL);
}
