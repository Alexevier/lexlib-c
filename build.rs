fn main(){
	println!("cargo:rerun-if-changed=src/*");
	println!("cargo:rerun-if-changed=include/*");

	let mut lexlibc = cc::Build::new();

	lexlibc
		.no_default_flags(true)
		.static_flag(true)
		.warnings(true)
		.flag("-Wpedantic")
		.define("LEXLIB_VERSION", Some(env!("CARGO_PKG_VERSION")));

	#[cfg(debug_assertions)]
	lexlibc.flag("-g");

	#[cfg(feature = "libpng")]
	lexlibc.define("LIBPNG", None);

	lexlibc.define("BUFFERSIZE", Some("256"));
	lexlibc.define("VEC_RESIZE", Some("2"));

	lexlibc
		.include("include")
		.file("src/cfile.c")
		.file("src/char.c")
		.file("src/color/color8.c")
		.file("src/color/colorf.c")
		.file("src/color/color16.c")
		.file("src/file.c")
		.file("src/filesystem.c")
		.file("src/image.c")
		.file("src/image/bin.c")
		.file("src/image/bmp.c")
		.file("src/image/png.c")
		.file("src/image/stbi.c")
		.file("src/image/stbiw.c")
		.file("src/io.c")
		.file("src/mem.c")
		.file("src/path.c")
		.file("src/str.c")
		.file("src/string.c")
		.file("src/time.c")
		.file("src/vec.c");

	#[cfg(target_family = "unix")]
	lexlibc
		.file("src/unix/mem.c")
		.file("src/unix/os.c");

	#[cfg(target_family = "windows")]
	lexlibc
		.file("src/windows/mem.c")
		.file("src/windows/os.c");

	lexlibc.compile("lexlibc");
}
