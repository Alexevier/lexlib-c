// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

//! time.

use crate::c;

pub mod real {
	//! Real time.

	use super::*;

	#[inline(always)]
	pub fn nanos() -> u64 {
		unsafe{c::lexlibTimeNanos()}
	}

	#[inline(always)]
	pub fn micros() -> u64 {
		unsafe{c::lexlibTimeMicros()}
	}

	#[inline(always)]
	pub fn millis() -> u64 {
		unsafe{c::lexlibTimeMillis()}
	}

	#[inline(always)]
	pub fn seconds() -> u64 {
		unsafe{c::lexlibTimeSeconds()}
	}
}

pub mod thread {
	//! cpu time consumed by the thread.

	use super::*;

	#[inline(always)]
	pub fn nanos() -> u64 {
		unsafe{c::lexlibThrdNanos()}
	}

	#[inline(always)]
	pub fn micros() -> u64 {
		unsafe{c::lexlibThrdMicros()}
	}

	#[inline(always)]
	pub fn millis() -> u64 {
		unsafe{c::lexlibThrdMillis()}
	}

	#[inline(always)]
	pub fn seconds() -> u64 {
		unsafe{c::lexlibThrdSeconds()}
	}
}

pub mod process {
	//! cpu time consumed by the process.

	use super::*;

	#[inline(always)]
	pub fn nanos() -> u64 {
		unsafe{c::lexlibProcNanos()}
	}

	#[inline(always)]
	pub fn micros() -> u64 {
		unsafe{c::lexlibProcMicros()}
	}

	#[inline(always)]
	pub fn millis() -> u64 {
		unsafe{c::lexlibProcMillis()}
	}

	#[inline(always)]
	pub fn seconds() -> u64 {
		unsafe{c::lexlibProcSeconds()}
	}
}

/// A simple timer.
///
/// uses real time.<br>
/// millisecond resolution.
#[repr(C)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Timer {
	time: u64,
	left: u64,
	last: u64,
}

impl Timer {
	/// creates a new Timer.
	///
	/// is started at creation.
	#[inline(always)]
	pub const fn new(ms: u64) -> Self {
		Self {
			time: ms,
			left: ms,
			last: 0,
		}
	}

	/// set the time.
	#[inline(always)]
	pub fn set(&mut self, ms: u64){
		unsafe{c::lexlibTimerSet(self, ms)}
	}

	// add time to a running timer.
	#[inline(always)]
	pub fn add(&mut self, ms: u64){
		unsafe{c::lexlibTimerAdd(self, ms)}
	}

	/// starts/restarts the Timer.
	#[inline(always)]
	pub fn start(&mut self){
		unsafe{c::lexlibTimerStart(self)}
	}

	/// updates the timer.
	#[inline(always)]
	pub fn update(&mut self){
		unsafe{c::lexlibTimerUpdate(self)}
	}

	/// checks if the timer is done.
	///
	/// implicitly calls [`Timer::update`].
	#[inline(always)]
	pub fn done(&mut self) -> bool {
		unsafe{ c::lexlibTimerDone(self) != 0 }
	}

	/// blocks the thread until the timer is done.
	#[inline(always)]
	pub fn until_done(&mut self){
		unsafe{c::lexlibTimerUntilDone(self)}
	}

	/// checks if the Timer has finished.
	///
	/// does not call [`Timer::update`].
	#[inline(always)]
	pub fn finished(&self) -> bool {
		unsafe{ c::lexlibTimerFinished(self) != 0 }
	}

	/// gets the remaining time.
	#[inline(always)]
	pub fn left(&self) -> u64 {
		self.left
	}

	/// gets the initial time.
	#[inline(always)]
	pub fn time(&self) -> u64 {
		self.time
	}
}
