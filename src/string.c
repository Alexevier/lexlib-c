/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexlib/string.h>
#include<lexlib/char.h>
#include<lexlib/str.h>
#include<lexlib/vec.h>
#include<lexlib/mem.h>
#include<stdlib.h>
#include<stddef.h>
#include<stdint.h>
#include<string.h>

struct LexlibString {
	size_t size;
	uint8_t str[];
};

LexlibString lexlibStringNew(const char *str){
	size_t size = lexlibStrSize(str);
	LexlibString string = malloc(sizeof(struct LexlibString) + size);
	if(!string)
		return NULL;

	string->size = size;
	memcpy(string->str, str, size);

	return string;
}

void lexlibStringDelete(LexlibString string){
	if(string == NULL)
		return;
	free(string);
}

LexlibString lexlibStringCopy(const LexlibString string){
	LexlibString newString = malloc(sizeof(struct LexlibString) + string->size);
	if(!newString)
		return NULL;

	newString->size = string->size;
	memcpy(newString->str, string->str, string->size);

	return newString;
}

LexlibString lexlibStringFromC(const char *cstring){
	size_t csize = strlen(cstring)+1;
	LexlibString string = malloc(sizeof(struct LexlibString) + csize);
	if(!string)
		return NULL;

	string->size = csize;
	memcpy(string->str, cstring, csize);

	return string;
}

LexlibString lexlibStringFromVec(const LexlibVec(char) vec){
	size_t veclen = lexlibVecLen(vec);
	size_t size = veclen;

	if(vec[veclen-1] != '\0')
		size += 1;

	LexlibString string = malloc(sizeof(struct LexlibString) + size);
	string->size = size;
	memcpy(string->str, vec, veclen * sizeof(char));

	string->str[string->size-1] = '\0';

	return string;
}

LexlibChar lexlibStringChar(const LexlibString string, size_t index){
	size_t idx = 0;
	for(size_t i = 0; i < string->size; ){
		LexlibChar chr = lexlibCharNew((const char *)string->str+i);
		if(chr){
			if(idx == index)
				return chr;
			i += lexlibCharSize(chr);
			++idx;
		} else {
			break;
		}
	}

	return LEXLIB_CHAR_NULL;
}

LexlibString lexlibStringAppend(LexlibString string, const LexlibString stringAdd){
	size_t size = string->size + stringAdd->size - 1;
	LexlibString newString = malloc(sizeof(struct LexlibString) + size);
	if(newString == NULL)
		return NULL;

	newString->size = size;
	memcpy(newString->str, string->str, string->size-1);
	memcpy(newString->str+string->size-1, stringAdd->str, stringAdd->size); /* will copy the nullchar */

	lexlibStringDelete(string);

	return newString;
}

LexlibString lexlibStringAppendStr(LexlibString string, const char *str){
	size_t strLen = strlen(str);
	size_t size = string->size + strLen;
	LexlibString newString = malloc(sizeof(struct LexlibString) + size);
	if(newString == NULL)
		return NULL;

	newString->size = size;
	memcpy(newString->str, string->str, string->size-1);
	memcpy(newString->str+string->size-1, str, strLen);
	newString->str[newString->size-1] = '\0';

	lexlibStringDelete(string);

	return newString;
}

LexlibBool lexlibStringCompare(const LexlibString lstring, const LexlibString rstring){
	if(lstring->size != rstring->size)
		return LEXLIB_FALSE;

	return lexlibMemCompare(lstring->str, rstring->str, lstring->size);
}

LexlibBool lexlibStringCompareStr(const LexlibString string, const char *cstring){
	const size_t csize = strlen(cstring)+1;

	if(string->size != csize)
		return LEXLIB_FALSE;

	return lexlibMemCompare(string->str, cstring, string->size);
}

size_t lexlibStringLen(const LexlibString string){
	/* :) */
	return lexlibStrLen((const char *)string->str);
}

size_t lexlibStringSize(const LexlibString string){
	return string->size;
}

LexlibBool lexlibStringValid(const LexlibString string){
	if(string->str[string->size-1] != '\0')
		return LEXLIB_FALSE;

	const size_t size = string->size-1;
	for(size_t i = 0; i < size; ){
		if(string->str[i] == '\0')
			return LEXLIB_FALSE;

		if(string->str[i] & 0x80){
			if(string->str[i] & 0x40){
				if(!(string->str[i+1] & 0x80) || string->str[i+1] & 0x40)
					return LEXLIB_FALSE;
				if(string->str[i] & 0x20){
					if(!(string->str[i+2] & 0x80) || string->str[i+2] & 0x40)
						return LEXLIB_FALSE;
					if(string->str[i] & 0x10){
						if(!(string->str[i+3] & 0x80) || string->str[i+3] & 0x40)
							return LEXLIB_FALSE;
						if(string->str[i] & 0x08){
							return LEXLIB_FALSE;
						}
						i+=4;
						continue;
					}
					i+=3;
					continue;
				}
				i+=2;
				continue;
			}
			return LEXLIB_FALSE;
		}
		i+=1;
	}

	return LEXLIB_TRUE;
}

LexlibBool lexlibStringIsAscii(const LexlibString string){
	for(size_t i = 0; i < string->size; i++)
		if(string->str[i] & 0x80)
			return LEXLIB_FALSE;

	return LEXLIB_TRUE;
}

LexlibString lexlibStringFix(LexlibString string){
	size_t size = string->size-1;

	if(string->str[size] != '\0'){
		string->size++;

		LexlibString new = realloc(string, sizeof(struct LexlibString) + string->size);
		if(new == NULL)
			return NULL;

		size = string->size-1;
		new->str[size] = '\0';
		string = new;
	}

	for(size_t i = 0; i < size; i++){
		if(string->str[i] == '\0')
			string->str[i] = ' ';
	}

	return string;
}

const char *lexlibStringRaw(const LexlibString string){
	return (const char *)string->str;
}

char *lexlibStringRawMut(LexlibString string){
	return (char *)string->str;
}
