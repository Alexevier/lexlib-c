// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#define _POSIX_C_SOURCE 199309L
#define _GNU_SOURCE

#include<sys/syscall.h>
#include<sys/sysinfo.h>
#include<sys/random.h>
#include<lexlib/os.h>
#include<unistd.h>
#include<time.h>

void lexlibSleep(unsigned int seconds){
	sleep(seconds);
}

void lexlibSleepMs(unsigned int millis){
	struct timespec time;
	time.tv_sec = millis / 1000;
	time.tv_nsec = (millis % 1000) * 1000000;
	nanosleep(&time, NULL);
}

unsigned int lexlibRandom(void){
	unsigned int num;
	ssize_t err = syscall(SYS_getrandom, &num, sizeof(unsigned int), GRND_NONBLOCK);
	if(err == -1)
		num = 0;
	return num;
}

uint64_t lexlibRamSize(void){
	struct sysinfo info;
	if(!sysinfo(&info))
		return info.totalram;
	return 0;
}
