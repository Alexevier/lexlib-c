// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/mem.h>
#include<sys/mman.h>

void lexlibMemUnmap(void* mem, size_t size){
	munmap(mem, size);
}
