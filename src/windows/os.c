// Copyright 2023-2024 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#define _CRT_RAND_S

#include<lexlib/str.h>
#include<lexlib/os.h>
#include<windows.h>

void lexlibSleep(unsigned int seconds){
	Sleep(seconds * 1000);
}

void lexlibSleepMs(unsigned int millis){
	Sleep(millis);
}

unsigned int lexlibRandom(void){
	unsigned int num;
	errno_t err;
	err = rand_s(&num);
	if(err != 0)
		num = 0;
	return num;
}

uint64_t lexlibRamSize(void){
	MEMORYSTATUSEX memoryStatus;
	memoryStatus.dwLength = sizeof(memoryStatus);
	if(GlobalMemoryStatusEx(&memoryStatus))
		return memoryStatus.ullTotalPhys;
	return 0;
}

char *lexlibCurrentExecutablePath(void){
	char buffer[MAX_PATH+1];

	if(GetModuleFileName(NULL, buffer, MAX_PATH) == 0)
		return NULL;

	char *path = lexlibStrCopy(buffer);

	return path;
}
