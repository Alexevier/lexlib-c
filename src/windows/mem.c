// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/mem.h>
#include<windows.h>

void lexlibMemUnmap(void* mem, LEXLIB_UNUSED size_t size){
	UnmapViewOfFile(mem);
}
