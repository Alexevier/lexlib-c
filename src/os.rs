// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

//! OS abstractions.
//!
//! Linux and Window$ are supported but this should work on all unix like systems.

use crate::c;

/// gets the physical ram size in bytes.
///
/// since it might fail a option is returned.
pub fn ram_size() -> Option<u64> {
	let size = unsafe{c::lexlibRamSize()};
	
	if size == 0 {
		None
	} else {
		Some(size)
	}
}

/// gets a pseudorandom number from the OS that is cryptographically secure.
///
/// since it might fail a option is returned.
pub fn random() -> Option<u32> {
	let mut num = unsafe{c::lexlibRandom()};
	
	if num == 0 { // might have failed, try again.
		num = unsafe{c::lexlibRandom()};
	}
	
	if num == 0 {
		None
	} else {
		Some(num)
	}
}

pub mod thread {
	use super::*;
	
	/// sleeps the thread for the specified number of seconds.
	#[inline(always)]
	pub fn sleep(seconds: u32){
		unsafe{c::lexlibSleep(seconds)}
	}
	
	/// sleeps the thread for the specified number of milliseconds.
	#[inline(always)]
	pub fn sleep_ms(milliseconds: u32){
		unsafe{c::lexlibSleepMs(milliseconds)}
	}
}
