#include<lexlib/image.h>
#include<lexlib/defines.h>
#include<stdlib.h>
#include<stdio.h>

/* bin structure
	|  uint32_t  |      uint8_t       |
	|width|height|channels|bpc|profile|
*/

uint8_t lexlibImageLoadBin(struct LexlibImage *image, const char *filename){
	*image = LEXLIB_IMAGE_ZERO;

	FILE *file = fopen(filename, "rb");
	if(!file)
		return LEXLIB_CANT_OPEN;

	if(fread(&image->width, sizeof(uint32_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}
	if(!image->width){
		fclose(file);
		return LEXLIB_INVALID_DATA;
	}

	if(fread(&image->height, sizeof(uint32_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}
	if(!image->height){
		fclose(file);
		return LEXLIB_INVALID_DATA;
	}

	if(fread(&image->channels, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}
	if(!image->channels){
		fclose(file);
		return LEXLIB_INVALID_DATA;
	}

	if(fread(&image->bpc, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}
	if(!image->bpc){
		fclose(file);
		return LEXLIB_INVALID_DATA;
	}

	if(fread(&image->profile, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}
	if(!image->profile){
		fclose(file);
		return LEXLIB_INVALID_DATA;
	}

	image->bpp = image->bpc * image->channels;
	image->flags = LEXLIB_NONE;

	size_t datasize = image->width * image->height * image->bpp / 8;
	image->data = malloc(datasize);
	if(!image->data){
		fclose(file);
		return LEXLIB_OUT_OF_MEMORY;
	}

	if(fread(image->data, sizeof(uint8_t), datasize, file) != datasize){
		fclose(file);
		return LEXLIB_PARTIAL_READ;
	}

	return LEXLIB_OK;
}

uint8_t lexlibImageSaveBin(const struct LexlibImage *image, const char *filename){
	FILE *file = fopen(filename, "wb");
	if(!file)
		return LEXLIB_CANT_WRITE;

	if(fwrite(&image->width, sizeof(uint32_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}
	if(fwrite(&image->height, sizeof(uint32_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}
	if(fwrite(&image->channels, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}
	if(fwrite(&image->bpc, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}
	if(fwrite(&image->profile, sizeof(uint8_t), 1, file) != 1){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}

	size_t datasize = image->width * image->height * image->bpp / 8;
	if(fwrite(image->data, sizeof(uint8_t), datasize, file) != datasize){
		fclose(file);
		return LEXLIB_PARTIAL_WRITE;
	}

	fclose(file);
	return LEXLIB_OK;
}
