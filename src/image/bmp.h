// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_internal_bmpheaders_h
#define lexlib_internal_bmpheaders_h

#include<stdint.h>

// "constants" //
#define BMP_RGB 0
#define BMP_RLE8 1
#define BMP_RLE4 2
#define BMP_BITFIELDS 3
#define BMP_JPEG 4
#define BMP_PNG 5
#define BMP_ALPHABITFIELDS 6
#define BMP_CMYK 11
#define BMP_CMYKRLE8 12
#define BMP_CMYKRLE4 13

// structs //

#pragma pack(push, 1)
typedef struct BmpFileHeader {
	// file type, should be "BM"
	uint16_t type;
	// size in bytes of the file
	uint32_t size;
	// set'd by generating program, generally 0
	uint16_t reserved1;
	uint16_t reserved2;
	// offset in bytes where the bitmap data starts
	uint32_t offset;
} BmpFileHeader;
#pragma pack(pop)

// windows 2.0 or later
typedef struct BmpCoreHeader {
	// size of the header
	uint32_t size;
	// bitmap width in pixels
	uint16_t width;
	// bitmap height in pixels
	uint16_t height;
	// number of color planes (must be 1)
	uint16_t planes;
	// number of bits per pixel
	uint16_t bitdepth;
} BmpCoreHeader;

// windows nt, 3.1x or later
typedef struct BmpInfoHeader {
	// size of the header
	uint32_t size;
	// bitmap width in pixels
	int32_t width;
	// bitmap height in pixels
	int32_t height;
	// number of color planes (must be 1)
	uint16_t planes;
	// number of bits per pixel
	uint16_t bitdepth;
	// compression method
	uint32_t compression;
	// raw size of the bitmap data
	uint32_t imagesize;
	// horizontal resolution, in pixels per meter
	int32_t xresm;
	// vertical resolution, in pixels per meter
	int32_t yresm;
	// number of color indices in the color table
	uint32_t colorPalette;
	// number of color indices that are considered important, if zero all are important
	uint32_t colorImportant;
} BmpInfoHeader;

// header 2 is not documented

// https://web.archive.org/web/20150127132443/https://forums.adobe.com/message/3272950#3272950
typedef struct BmpInfoHeader3 {
	// size of the header
	uint32_t size;
	// bitmap width in pixels
	int32_t width;
	// bitmap height in pixels
	int32_t height;
	// number of color planes (must be 1)
	uint16_t planes;
	// number of bits per pixel
	uint16_t bitdepth;
	// compression method
	uint32_t compression;
	// raw size of the bitmap data
	uint32_t imagesize;
	// horizontal resolution, in pixels per meter
	int32_t xresm;
	// vertical resolution, in pixels per meter
	int32_t yresm;
	// number of color indices in the color table
	uint32_t colorPalette;
	// number of color indices that are considered important, if zero all are important
	uint32_t colorImportant;
	// red color per pixel bit mask
	uint32_t redbitmask;
	// green color per pixel bit mask
	uint32_t greenbitmask;
	// blue color per pixel bit mask
	uint32_t bluebitmask;
	// alpha color per pixel bit mask
	uint32_t alphabitmask;
} BmpInfoHeader3;

// windows nt 4.0, 95
typedef struct BmpInfoHeader4 {
	// size of the header
	uint32_t size;
	// bitmap width in pixels
	int32_t width;
	// bitmap height in pixels
	int32_t height;
	// number of color planes (must be 1)
	uint16_t planes;
	// number of bits per pixel
	uint16_t bitdepth;
	// compression method
	uint32_t compression;
	// raw size of the bitmap data
	uint32_t imagesize;
	// horizontal resolution, in pixels per meter
	int32_t xresm;
	// vertical resolution, in pixels per meter
	int32_t yresm;
	// number of color indices in the color table
	uint32_t colorPalette;
	// number of color indices that are considered important, if zero all are important.
	uint32_t colorImportant;
	// red color per pixel bit mask
	uint32_t redbitmask;
	// green color per pixel bit mask
	uint32_t greenbitmask;
	// blue color per pixel bit mask
	uint32_t bluebitmask;
	// alpha color per pixel bit mask
	uint32_t alphabitmask;
	// color space
	uint32_t cstype;
	// specifies the x, y, and z coordinates of the three colors that correspond to the red, green, and blue endpoints 
	int32_t endpoint[3][3]; /* this type is wrong */
	// Tone response curve for red
	uint32_t redgamma;
	// Tone response curve for green
	uint32_t greengamma;
	// Tone response curve for blue
	uint32_t bluegamma;
} BmpInfoHeader4;

// windows nt 5.0, 98
typedef struct BmpInfoHeader5 {
	// size of the header
	uint32_t size;
	// bitmap width in pixels
	int32_t width;
	// bitmap height in pixels
	int32_t height;
	// number of color planes (must be 1)
	uint16_t planes;
	// number of bits per pixel
	uint16_t bitdepth;
	// compression method
	uint32_t compression;
	// raw size of the bitmap data
	uint32_t imagesize;
	// horizontal resolution, in pixels per meter
	int32_t xresm;
	// vertical resolution, in pixels per meter
	int32_t yresm;
	// number of color indices in the color table
	uint32_t colorPalette;
	// number of color indices that are considered important, if zero all are important
	uint32_t colorImportant;
	// red color per pixel bit mask
	uint32_t redbitmask;
	// green color per pixel bit mask
	uint32_t greenbitmask;
	// blue color per pixel bit mask
	uint32_t bluebitmask;
	// alpha color per pixel bit mask
	uint32_t alphabitmask;
	// color space
	uint32_t cstype;
	// specifies the x, y, and z coordinates of the three colors that correspond to the red, green, and blue endpoints 
	int32_t endpoint[3][3]; /* this type is wrong */
	// Tone response curve for red.
	uint32_t redgamma;
	// Tone response curve for green
	uint32_t greengamma;
	// Tone response curve for blue
	uint32_t bluegamma;
	// Rendering intent for bitmap
	uint32_t intent;
	// offset in bytes from the beginning of the header to the start of profile data
	uint32_t profiledata;
	// size of the embedded profile data
	uint32_t profilesize;
	// value should be set to zero
	uint32_t reserved;
} BmpInfoHeader5;

#endif
