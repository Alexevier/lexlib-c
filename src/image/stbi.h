#ifndef lexlib_stbi_h
#define lexlib_stbi_h
#include<lexlib/common.h>

#include<stdint.h>

LEXLIBFN uint8_t* lexlibStbImageLoad(const char* filename, int* x, int* y, int* channels);
LEXLIBFN uint8_t* lexlibStbImage16Load(const char* filename, int* x, int* y, int* channels);

LEXLIBFN uint8_t lexlibStbImageIs16(const char* filename);
LEXLIBFN int lexlibStbImageInfo(const char* filename, int* x, int* y, int* channels);

LEXLIBFN uint8_t lexlibStbImageWritePng(const char* filename, uint8_t* data, uint32_t width, uint32_t height, uint8_t channels, int stride);

#endif
