/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexlib/char.h>

/* |uint32_t               |
   |byte1|byte2|byte3|byte4| */
union LexlibCharacter {
	uint32_t chr;
	uint8_t byte[4];
};
static const union LexlibCharacter LEXLIB_CHARACTER_NULL = {.chr = LEXLIB_CHAR_NULL};

LexlibChar lexlibCharNew(const char chr[]){
	union LexlibCharacter character = LEXLIB_CHARACTER_NULL;
	if(chr[0] & 0x80){
		character.byte[0] = chr[0];
		if(chr[0] & 0x40){
			character.byte[1] = chr[1];
			if(chr[0] & 0x20){
				character.byte[2] = chr[2];
				if(chr[0] & 0x10){
					character.byte[3] = chr[3];
				}
			}
		}
		return character.chr;
	} else {
		character.byte[0] = chr[0];
		return character.chr;
	}
}

LexlibChar lexlibCharFromAscii(char ascii){
	union LexlibCharacter character = LEXLIB_CHARACTER_NULL;
	character.byte[0] = ascii;
	return character.chr;
}

LexlibChar lexlibCharFromBytes(uint8_t byte0, uint8_t byte1, uint8_t byte2, uint8_t byte3){
	union LexlibCharacter chr = {0};

	chr.byte[0] = byte0;
	if(byte0 & 0x80){
		if(byte0 & 0x40){
			chr.byte[1] = byte1;
			if(byte0 & 0x20){
				chr.byte[2] = byte2;
				if(byte0 & 0x10){
					chr.byte[3] = byte3;
				}
			}
		}
	}

	if(!lexlibCharValid(chr.chr))
		return LEXLIB_CHAR_NULL;

	return chr.chr;
}

LexlibChar lexlibCharFromMem(const void *mem){
	const uint8_t *byte = mem;

	if(byte[0] & 0x80){
		if(byte[0] & 0x40){
			if(byte[0] & 0x20){
				if(byte[0] & 0x10){
					if(byte[0] & 0x08){
						return LEXLIB_CHAR_NULL;
					}
					return lexlibCharFromBytes(byte[0], byte[1], byte[2], byte[3]);
				}
				return lexlibCharFromBytes(byte[0], byte[1], byte[2], 0);
			}
			return lexlibCharFromBytes(byte[0], byte[1], 0, 0);
		}
		return LEXLIB_CHAR_NULL;
	}

	return lexlibCharFromAscii(byte[0]);
}

char lexlibCharIntoAscii(LexlibChar chr){
	union LexlibCharacter character = {.chr = chr};
	if(character.byte[0] & 0x80)
		return '\0';
	return character.byte[0];
}

void lexlibCharIntoBytes(LexlibChar chr, uint8_t bytes[4]){
	union LexlibCharacter character = {.chr = chr};
	bytes[0] = character.byte[0];
	bytes[1] = character.byte[1];
	bytes[2] = character.byte[2];
	bytes[3] = character.byte[3];
}

LexlibBool lexlibCharIsAscii(LexlibChar chr){
	union LexlibCharacter character = {.chr = chr};
	if(character.byte[0] & 0x80)
		return LEXLIB_FALSE;
	return LEXLIB_TRUE;
}

uint8_t lexlibCharSize(LexlibChar chr){
	union LexlibCharacter character = {.chr = chr};
	if(character.byte[0] & 0x80){
		if(character.byte[0] & 0x40 && !(character.byte[0] & 0x20)){
			return 2;
		}
		if(character.byte[0] & 0x20 && !(character.byte[0] & 0x10)){
			return 3;
		}
		if(character.byte[0] & 0x10 && !(character.byte[0] & 0x08)){
			return 4;
		}
	}
	return 1;
}

LexlibBool lexlibCharValid(LexlibChar chr){
	const uint8_t *byte = (const uint8_t *)&chr;

	if(!byte[0])
		return LEXLIB_FALSE;

	/* this is nice */
	if(byte[0] & 0x80){
		if(byte[0] & 0x40){
			if(!(byte[1] & 0x80))
				return LEXLIB_FALSE;
			if(byte[1] & 0x40)
				return LEXLIB_FALSE;
			if(byte[0] & 0x20){
				if(!(byte[2] & 0x80))
					return LEXLIB_FALSE;
				if(byte[2] & 0x40)
					return LEXLIB_FALSE;
				if(byte[0] & 0x10){
					if(!(byte[3] & 0x80))
						return LEXLIB_FALSE;
					if(byte[3] & 0x40)
						return LEXLIB_FALSE;
					if(byte[0] & 0x08)
						return LEXLIB_FALSE;
					return LEXLIB_TRUE;
				}
			}
			return LEXLIB_TRUE;
		}
		return LEXLIB_FALSE;
	}

	return LEXLIB_TRUE;
}
