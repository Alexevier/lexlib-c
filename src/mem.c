/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#include<lexlib/mem.h>
#include<stddef.h>
#include<stdint.h>

LexlibBool lexlibMemCompare(const void *LEXLIB_RESTRICT mem1v, const void *LEXLIB_RESTRICT mem2v, size_t size){
	const uint8_t *mem1 = mem1v;
	const uint8_t *mem2 = mem2v;

	/* TODO check bigger batches at once */
	for(size_t i = 0; i < size; i++){
		if(mem1[i] != mem2[i])
			return LEXLIB_FALSE;
	}

	return LEXLIB_TRUE;
}
