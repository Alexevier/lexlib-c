// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifdef __unix__
#	define _POSIX_SOURCE
#	include<unistd.h>
#	include<fcntl.h>
#	include<sys/stat.h>
#endif

#include<lexlib/str.h>

#include<stdlib.h>
#include<string.h>
#include<stdarg.h>
#include<stdio.h>

#ifdef _WIN32
#	define SEP '\\'
#else
#	define SEP '/'
#endif

#if __GNUC__
	__attribute__((alias("lexlibStrCopy"))) char *lexlibStrNew(const char *str);
#else
	char *lexlibStrNew(const char *str){return lexlibStrCopy(str);}
#endif

char *lexlibStrCat(const char *str1, const char *str2){
	/* meow
	      /\_/\
	     ( o.o )
	      > ^ <
	*/
	size_t len1 = strlen(str1);
	size_t len2 = strlen(str2);
	size_t len = len1 + len2 + 1;

	char *str = malloc(len * sizeof(char));
	if(!str)
		return NULL;

	memcpy(str, str1, len1);
	memcpy(str+len1, str2, len2);

	str[len-1] = '\0';

	return str;
}

char *lexlibStrAppend(char *str1, const char *str2){
	size_t size1 = lexlibStrSize(str1)-1; /* the -1 removes the null char */
	size_t size2 = lexlibStrSize(str2);
	size_t size = size1 + size2;

	char *str = realloc(str1, size);
	if(!str)
		return NULL;

	memcpy(str+size1, str2, size2);

	return str;
}

LexlibBool lexlibStrCompare(const char *LEXLIB_RESTRICT str1, const char *LEXLIB_RESTRICT str2){
	// TODO make check locally
	int len1 = lexlibStrLen(str1);
	if(len1 == -1 || len1 == 0)
		return LEXLIB_FALSE;
	int len2 = lexlibStrLen(str2);
	if(len2 == -1 || len2 == 0)
		return LEXLIB_FALSE;
	if(len1 != len2)
		return LEXLIB_FALSE;

	return (LexlibBool)(memcmp(str1, str2, lexlibStrSize(str1)) == 0);
}

int lexlibStrLen(const char *str){
	int len = 0;

	while(*str != '\0'){
		len++;

		/* this is ok */
		if(str[0] & 0x80){
			if(str[0] & 0x40){
				if(str[0] & 0x20){
					if(str[0] & 0x10){
						if(!(str[1] & 0x80) || str[1] & 0x40 ||
							!(str[2] & 0x80) || str[2] & 0x40 ||
							!(str[3] & 0x80) || str[3] & 0x40
						) return -1;
						str+=4;
						continue;
					}
					if(!(str[1] & 0x80) || str[1] & 0x40 ||
						!(str[2] & 0x80) || str[2] & 0x40
					) return -1;
					str+=3;
					continue;
				}
				if(!(str[1] & 0x80) || str[1] & 0x40)
					return -1;
				str+=2;
				continue;
			}
			return -1;
		}

		str++;
	}

	return len;
}

size_t lexlibStrSize(const char *str){
	size_t size = 0;

	while(*str != '\0'){
		size++;
		str++;
	}

	return size+1;
}

char *lexlibStrCopy(const char *str){
	char *new = NULL;
	size_t len = strlen(str) + 1; // will include the null char

	new = malloc(len * sizeof(char));
	if(!new)
		return NULL;

	memcpy(new, str, len);

	return new;
}

char *lexlibStrFile(const char *filename){
	char *buffer = NULL;
	unsigned long length = 0;

	FILE *file = fopen(filename, "r");
	if(!file){
		return NULL;
	}

#ifdef __unix__
	struct stat info;
	if(fstat(fileno(file), &info) != 0){
		fclose(file);
		return NULL;
	}
	length = info.st_size;
#else
	if(fseek(file, 0, SEEK_END) != 0){
		fclose(file);
		return NULL;
	};
	length = ftell(file);
	if(fseek(file, 0, SEEK_SET) != 0){
		fclose(file);
		return NULL;
	};
#endif

	buffer = malloc(length+1);
	if(!buffer){
		fclose(file);
		return NULL;
	}
	buffer[length] = '\0';

	if(fread(buffer, sizeof(char), length, file) != length){
		free(buffer);
		fclose(file);
		return NULL;
	}

	fclose(file);
	return buffer;
}

char *lexlibStrFormat(const char *fmt, ...){
	va_list args;

	va_start(args, fmt);
	int size = vsnprintf(NULL, 0, fmt, args);
	va_end(args);

	if(size < 0)
		return NULL;

	char *str = malloc(size+1);
	if(!str)
		return NULL;

	va_start(args, fmt);
	vsnprintf(str, size+1, fmt, args);
	va_end(args);

	return str;
}

char *lexlibStrPathNew(const char *str){
	size_t len = strlen(str);
	char *path = calloc(len+1, sizeof(char));

	if(!path)
		return NULL;

	for(size_t i = 0; i < len; i++){
		char chr = str[i];
		if(chr == '/' || chr == '\\')
			chr = SEP;
		path[i] = chr;
	}

	return path;
}

uint8_t lexlibStrPathPush(char **str, const char *add){
	size_t strLen = strlen(*str);
	size_t addLen = strlen(add);
	size_t len = strLen + addLen;

	if(strLen == 0 || addLen == 0)
		return LEXLIB_INVALID_VALUE;

	char *path = realloc(*str, (len+2) * sizeof(char));
	char *offPath = path+strLen;

	if(!path)
		return LEXLIB_OUT_OF_MEMORY;

	if(path[strLen-1] != '/' && path[strLen-1] != '\\'){
		offPath++;
		path[strLen] = SEP;
	}

	for(size_t i = 0; i < addLen; i++){
		char chr = add[i];
		if(chr == '/' || chr == '\\'){
			if(i == 0){
				offPath--;
			}
			chr = SEP;
		}
		offPath[i] = chr;
	}

	offPath[addLen] = '\0';

	// switch the string and return.
	*str = path;
	return LEXLIB_OK;
}

uint8_t lexlibStrPathPop(char **str){
	size_t len = 0;
	char *fst;
	char *lst;

	// get dividers pos
	fst = strchr(*str, '/');
	if(!fst)
		fst = strchr(*str, '\\');
	lst = strrchr(*str, '/');
	if(!lst)
		lst = strrchr(*str, '\\');
	if(!fst || !lst)
		return LEXLIB_INVALID_OPERATION;
	if(fst == lst)
		return LEXLIB_INVALID_OPERATION;

	len = strlen(lst);
	memset(lst, 0, len);

	return LEXLIB_OK;
}

uint8_t lexlibStrPathAsDir(char **str){
	size_t len = strlen(*str);

	if(len == 0)
		return LEXLIB_INVALID_VALUE;

	if((*str)[len-1] == '/' || (*str)[len-1] == '\\')
		return LEXLIB_OK;

	char *new = realloc(*str, (len+2) * sizeof(char));
	if(!new)
		return LEXLIB_OUT_OF_MEMORY;
	new[len+1] = '\0';

	new[len] = SEP;

	*str = new;
	return LEXLIB_OK;
}

uint8_t lexlibStrPathAsFile(char **str){
	size_t len = strlen(*str);

	if(len == 0)
		return LEXLIB_INVALID_VALUE;

	len--;

	if((*str)[len] == '/' || (*str)[len] == '\\')
		(*str)[len] = '\0';
	else
		return LEXLIB_OK;

	return LEXLIB_OK;
}

/* this is here to not add a new source file just for this. */
#ifndef VERSION
#	define VERSION "0.0.0"
#endif
const char *lexlibVersion(void){
	static const char version[] = VERSION;
	return version;
}

/* https://ascii.co.uk/art/cats */
