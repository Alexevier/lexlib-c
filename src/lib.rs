/*
Copyright (C) 2023 alexevier <alexevier@proton.me>

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.

*/

//! Miscellaneous stuff for Rust and C development.<br>
//! Contains small things that could be useful in some regular basis.
//!
//! Some parts from lexlib are not wrapped/ignored if a better/native option is available in the [`std`] library.
//!
//! NOTE: currently being made compatible with Rust.

pub mod c;
pub mod color;
pub mod image;
pub mod mem;
pub mod os;
pub mod time;
