#include<lexlib/defines.h>
#include<lexlib/vec.h>
#include<lexlib/str.h>
#include<lexlib/io.h>
#include<stdlib.h>
#include<stddef.h>
#include<stdint.h>
#include<stdio.h>

struct LexlibIO {
	union {
		FILE *file;
		uint8_t *buffer;
	} data;
	uint64_t size;
	uint64_t pos;
	uint8_t mode;
	uint8_t type;
};

enum Type {
	TYPE_FILE,
	TYPE_MEMORY,
};

LexlibIO lexlibIOfile(const char *filename, uint8_t mode){
	char flags[4] = {0};

	if(!mode){
		flags[0] = 'r';
		flags[1] = 'b';
		goto doe;
	}

	if(mode & LEXLIB_RD){
		if(mode & LEXLIB_WR){
			flags[0] = (mode & LEXLIB_OVERWRITE) ? 'w' : 'r';
			flags[1] = '+';
			flags[2] = 'b';
			goto doe;
		}
		flags[0] = 'r';
		flags[1] = 'b';
		goto doe;
	}

	if(mode & LEXLIB_WR){
		flags[0] = 'w';
		flags[1] = 'b';
		goto doe;
	}

	doe:
	if(!flags[0])
		return NULL;

	FILE *file = fopen(filename, flags);
	if(!file)
		return NULL;

	LexlibIO io = malloc(sizeof(struct LexlibIO));
	io->data.file = file;
	io->size = 0;
	io->pos = 0;
	io->mode = mode;
	io->type = TYPE_FILE;

	return io;
}

LexlibIO lexlibIOmem(void *mem, size_t size){
	LexlibIO io = malloc(sizeof(struct LexlibIO));
	if(!io)
		return NULL;

	io->data.buffer = (uint8_t*)mem;
	io->size = size;
	io->pos = 0;
	io->mode = LEXLIB_RDWR;
	io->type = TYPE_MEMORY;

	return io;
}

LexlibIO lexlibIOmemConst(const void *mem, size_t size){
	LexlibIO io = malloc(sizeof(struct LexlibIO));
	if(!io)
		return NULL;

	io->data.buffer = (uint8_t*)mem;
	io->size = size;
	io->pos = 0;
	io->mode = LEXLIB_RD;
	io->type = TYPE_MEMORY;

	return io;
}

size_t lexlibIOread(LexlibIO io, void *buffer, size_t bytes){
	if(!(io->mode & LEXLIB_RD))
		return 0;
	if(io->type == TYPE_MEMORY){
		size_t i = 0;
		while(i < bytes && io->pos < io->size){
			((uint8_t*)buffer)[i] = io->data.buffer[io->pos];
			i++; io->pos++;
		}
		return i;
	}
	if(io->type == TYPE_FILE)
		return fread(buffer, 1, bytes, io->data.file);

	return 0;
}

size_t lexlibIOwrite(LexlibIO io, const void *buffer, size_t bytes){
	if(!(io->mode & LEXLIB_WR))
		return 0;
	if(io->type == TYPE_MEMORY){
		size_t i = 0;
		while(i < bytes && io->pos < io->size){
			io->data.buffer[io->pos] = (buffer)? ((uint8_t*)buffer)[i] : 0;
			i++; io->pos++;
		}
		return i;
	}
	if(io->type == TYPE_FILE){
		if(buffer)
			return fwrite(buffer, 1, bytes, io->data.file);
		uint8_t zero = 0;
		size_t i = 0;
		while(i != bytes){
			if(!fwrite(&zero, 1, 1, io->data.file))
				break;
			i++;
		}
		return i;
	}
	return 0;
}

LexlibChar lexlibIOreadChar(LexlibIO io){
	uint8_t byte[4];

	if(lexlibIOread(io, byte, sizeof(uint8_t)) != sizeof(uint8_t))
		return LEXLIB_CHAR_EOF;

	if(byte[0] & 0x80){
		if(byte[0] & 0x40){
			if(lexlibIOread(io, byte+1, sizeof(uint8_t)) != sizeof(uint8_t))
				return LEXLIB_CHAR_EOF;
			if(byte[0] & 0x20){
				if(lexlibIOread(io, byte+2, sizeof(uint8_t)) != sizeof(uint8_t))
					return LEXLIB_CHAR_EOF;
				if(byte[0] & 0x10){
					if(lexlibIOread(io, byte+3, sizeof(uint8_t)) != sizeof(uint8_t))
						return LEXLIB_CHAR_EOF;
					if(byte[0] & 0x08){
						return LEXLIB_CHAR_NULL;
					}
				}
			}
			return lexlibCharFromBytes(byte[0], byte[1], byte[2], byte[3]);
		}
		return LEXLIB_CHAR_NULL;
	}
	return lexlibCharFromAscii(byte[0]);
}

int lexlibIOreadCharc(LexlibIO io){
	unsigned char chr;
	if(lexlibIOread(io, &chr, sizeof(chr)) != sizeof(chr))
		return EOF;

	return chr;
}

LexlibString lexlibIOreadLine(LexlibIO io){
	LexlibVec(char) buffer = lexlibVecNew(256, *buffer);
	if(!buffer)
		return NULL;
	lexlibVecClear(buffer);

	char chr;
	for(;;){
		if(lexlibIOread(io, &chr, sizeof(chr)) != sizeof(chr)){
			chr = '\0';
			lexlibVecPush(buffer, &chr);
			break;
		}
		if(chr == '\0' || chr == '\n'){
			chr = '\0';
			lexlibVecPush(buffer, &chr);
			break;
		}
		lexlibVecPush(buffer, &chr);
	}

	LexlibString string = lexlibStringFromVec(buffer);
	lexlibVecDelete(buffer);
	return string;
}

char *lexlibIOreadLineStr(LexlibIO io){
	LexlibVec(char) buffer = lexlibVecNew(BUFFERSIZE, *buffer);
	if(!buffer)
		return NULL;
	lexlibVecClear(buffer);

	char chr;
	for(;;){
		if(lexlibIOread(io, &chr, sizeof(chr)) != sizeof(chr)){
			chr = '\0';
			lexlibVecPush(buffer, &chr);
			break;
		}
		if(chr == '\0' || chr == '\n'){
			chr = '\0';
			lexlibVecPush(buffer, &chr);
			break;
		}
		lexlibVecPush(buffer, &chr);
	}

	char *str = lexlibStrCopy(buffer);
	lexlibVecDelete(buffer);
	return str;
}

uint8_t lexlibIOsetpos(LexlibIO io, uint64_t pos){
	if(io->type == TYPE_FILE){
		if(fseek(io->data.file, pos, SEEK_SET) != 0)
			return LEXLIB_INVALID_OPERATION;
		return LEXLIB_OK;
	}

	if(pos >= io->size)
		return LEXLIB_INVALID_OPERATION;
	io->pos = pos;

	return LEXLIB_OK;
}

uint64_t lexlibIOgetpos(LexlibIO io){
	if(io->type == TYPE_FILE)
		io->pos = ftell(io->data.file);
	return io->pos;
}

void lexlibIOflush(LexlibIO io){
	if(io->type == TYPE_FILE)
		fflush(io->data.file);
}

void lexlibIOclose(LexlibIO io){
	if(!io)
		return;
	if(io->type == TYPE_FILE)
		fclose(io->data.file);

	free(io);
}
