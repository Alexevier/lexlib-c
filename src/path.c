#include "lexlib/defines.h"
#include<lexlib/path.h>
#include<lexlib/str.h>
#include<stdlib.h>
#include<string.h>
#include<math.h>

#ifdef _WIN32
#	define SEPARATOR '\\'
#else
#	define SEPARATOR '/'
#endif

char *lexlibPathNew(const char *str){
	size_t size = lexlibStrSize(str);

	char *path = malloc(size);
	if(!path)
		return NULL;

	for(size_t i = 0; i < size; i++){
		char chr = str[i];
		if(chr == '/' || chr == '\\')
			chr = SEPARATOR;
		path[i] = chr;
	}

	return path;
}

uint8_t lexlibPathAdd(char **pathIn, const char *str){
	size_t pathInLen = strlen(*pathIn);
	size_t strLen = strlen(str);

	if(pathInLen == 0 || strLen == 0)
		return LEXLIB_INVALID_VALUE;

	size_t len = pathInLen + strLen;
	size_t offset = pathInLen;

	if((*pathIn)[pathInLen-1] != SEPARATOR){
		(*pathIn)[pathInLen] = SEPARATOR;
		len++;
		offset++;
	}
	if(str[0] == '/' || str[0] == '\\'){
		len--;
		offset--;
	}

	char *path = realloc(*pathIn, (len+1) * sizeof(char));
	if(!path){
		(*pathIn)[pathInLen] = '\0';
		return LEXLIB_OUT_OF_MEMORY;
	}

	for(size_t i = 0; i < strLen; i++){
		char chr = str[i];
		if(chr == '/' || chr == '\\')
			chr = SEPARATOR;
		path[i+offset] = chr;
	}

	path[len] = '\0';
	*pathIn = path;

	return LEXLIB_OK;
}

uint8_t lexlibPathPop(char **pathIn){
	char *path = *pathIn;
	long int len = strlen(path);
	long int first = -1;
	long int last = -1;

	for(long int i = 0; i < len; i++){
		if(path[i] == SEPARATOR){
			if(first == -1)
				first = i;
			last = i;
		}
	}

	if(last == -1)
		return LEXLIB_INVALID_OPERATION;

	if(path[last+1] == '\0'){
		long int i = last;
		while(i > first){
			i--;
			if(path[i] == SEPARATOR){
				last = i;
				break;
			}
		}
	}

	if(last == first)
		return LEXLIB_INVALID_OPERATION;

	len = len - (len - last) + 1;
	path = realloc(path, len+1);
	if(!path)
		return LEXLIB_OUT_OF_MEMORY;

	path[len] = '\0';
	*pathIn = path;

	return LEXLIB_OK;
}

LexlibBool lexlibPathAbsolute(const char *path){
#ifdef _WIN32
	if(!path[0] || !path[1] || !path[2])
		return LEXLIB_FALSE;

	if(path[1] != ':')
		return LEXLIB_FALSE;
	if(path[2] != '\\')
		return LEXLIB_FALSE;

	switch(path[0]){ /* ensure that it is an actual drive letter */
		case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
		case 'G': case 'H': case 'I': case 'J': case 'K': case 'L':
		case 'M': case 'N': case 'O': case 'P': case 'Q': case 'R':
		case 'S': case 'T': case 'U': case 'V': case 'W': case 'X':
		case 'Y': case 'Z': return LEXLIB_TRUE;
	}
#else
	if(path[0] == '/')
		return LEXLIB_TRUE;
#endif

	return LEXLIB_FALSE;
}

uint8_t lexlibPathAsDir(char **pathInOut){
	char *path = *pathInOut;
	size_t len = strlen(path);

	if(path[len-1] == SEPARATOR)
		return LEXLIB_OK;

	path = realloc(path, (len+2) * sizeof(char));
	if(!path)
		return LEXLIB_OUT_OF_MEMORY;

	path[len] = SEPARATOR;
	path[len+1] = '\0';
	*pathInOut = path;

	return LEXLIB_OK;
}

uint8_t lexlibPathAsFile(char **pathInOut){
	char *path = *pathInOut;
	size_t len = strlen(path);

	if(path[len-1] != SEPARATOR)
		return LEXLIB_OK;

	path = realloc(path, len);
	if(!path)
		return LEXLIB_OUT_OF_MEMORY;

	path[len-1] = '\0';
	*pathInOut = path;

	return LEXLIB_OK;
}

const char *lexlibPathExtension(const char *path){
	size_t len = strlen(path);

	while(len--){
		if(path[len] == '.')
			return path+len;
	}

	return NULL;
}
