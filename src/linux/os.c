/* Copyright 2024 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#define _GNU_SOURCE

#include<linux/limits.h>
#include<sys/sysinfo.h>
#include<sys/syscall.h>
#include<unistd.h>
#include<stddef.h>
#include<stdlib.h>
#include<string.h>

char *lexlibCurrentExecutablePath(void){
	char buf[PATH_MAX+1] = {0};
	size_t read = readlink("/proc/self/exe", buf, PATH_MAX);

	if(read == (size_t)-1)
		return NULL;
	if(read == PATH_MAX)
		return NULL;

	size_t len = strlen(buf);
	char *path = malloc(len * sizeof(*path) + 1);
	if(!path)
		return NULL;

	memcpy(path, buf, len);
	path[len] = '\0';

	return path;
}
