// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

//! color manipulation

mod color8;
mod colorf;
mod color16;

/// common color interface
pub trait Color<T> {
	fn r(&self) -> T;
	fn g(&self) -> T;
	fn b(&self) -> T;
	fn a(&self) -> T;
	/// converts the color to gray scale.
	fn gray(&self) -> T;
	/// converts the color to gray maintaining alpha.
	fn gray_alpha(&self) -> Self;
	/// premultiplies the alpha.
	fn premultiply(&self) -> Self;
	/// color blending.
	fn blend(&self, src: Self, mode: BlendMode) -> Self;

	// min value the color can have.
	const MIN: T;
	// max value the color can have.
	const MAX: T;

	const BLACK: Self;
	const WHITE: Self;
	const RED: Self;
	const GREEN: Self;
	const BLUE: Self;
	const YELLOW: Self;
	const MAGENTA: Self;
	const CYAN: Self;
}

/// blend modes.
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum BlendMode {
	/// No blending.<br>
	/// `res.rgba = src.rgba`
	None = 0x00,
	/// Additive blending.<br>
	/// `res.rgb = dst.rgb + (src.rgb * src.a)`<br>
	/// `res.a = dst.a`
	Add = 0x01,
	/// Subtractive blending.<br>
	/// `res.rgb = dst.rgb - (src.rgb * src.a)`<br>
	/// `res.a = dst.a`
	Sub = 0x02,
	/// Multiplicative blending.<br>
	/// `res.rgb = (src.rgb * dst.rgb) + (dst.rgb * (1 - src.a))`<br>
	/// `res.a = (src.a * dst.a) + (dst.a * (1 - src.a))`
	Mul = 0x04,
	/// Modulation.<br>
	/// `res.rgb = src.rgb * dst.rgb`<br>
	/// `res.a = dst.a`
	Mod = 0x10,
	/// Alpha blending.<br>
	/// `res.rgb = (src.rgb * src.a) + (dst.rgb * (1 - src.a))`<br>
	/// `res.a = dst.a * (1 - src.a)`
	Blend = 0x80,
}

/// Color profiles.
#[repr(u8)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Profile {
	/// Gray scale.
	Gray = 0x01,
	/// Gray scale with alpha.
	GrayAlpha,
	/// Standard: Red Green Blue.
	RGB,
	/// Standard: Red Green Blue Alpha.
	RGBA,
	/// R.G.B.A.X: 5.5.5.0.1
	RGB555,
	/// R.G.B.A.X: 5.6.5.0.0
	RGB565,
}

/// 8 bits per color
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color8 {
	pub r: u8,
	pub g: u8,
	pub b: u8,
	pub a: u8,
}

/// 16 bits per color
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct Color16 {
	pub r: u16,
	pub g: u16,
	pub b: u16,
	pub a: u16,
}

/// floating point color<br>
/// the range is 0..1
#[repr(C)]
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct ColorF {
	pub r: f32,
	pub g: f32,
	pub b: f32,
	pub a: f32,
}
