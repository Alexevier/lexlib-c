/* Copyright 2023-2024 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifdef __unix__
#	define _POSIX_C_SOURCE 199309L
#	define _GNU_SOURCE
#endif

#include<lexlib/filesystem.h>
#include<lexlib/str.h>

#include<stdlib.h>

#ifdef __unix__
#	include<sys/stat.h>
#	include<dirent.h>
#	include<errno.h>
#elif defined(_WIN32)
#	include<lexlib/path.h>
#	include<windows.h>
#endif

uint8_t lexlibFsCreateDir(const char *path){
	int errnoOg = errno;

#if defined(__unix__)
	if(mkdir(path, 0777) == -1){
		int errnoC = errno;
		errno = errnoOg;
		switch(errnoC){
			case EEXIST:
				return LEXLIB_EXIST;
			case ENOENT:
			case ENOTDIR:
				return LEXLIB_INVALID_PATH;
			case EPERM:
				return LEXLIB_INVALID_OPERATION;
			default:
				return LEXLIB_ERROR;
		}
	}
#elif defined(_WIN32)
	if(CreateDirectory(path, NULL) == 0){
		switch(GetLastError()){
			case ERROR_ALREADY_EXISTS:
				return LEXLIB_EXIST;
			case ERROR_PATH_NOT_FOUND:
				return LEXLIB_INVALID_PATH;
			default:
				return LEXLIB_ERROR;
		}
	}
#endif

	errno = errnoOg;
	return LEXLIB_OK;
}

char *lexlibFsAbsolute(const char *path){
#if defined(__unix__)
	return realpath(path, NULL);
#elif defined(_WIN32)
	DWORD len = GetFullPathName(path, 0, NULL, NULL);
	if(len == 0)
		return NULL;

	char *fullpath = malloc(len);
	if(!fullpath)
		return NULL;

	DWORD lenr = GetFullPathName(path, len, fullpath, NULL);
	if((len-1) != lenr){
		free(fullpath);
		return NULL;
	}

	return fullpath;
#endif
}

int64_t lexlibFsSize(const char *path){
#if defined(__unix__)
	struct stat info;
	int state = stat(path, &info);
	if(state != 0)
		return -1;

	return (int64_t)info.st_size;
#elif defined(__WIN32)
	HANDLE handle = CreateFileA(path, GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	if(handle == INVALID_HANDLE_VALUE)
		return -1;

	LARGE_INTEGER size;
	DWORD state = GetFileSizeEx(handle, &size);
	if(state == 0)
		return -1;

	return (int64_t)size.QuadPart;
#endif
}

uint8_t lexlibFsList(struct LexlibFsList *list, const char *path){
	list->count = 0;
	uint8_t error = LEXLIB_OK;

#if defined(__unix__)
	DIR *dir = opendir(path);
	if(!dir)
		return LEXLIB_INVALID_PATH;
#elif defined(_WIN32)
	WIN32_FIND_DATA fdata;
	char *pathn = lexlibStrCopy(path);
	if(!pathn)
		return LEXLIB_OUT_OF_MEMORY;
	if((error = lexlibPathAdd(&pathn, "/*")) != LEXLIB_OK){
		free(pathn);
		return error;
	};
	HANDLE search = FindFirstFile(pathn, &fdata); // TODO check error
	free(pathn);
#endif

#if defined(__unix__)
	for(;;){
#elif defined(_WIN32)
	do{
#endif
	#if defined(__unix__)
		struct dirent* dire = readdir(dir);
		if(!dire)
			break;
	#endif

		const char *name =
		#if defined(__unix__)
			dire->d_name;
		#elif defined(_WIN32)
			fdata.cFileName;
		#endif

		if(lexlibStrCompare(name, "."))
			continue;
		if(lexlibStrCompare(name, ".."))
			continue;

		if(!list->element){
			list->element = malloc(sizeof(*list->element));
		} else {
			struct LexlibFsListElement_ *newlist = realloc(list->element, (list->count+1) * sizeof(*list->element));
			if(!newlist){
				error = LEXLIB_OUT_OF_MEMORY;
				goto end;
			}
			list->element = newlist;
		}

	#if defined(__unix__)
		switch(dire->d_type){
			case DT_REG:
				list->element[list->count].type = LEXLIB_FILE;
				break;
			case DT_DIR:
				list->element[list->count].type = LEXLIB_DIRECTORY;
				break;
			default:
				list->element[list->count].type = LEXLIB_NONE;
				break;
		}
	#endif

		list->element[list->count].name = lexlibStrCopy(name);
		list->count++;
	}
	#if defined(_WIN32)
		while(FindNextFile(search, &fdata) != 0);
		// TODO check error
	#endif

	end:
#if defined(__unix__)
	closedir(dir);
#elif defined(_WIN32)
	FindClose(search);
#endif
	return error;
}

void lexlibFsListFree(struct LexlibFsList *list){
	for(size_t i = 0; i < list->count; i++)
		free(list->element[i].name);
	free(list->element);
}
