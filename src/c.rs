// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

//! Direct interface with the C part.
//!
//! this is not stable and is subject to changes.

use crate::color::{Color8, ColorF, Color16};
use crate::image::Image;
use crate::time;
use core::ffi::c_void;
use core::ffi::c_char;
use core::ffi::c_uchar;
use core::ffi::c_int;
use core::ffi::c_uint;



// cfile.h //

extern "C" {
	pub fn lexlibCFileMap(file: *mut libc::FILE) -> *mut c_void;
	pub fn lexlibCFileMode(file: *mut libc::FILE) -> u8;
	pub fn lexlibCFileSize(file: *mut libc::FILE) -> i64;
	pub fn lexlibCFileType(file: *mut libc::FILE) -> u16;
}

// ..cfile.h.. //



// color.h //

// modes
// LEXLIB_NONE is defined in defines.h
pub const LEXLIB_ADD: u8 = 0x01;
pub const LEXLIB_SUB: u8 = 0x02;
pub const LEXLIB_MUL: u8 = 0x04;
pub const LEXLIB_DIV: u8 = 0x08;
pub const LEXLIB_MOD: u8 = 0x10;
pub const LEXLIB_BLEND: u8 = 0x80;

// limits
pub const LEXLIB_COLOR_MIN: u8 = 0x00;
pub const LEXLIB_COLOR_MAX: u8 = 0xFF;
pub const LEXLIB_COLORF_MIN: f32 = 0.0;
pub const LEXLIB_COLORF_MAX: f32 = 1.0;
pub const LEXLIB_COLOR16_MIN: u16 = 0x0000;
pub const LEXLIB_COLOR16_MAX: u16 = 0xFFFF;

extern "C" {
	// performs a color blend depending on the mode.
	// if mode is invalid LEXLIB_ADD is performed.
	pub fn lexlibColorBlend(dst: Color8, src: Color8, mode: u8) -> Color8;
	pub fn lexlibColorFBlend(dst: ColorF, src: ColorF, mode: u8) -> ColorF;
	pub fn lexlibColor16Blend(dst: Color16, src: Color16, mode: u8) -> Color16;

	// convert to gray scale, premultiplies the alpha.
	pub fn lexlibColorGray(color: Color8) -> u8;
	pub fn lexlibColorFGray(color: ColorF) -> f32;
	pub fn lexlibColor16Gray(color: Color16) -> u16;

	// convert to gray scale preserves the alpha.
	pub fn lexlibColorGrayAlpha(color: Color8) -> Color8;
	pub fn lexlibColorFGrayAlpha(color: ColorF) -> ColorF;
	pub fn lexlibColor16GrayAlpha(color: Color16) -> Color16;

	// premultiply alpha.
	pub fn lexlibColorPremultiply(color: Color8) -> Color8;
	pub fn lexlibColorFPremultiply(color: ColorF) -> ColorF;
	pub fn lexlibColor16Premultiply(color: Color16) -> Color16;

	// clamps a LexlibColorF to be in the range  0..1
	pub fn lexlibColorFClamp(color: ColorF) -> ColorF;

	// convertions from Color
	pub fn lexlibColorToF(color: Color8) -> ColorF;
	pub fn lexlibColorTo16(color: Color8) -> Color16;

	// convertions from ColorF
	pub fn lexlibColorFTo8(color: ColorF) -> Color8;
	pub fn lexlibColorFTo16(color: ColorF) -> Color16;

	// convertions from Color16
	pub fn lexlibColor16To8(color: Color16) -> Color8;
	pub fn lexlibColor16ToF(color: Color16) -> ColorF;
}

// ..color.h.. //



// defines.h //

/// represents a true or false type explicitly.
pub type LexlibBool = c_uchar;
pub const LEXLIB_TRUE: u8 = 1;
pub const LEXLIB_FALSE: u8 = 0;

// misc
pub const LEXLIB_NONE: u8 = 0x00;

// errors : limited to u8 range.
pub const LEXLIB_OK: u8 = 0x00;
pub const LEXLIB_INVALID_VALUE: u8 = 0xB0;
pub const LEXLIB_INVALID_OPERATION: u8 = 0xB1;
pub const LEXLIB_INVALID_REQUEST: u8 = 0xB2;
pub const LEXLIB_INVALID_ARG: u8 = 0xB3;
pub const LEXLIB_INVALID_DIR: u8 = 0xB4;
pub const LEXLIB_INVALID_LEN: u8 = 0xB5;
pub const LEXLIB_INVALID_MSG: u8 = 0xB6;
pub const LEXLIB_INVALID_SIZE: u8 = 0xB7;
pub const LEXLIB_INVALID_FILE: u8 = 0xB8;
pub const LEXLIB_INVALID_PATH: u8 = 0xB9;
pub const LEXLIB_INVALID_NAME: u8 = 0xBA;
pub const LEXLIB_INVALID_TYPE: u8 = 0xBB;
pub const LEXLIB_INVALID_CHAR: u8 = 0xBC;
pub const LEXLIB_INVALID_DATA: u8 = 0xBD;
pub const LEXLIB_CORRUPT_DATA: u8 = 0xC0;
pub const LEXLIB_EOF: u8 = 0xC1;
pub const LEXLIB_EXIST: u8 = 0xFF;
pub const LEXLIB_CANT_OPEN: u8 = 0xC3;
pub const LEXLIB_CANT_CLOSE: u8 = 0xC4;
pub const LEXLIB_CANT_READ: u8 = 0xC5;
pub const LEXLIB_CANT_WRITE: u8 = 0xC6;
pub const LEXLIB_PARTIAL_READ: u8 = 0xC7;
pub const LEXLIB_PARTIAL_WRITE: u8 = 0xC8;
pub const LEXLIB_RESET: u8 = 0xE0;
pub const LEXLIB_ABORT: u8 = 0xE1;
pub const LEXLIB_BUSY: u8 = 0xE2;
pub const LEXLIB_DENIED: u8 = 0xE3;
pub const LEXLIB_CANCEL: u8 = 0xE4;
pub const LEXLIB_BROKEN: u8 = 0xE5;
pub const LEXLIB_BLOCKING: u8 = 0xE6;
pub const LEXLIB_UNAVAILABLE: u8 = 0xE7;
pub const LEXLIB_INTERRUPTED: u8 = 0xE8;
pub const LEXLIB_LIMIT: u8 = 0xE9;
pub const LEXLIB_NOT_FOUND: u8 = 0xEA;
pub const LEXLIB_UNSUPORTED: u8 = 0xF0;
pub const LEXLIB_OUT_OF_DISK: u8 = 0xFD;
pub const LEXLIB_OUT_OF_MEMORY: u8 = 0xFE;
pub const LEXLIB_ERROR: u8 = 0xFF;

// access permissions
pub const LEXLIB_RD: u8 = 0x01;
pub const LEXLIB_WR: u8 = 0x02;
pub const LEXLIB_RDWR: u8 = 0x03;
pub const LEXLIB_APPEND: u8 = 0x04;
pub const LEXLIB_CREATE: u8 = 0x08;
pub const LEXLIB_OVERWRITE: u8 = 0x10;

/// different file types recognised by lexlib.
#[repr(C)]
#[allow(non_camel_case_types)]
pub enum LexlibFileType {
	LEXLIB_FILETYPE_UNKNOWN = 0,
	LEXLIB_FILETYPE_BMP,
	LEXLIB_FILETYPE_JPG,
	LEXLIB_FILETYPE_PNG,
	LEXLIB_FILETYPE_SCRIPT,
}

// ..defines.h.. //



// image.h //
pub const LEXLIB_FLIP_X: u8 = 0x01;
pub const LEXLIB_FLIP_Y: u8 = 0x02;
pub const LEXLIB_ALPHA: u8 = 0x80;

pub const LEXLIB_BMP_CORE: u8 = 0x0C;
pub const LEXLIB_BMP_INFO: u8 = 0x28;
pub const LEXLIB_BMP_INFO_V3: u8 = 0x38;

extern "C" {
	pub fn lexlibImageNew(image: *mut Image, width: u32, height: u32, colorProfile: u8, bpc: u8) -> u8;
	pub fn lexlibImageDelete(image: *mut Image);
	pub fn lexlibImageCopy(copy: *mut Image, original: *const Image) -> u8;
	pub fn lexlibImageValidate(image: *const Image) -> u8;
	pub fn lexlibImageFlip(image: *mut Image, flags: u8) -> u8;
	pub fn lexlibImageProfileChange(image: *mut Image, profile: u8) -> u8;
	pub fn lexlibImageFillArea(image: *mut Image, x: u32, y: u32, w: u32, h: u32, color: Color8, blendmode: u8) -> u8;
	pub fn lexlibImagePixel(image: *const Image, x: u32, y: u32, color: *mut Color8) -> u8;
	pub fn lexlibImagePixel16(image: *const Image, x: u32, y: u32, color: *mut Color16) -> u8;
	pub fn lexlibImagePixelSet(image: *mut Image, x: u32, y: u32, color: Color8, blendmode: u8) -> u8;
	pub fn lexlibImagePixel16Set(image: *mut Image, x: u32, y: u32, color: Color16, blendmode: u8) -> u8;
	pub fn lexlibImageLoad(image: *mut Image, filename: *const c_char) -> u8;
	pub fn lexlibImageSave(image: *const Image, filename: *const c_char) -> u8;
	pub fn lexlibImageLoadBmp(image: *mut Image, filename: *const c_char) -> u8;
	pub fn lexlibImageLoadBmpMem(image: *mut Image, mem: *const c_void, size: usize) -> u8;
	pub fn lexlibImageSaveBmp(image: *const Image, filename: *const c_char) -> u8;
	pub fn lexlibImageSaveBmpEx(image: *const Image, filename: *const c_char, profile: u8, header: u8) -> u8;
	pub fn lexlibImageLoadPng(image: *mut Image, filename: *const c_char) -> u8;
	pub fn lexlibImageSavePng(image: *const Image, filename: *const c_char) -> u8;
	#[cfg(feature = "experimental")]
	pub fn lexlibImageRepairStruct(image: *mut Image) -> u8;
}
// ..image.h.. //



// mem.h //

extern "C" {
	pub fn lexlibMemUnmap(mem: *mut c_void, size: usize);
}

// ..mem.h.. //



// os.h //

extern "C" {
	pub fn lexlibSleep(seconds: c_uint);
	pub fn lexlibSleepMs(millis: c_uint);
	pub fn lexlibRandom() -> c_uint;
	pub fn lexlibRamSize() -> u64;
}

// ..os.h.. //



// str.h //

extern "C" {
	pub fn lexlibStrCat(str1: *const c_char, str2: *const c_char) -> *mut c_char;
	pub fn lexlibStrCopy(str: *const c_char) -> *mut c_char;
	pub fn lexlibStrFile(filename: *const c_char) -> *mut c_char;
	pub fn lexlibStrPathNew(str: *const c_char) -> *mut c_char;
	pub fn lexlibStrPathPush(str: *mut *mut c_char, add: *const c_char) -> u8;
	pub fn lexlibStrPathPop(str: *mut *mut c_char) -> u8;
	pub fn lexlibStrPathAsDir(str: *mut *mut c_char) -> u8;
	pub fn lexlibStrPathAsFile(str: *mut *mut c_char) -> u8;
}

// ..str.h.. //



// time.h //

extern "C" {
	pub fn lexlibTimeNanos() -> u64;
	pub fn lexlibTimeMicros() -> u64;
	pub fn lexlibTimeMillis() -> u64;
	pub fn lexlibTimeSeconds() -> u64;

	pub fn lexlibThrdNanos() -> u64;
	pub fn lexlibThrdMicros() -> u64;
	pub fn lexlibThrdMillis() -> u64;
	pub fn lexlibThrdSeconds() -> u64;

	pub fn lexlibProcNanos() -> u64;
	pub fn lexlibProcMicros() -> u64;
	pub fn lexlibProcMillis() -> u64;
	pub fn lexlibProcSeconds() -> u64;

	pub fn lexlibTimerNew(ms: u64) -> time::Timer;
	pub fn lexlibTimerSet(timer: *mut time::Timer, ms: u64);
	pub fn lexlibTimerAdd(timer: *mut time::Timer, ms: u64);
	pub fn lexlibTimerStart(timer: *mut time::Timer);
	pub fn lexlibTimerUpdate(timer: *mut time::Timer);
	pub fn lexlibTimerDone(timer: *mut time::Timer) -> LexlibBool;
	pub fn lexlibTimerUntilDone(timer: *mut time::Timer);
	pub fn lexlibTimerFinished(timer: *const time::Timer) -> LexlibBool;
}

// ..time.h.. //



// vec.h //

extern "C" {
	pub fn lexlibVecNew_(len: usize, typeSize: usize) -> *mut c_void;
	pub fn lexlibVecDelete(vec: *mut c_void);
	pub fn lexlibVecGet(vec: *const c_void, index: usize) -> *mut c_void;
	pub fn lexlibVecPush_(vec: *mut *mut c_void, obj: *const c_void) -> c_int;
	pub fn lexlibVecPop(vec: *mut c_void);
	pub fn lexlibVecInsert_(vec: *mut *mut c_void, index: usize, obj: *mut c_void) -> c_int;
	pub fn lexlibVecRemove(vec: *mut c_void, index: usize) -> c_int;
	pub fn lexlibVecResize(vec: *mut *mut c_void, newcap: usize) -> c_int;
	pub fn lexlibVecFind(vec: *const c_void, obj: *const c_void) -> *mut c_void;
	pub fn lexlibVecSwap(vec: *mut c_void, a: usize, b: usize) -> c_int;
	pub fn lexlibVecClear(vec: *mut c_void);
	pub fn lexlibVecLen(vec: *const c_void) -> usize;
	pub fn lexlibVecCap(vec: *const c_void) -> usize;
}

// ..vec.h.. //
