// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include"misc.h"

// this is used in cfile too
uint16_t lexlibInternalFileTypeSig(const uint8_t* sig, size_t len){
	if(len == 2){
		if(// bmp
			sig[0] == 'B' &&
			sig[1] == 'M'
		) return LEXLIB_FILETYPE_BMP;

		if(// script
			sig[0] == '#' &&
			sig[1] == '!'
		) return LEXLIB_FILETYPE_SCRIPT;
	}

	if(len == 8){
		if(// png
			sig[0] == 0x89 &&
			sig[1] == 0x50 &&
			sig[2] == 0x4E &&
			sig[3] == 0x47 &&
			sig[4] == 0x0D &&
			sig[5] == 0x0A &&
			sig[6] == 0x1A &&
			sig[7] == 0x0A
		) return LEXLIB_FILETYPE_PNG;
	}

	return LEXLIB_FILETYPE_UNKNOWN;
}
