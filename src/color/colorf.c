// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#include<lexlib/color.h>
#include<lexlib/math.h>

struct LexlibColorF lexlibColorFBlend(struct LexlibColorF dst, struct LexlibColorF src, uint8_t mode){
	switch(mode){
		case LEXLIB_NONE:
			return src;
		case LEXLIB_ADD:
			dst.r = src.r * src.a + dst.r;
			dst.g = src.g * src.a + dst.g;
			dst.b = src.b * src.a + dst.b;
			return lexlibColorFClamp(dst);
		case LEXLIB_SUB:
			dst.r = dst.r - src.r * src.a;
			dst.g = dst.g - src.g * src.a;
			dst.b = dst.b - src.b * src.a;
			return lexlibColorFClamp(dst);
		case LEXLIB_MUL:{
			float alphaInv = 1.0f - src.a;
			dst.r = src.r * dst.r + dst.r * alphaInv;
			dst.g = src.g * dst.g + dst.g * alphaInv;
			dst.b = src.b * dst.b + dst.b * alphaInv;
			dst.a = src.a * dst.a + dst.a * alphaInv;
			return lexlibColorFClamp(dst);
		}
		case LEXLIB_MOD:
			dst.r = src.r * dst.r;
			dst.g = src.g * dst.g;
			dst.b = src.b * dst.b;
			return dst;
		case LEXLIB_BLEND:{
			float alphaInv = 1.0f - src.a;
			dst.r = src.r * src.a + dst.r * alphaInv;
			dst.g = src.g * src.a + dst.g * alphaInv;
			dst.b = src.b * src.a + dst.b * alphaInv;
			dst.a = src.a + dst.a * alphaInv;
			return dst;
		}
		default:
			return lexlibColorFBlend(dst, src, LEXLIB_ADD);
	}
}

float lexlibColorFGray(struct LexlibColorF color){
	return
		((color.r * 0.299f)  +
		 (color.g * 0.587f)  +
		 (color.b * 0.114f)) *
		  color.a;
}

struct LexlibColorF lexlibColorFGrayAlpha(struct LexlibColorF color){
	color.r = rintf(color.r * 0.299f + color.g * 0.587f  + color.b * 0.114f);
	color.g = color.r;
	color.b = color.r;

	return color;
}

struct LexlibColorF lexlibColorFPremultiply(struct LexlibColorF color){
	color.r = color.r * color.a;
	color.g = color.g * color.a;
	color.b = color.b * color.a;

	return color;
}

struct LexlibColorF lexlibColorFClamp(struct LexlibColorF color){
	color.r = lexlibClampf(color.r, 0.0f, 1.0f);
	color.g = lexlibClampf(color.g, 0.0f, 1.0f);
	color.b = lexlibClampf(color.b, 0.0f, 1.0f);
	color.a = lexlibClampf(color.a, 0.0f, 1.0f);
	return color;
}

struct LexlibColor lexlibColorFToColor(struct LexlibColorF color){
	return (struct LexlibColor){
		.r = (uint8_t)rintf((color.r * 255.0f)),
		.g = (uint8_t)rintf((color.g * 255.0f)),
		.b = (uint8_t)rintf((color.b * 255.0f)),
		.a = (uint8_t)rintf((color.a * 255.0f)),
	};
}

struct LexlibColor16 lexlibColorFTo16(struct LexlibColorF color){
	return (struct LexlibColor16){
		.r = (uint16_t)(color.r * 65535.0f),
		.g = (uint16_t)(color.g * 65535.0f),
		.b = (uint16_t)(color.b * 65535.0f),
		.a = (uint16_t)(color.a * 65535.0f),
	};
}
