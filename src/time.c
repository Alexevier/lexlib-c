// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#define _POSIX_C_SOURCE 199309L

#include<lexlib/time.h>
#include<lexlib/os.h>
#include<time.h>

// real time //

uint64_t lexlibTimeNanos(void){
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
    return ((uint64_t)time.tv_sec) * 1000000000 + ((uint64_t)time.tv_nsec);
}

uint64_t lexlibTimeMicros(void){
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
    return ((uint64_t)time.tv_sec) * 1000000 + ((uint64_t)time.tv_nsec) / 1000;
}

uint64_t lexlibTimeMillis(void){
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
    return ((uint64_t)time.tv_sec) * 1000 + ((uint64_t)time.tv_nsec) / 1000000;
}

uint64_t lexlibTimeSeconds(void){
	struct timespec time;
	clock_gettime(CLOCK_REALTIME, &time);
	return (uint64_t)time.tv_sec;
}

// ..real time.. //



// thread cpu time //

uint64_t lexlibThrdNanos(void){
	struct timespec time;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000000000 + ((uint64_t)time.tv_nsec);
}

uint64_t lexlThrdMicros(void){
	struct timespec time;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000000 + ((uint64_t)time.tv_nsec) / 1000;
}

uint64_t lexlibThrdMillis(void){
	struct timespec time;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000 + ((uint64_t)time.tv_nsec) / 1000000;
}

uint64_t lexlibThrdSeconds(void){
	struct timespec time;
	clock_gettime(CLOCK_THREAD_CPUTIME_ID, &time);
	return (uint64_t)time.tv_sec;
}

// ..thread cpu time.. //



// process cpu time //

uint64_t lexlibProcNanos(void){
	struct timespec time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000000000 + ((uint64_t)time.tv_nsec);
}

uint64_t lexlibProcMicros(void){
	struct timespec time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000000 + ((uint64_t)time.tv_nsec) / 1000;
}

uint64_t lexlibProcMillis(void){
	struct timespec time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time);
    return ((uint64_t)time.tv_sec) * 1000 + ((uint64_t)time.tv_nsec) / 1000000;
}

uint64_t lexlibProcSeconds(void){
	struct timespec time;
	clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &time);
	return (uint64_t)time.tv_sec;
}

// ..process cpu time.. //



// timer //

struct LexlibTimer lexlibTimerNew(uint64_t ms){
	return (struct LexlibTimer){
		.time = ms,
		.left = ms,
		.last = 0
	};
}

void lexlibTimerSet(struct LexlibTimer* timer, uint64_t ms){
	timer->time = ms;
}

void lexlibTimerAdd(struct LexlibTimer* timer, uint64_t ms){
	timer->time += ms;
	timer->left += ms;
}

void lexlibTimerStart(struct LexlibTimer* timer){
	timer->left = timer->time;
	timer->last = lexlibTimeMillis();
}

void lexlibTimerUpdate(struct LexlibTimer* timer){
	if(!timer->left)
		return;

	uint64_t newtime = lexlibTimeMillis();
	uint64_t diff = newtime - timer->last;

	if(diff > timer->left)
		timer->left = 0;
	else
		timer->left -= diff;

	timer->last = newtime;
}

LexlibBool lexlibTimerDone(struct LexlibTimer* timer){
	lexlibTimerUpdate(timer);
	if(timer->left)
		return LEXLIB_FALSE;
	return LEXLIB_TRUE;
}

void lexlibTimerUntilDone(struct LexlibTimer* timer){
	lexlibTimerUpdate(timer);
	lexlibSleepMs(timer->left);
	timer->left = 0;
}

LexlibBool lexlibTimerFinished(const struct LexlibTimer* timer){
	if(timer->left)
		return LEXLIB_FALSE;
	return LEXLIB_TRUE;
}

// ..timer.. //
