// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_internal_misc_h
#define lexlib_internal_misc_h

#include<lexlib/common.h>
#include<stddef.h>
#include<stdint.h>

LEXLIBFN uint16_t lexlibInternalFileTypeSig(const uint8_t* sig, size_t len);

#endif
