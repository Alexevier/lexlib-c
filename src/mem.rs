// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

//! memory.

use crate::c;
use core::ffi::c_void;

/// unmaps memory mapped by lexlib; used for mapped files
pub unsafe fn unmap(mem: *mut u8, size: usize){
	c::lexlibMemUnmap(mem as *mut c_void, size);
}
