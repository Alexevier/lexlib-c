## 2.3.0
+ lexlibFsSize().
+ lexlibVec2Rotate().
+ lexlibStrAppend().
+ lexlibStrFormat().
+ lexlibCurrentExecutablePath().
+ generic vector macro
+ generic vector: new().
+ generic vector: push().
+ generic vector: free().
+ generic vector: pop().
+ generic vector: pushPtr().
+ generic vector: remove().


## 2.2.0 : the IO & UTF8 update
+ added io.h.
+ added char.h.
+ added string.h.
+ opaque type LexlibIO.
+ opaque type LexlibString.
+ type LexlibChar.
+ deprecated lexlibImageLoadBmp().
+ deprecated lexlibImageLoadBmpMem().
+ changed lexlibImageSaveBmp().
+ changed lexlibImageLoadBmp().
+ removed lexlibImageSaveBmpEx().
+ removed lexlibImageLoadBmpMem().
+ lexlibVersion().
+ lexlibIOfile().
+ lexlibIOmem().
+ lexlibIOmemconst().
+ lexlibIOread().
+ lexlibIOwrite().
+ lexlibIOreadLine().
+ lexlibIOreadLineStr().
+ lexlibIOsetpos().
+ lexlibIOgetpos().
+ lexlibIOflush().
+ lexlibIOclose().
+ lexlibCharNew().
+ lexlibCharFromAscii().
+ lexlibCharFromBytes().
+ lexlibCharIntoAscii().
+ lexlibCharIntoBytes().
+ lexlibCharIsAscii().
+ lexlibCharSize().
+ lexlibCharValid().
+ lexlibStringNew().
+ lexlibStringDelete().
+ lexlibStringCopy().
+ lexlibStringFromC().
+ lexlibStringFromVec().
+ lexlibStringChar().
+ lexlibStringAppend().
+ lexlibStringAppendStr().
+ lexlibStringCompare().
+ lexlibStringCompareStr().
+ lexlibStringLen().
+ lexlibStringSize().
+ lexlibStringValid().
+ lexlibStringIsAscii().
+ lexlibStringRaw().
+ lexlibStringRawMut().
+ lexlibMemCompare().
+ lexlibPathExtension().

## 2.1.0
+ struct LexlibRect.
+ lexlibPathNew().
+ lexlibPathAdd().
+ lexlibPathPop().
+ lexlibPathAbsolute().
+ lexlibPathAsDir().
+ lexlibPathAsFile().
+ lexlibStrCompare().
+ deprecated lexlibStrPathNew().
+ deprecated lexlibStrPathPush().
+ deprecated lexlibStrPathPop().
+ deprecated lexlibStrPathAsDir().
+ deprecated lexlibStrPathAsFile().

## 2.0.1
+ fix LEXLIB_VEC2I/3I_ZERO.
+ fix cfile mode in windows.
+ LexlibVec(T).
+ lexlibStrNew().

## 2.0.0
+ changed license to zlib.
+ rust bindings and wrappers.
+ gray_scale with alpha support.
+ structs are not typedef'd anymore.
+ SIMD support.
+ reworked Vec4 math.
+ reworked image loading.
+ LexlibColorFlt -> LexlibColorF.
+ LexlibImage.depth -> LexlibImage.bpc.
+ lexlibImagePixelGet() -> lexlibImagePixel().
+ lexlibImagePixel16Get() -> lexlibImagePixel16().
+ lexlibMillis() -> lexlibTimeMillis().
+ lexlibSeconds() -> lexlibTimeSeconds().
+ lexlibStrcat() -> lexlibStrCat().
+ added typedef.h.
+ added vec.h (growable array).
+ removed path.
+ removed aabb.
+ removed LexlibBytes.
+ removed lexlibFileToString().
+ removed lexlibStringToFile().
+ removed lexlibFileBytes().
+ removed lexlibPrintTimed().
+ removed lexlibVersion().
+ removed LEXLIB_UNREACHABLE.
+ LexlibBool.
+ lexlibStrFile().
+ LEXLIB_STRING().
+ lexlibCFileMap().
+ lexlibCFileMode().
+ lexlibCFileSize().
+ lexlibCFileType().
+ lexlibColorGrayAlpha().
+ lexlibTimeNanos().
+ lexlibTimeMicros().
+ lexlibThrdNanos().
+ lexlibThrdMicros().
+ lexlibThrdMillis().
+ lexlibThrdSeconds().
+ lexlibProcNanos().
+ lexlibProcMicros().
+ lexlibProcMillis().
+ lexlibProcSeconds().
+ lexlibTimerAdd().
+ LEXLIB_CLAMP().
+ lexlibRad().
+ lexlibDeg():
+ lexlibFloatCmp().
+ lexlibMemUnmap().
+ lexlibMemReverse16().
+ lexlibMemReverse32().
+ lexlibMemReverse64().
+ lexlibImageCopy().
+ lexlibImageFillArea().
+ lexlibImageLoadBmpMem().
+ lexlibVecNew().
+ lexlibVecDelete().
+ lexlibVecGet().
+ lexlibVecPush().
+ lexlibVecPop().
+ lexlibVecInsert().
+ lexlibVecRemove().
+ lexlibVecResize().
+ lexlibVecFind().
+ lexlibVecSwap().
+ lexlibVecClear().
+ lexlibVecLen().
+ lexlibVecCap().

## 1.5.0
+ lexlibFileBytes();
+ lexlibPowu();
+ lexlibColor16Blend();
+ lexlibImagePixelSet();
+ lexlibImagePixelGet();
+ lexlibImagePixel16Set();
+ lexlibImagePixel16Get();
+ lexlibImageProfileChange();
+ lexlibImageLoadBmp();
+ lexlibImageSaveBmp();
+ lexlibImageSaveBmpEx();
+ LexlibImage.data is officially native to the machine endianness.
+ fixed strPath buffer overflows.
+ fixed imageFlip X buffer overflow.
+ fixed linkedList memleaks.

## 1.4.0
+ function lexlibStrCopy().
+ removed path.h from default includes.
+ renamed LEXLIB_C_EXTERN to LEXLIB_EXTERN.
+ functions lexlibClamp().
+ struct LexlibColor8.
+ struct LexlibColor16.
+ struct LexlibColorFtl.
+ functions lexlibColorBlend().
+ functions lexlibColorGray().
+ functions lexlibColorPremultiply().
+ functions lexlibColorTo8().
+ functions lexlibColorTo16().
+ functions lexlibColorToFlt().
+ function lexlibColorFltClamp().

## 1.3.1
+ deprecation attribute.
+ removed PKGBUILD from package.
+ fixed a scope problem with clang.

## 1.3.0
+ LexlibImage.
+ support for loading and saving pngs.
+ lexlibStrPath.
+ LexlibPath is deprecated in favor of lexlibStrPath.
+ changed all _Bool for uint8_t.

## 1.2.0
+ lexlibRandom(), gets a secure random number.

## 1.1.0
+ official support for C++.
+ io: lexlibPrintTimed.

## 1.0.0
+ math: vector 2,3,4; float and integer.
+ os: lexlibRamSize().

## 0.10.0
+ moved LEXLIB_INLINE to macros.h.
+ current math functions are inlined.
+ removed obsolete math macros.

## 0.9.0
+ sleeping functions.
+ timer.
+ funcs to get time.

## 0.8.0
+ fixed overallocation in LexlibPath.
+ LexlibPath stores a copy of the raw strings.
+ new functions to get strings from LexlibPath.

## 0.7.3
+ typedefs.h now is defines.h
+ check if platform is support and spit a warning otherwise
+ defines.h organised and new definitions

## 0.7.2
+ lexlibPathGetParent/(Dir)().
+ fixed missing '\' with lexlibPathGetDir() on windows.

## 0.7.1
+ LEXLIB_EXPERIMENTAL modifier.
+ LinkedList macro.

## 0.7.0
+ LexlibLinkedList.

## 0.6.1
+ lexlibPathDelete();

## 0.6.0
+ LexlibPath for paths in a platform independent manner.

## 0.5.0
+ lexlibStrcat allows to create a new string from 2 existing ones.

## 0.4.0
+ converting beetween degrees and radians.
+ removed Vector2s, use <a href=https://github.com/recp/cglm>cglm</a> instead.
+ typedefs.h is officialy part of lexlib but not included by default.

## 0.3.0
+ lexlibVersion() that returns the version in a string.
+ removed the "#include&lt;stdint.h&gt;" in typedefs.h.

## 0.2.0
+ cmake build system.
+ lexlibFileToString; allows to read an entire file into a heap allocated string.
+ lexlibStringToFile; allows to write a string into a file.

## 0.1.0
+ Vector2s for simple 2D coordinates.
