#!/bin/bash

# install dirs
linuxdir=/usr/local
mingw32dir=/usr/i686-w64-mingw32
mingw64dir=/usr/x86_64-w64-mingw32

# lexlib vars, do not touch.
version="2.2.0"

function message () {
	echo -e "[\e[1;38;5;243mlinux.sh\e[0m]: $@\e[0m"
}

function createBuildDirs() {
	mkdir -p build/$1
}

if [[ $1 == cmake ]]; then
	if [[ $2 == all ]]; then
		./linux.sh cmake linux $3
		./linux.sh cmake mingw86 $3
		./linux.sh cmake mingw64 $3
		exit 0
	fi
	if [[ $2 == linux ]]; then
		message "cmake linux"
		createBuildDirs linux
		cd build/linux
		if [[ $3 == debug ]]; then
			cmake ../.. -DCMAKE_BUILD_TYPE=Debug
		else
			cmake ../..
		fi
		exit 0
	fi
	if [[ $2 == mingw86 ]]; then
		message "cmake mingw_i686"
		createBuildDirs mingw_i686
		cd build/mingw_i686
		if [[ $3 == debug ]]; then
			cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../cmake/mingw_i686.cmake -DCMAKE_BUILD_TYPE=Debug
		else
			cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../cmake/mingw_i686.cmake
		fi
		exit 0
	fi
	if [[ $2 == mingw64 ]]; then
		message "cmake mingw64"
		createBuildDirs mingw_x86_64
		cd build/mingw_x86_64
		if [[ $3 == debug ]]; then
			cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../cmake/mingw_x86_64.cmake -DCMAKE_BUILD_TYPE=Debug
		else
			cmake ../.. -DCMAKE_TOOLCHAIN_FILE=../../cmake/mingw_x86_64.cmake
		fi
		exit 0
	fi
	if [[ $2 == install ]]; then
		message "cmake install"
		if [ -d build/linux ]; then
			cd build/linux
			make install
			exit 0
		fi
		message "no build dir found"
		exit 1
	fi
	message "cmake option: all | linux | mingw86 | mingw64 | install"
	exit 1
fi

if [[ $1 == build ]]; then
	buildvar=0
	if [ -d build/linux ]; then
		message "build linux"
		buildvar=1
		cd build/linux
		make
		if [ $? != 0 ]; then
			message "linux build error"
			exit 1
		fi
		cd ../..
	fi
	if [ -d build/mingw_i686 ]; then
		message "build mingw_i686"
		buildvar=1
		cd build/mingw_i686
		make
		if [ $? != 0 ]; then
			message "mingw_i686 build error"
			exit 1
		fi
		cd ../..
	fi
	if [ -d build/mingw_x86_64 ]; then
		message "build mingw64"
		buildvar=1
		cd build/mingw_x86_64
		make
		if [ $? != 0 ]; then
			message "mingw64 build error"
			exit 1
		fi
		cd ../..
	fi
	if [[ $buildvar != 1 ]]; then
		message "no build dir found, do \"./linux.sh cmake\" first."
		exit 1
	fi
	exit 0
fi

if [[ $1 == install ]]; then
	installvar=0
	./linux.sh build
	if [ $? != 0 ]; then
		exit 1
	fi
	if [ -d build/linux ]; then
		message "install linux"
		installvar=1
		cp -v build/linux/liblexlib.a $linuxdir/lib
# 		cp -v build/linux/liblexlib.a /usr/lib
		cp -vr include/lexlib $linuxdir/include/
	fi
	if [ -d build/mingw_i686 ]; then
		message "install mingw_i686"
		installvar=1
		cp -v build/mingw_i686/liblexlib.a $mingw32dir/lib
		cp -vr include/lexlib $mingw32dir/include/
	fi
	if [ -d build/mingw_x86_64 ]; then
		message "install mingw_x86_64"
		installvar=1
		cp -v build/mingw_x86_64/liblexlib.a $mingw64dir/lib
		cp -vr include/lexlib $mingw64dir/include/
	fi
	if [[ $installvar != 1 ]]; then
		message "no build dir found, do \"./linux.sh cmake\" first."
		exit 1
	fi
	exit 0
fi

if [[ $1 == uninstall ]]; then
	uninstallvar=0
	if [ -f $linuxdir/lib/liblexlib.a ]; then
		rm -v $linuxdir/lib/liblexlib.a
		rm -v /usr/lib/liblexlib.a
		uninstallvar=1
	fi
	if [ -d $linuxdir/include/lexlib ]; then
		rm -vr $linuxdir/include/lexlib
		uninstallvar=1
	fi
	if [ -f $mingw32dir/lib/liblexlib.a ]; then
		rm -v $mingw32dir/lib/liblexlib.a
		uninstallvar=1
	fi
	if [ -d $mingw32dir/include/lexlib ]; then
		rm -vr $mingw32dir/include/lexlib
		uninstallvar=1
	fi
	if [ -f $mingw64dir/lib/liblexlib.a ]; then
		rm -v $mingw64dir/lib/liblexlib.a
		uninstallvar=1
	fi
	if [ -d $mingw64dir/include/lexlib ]; then
		rm -vr $mingw64dir/include/lexlib
		uninstallvar=1
	fi
	if [[ $uninstallvar != 1 ]]; then
		message "nothing to uninstall"
	fi
	exit 0
fi

if [[ $1 == test ]]; then
	testvar=0
	./linux.sh build
	if [ $? != 0 ]; then
		message "build error, no tests are run"
		exit 1
	fi
	if [ -f build/mingw_x86_64/test.exe ]; then
		message "mingw_x86_64 test"
		testvar=1
		wine64 build/mingw_x86_64/test.exe $2 $3
	fi
	if [ -f build/mingw_i686/test.exe ]; then
		message "mingw_i686 test"
		testvar=1
		wine build/mingw_i686/test.exe $2 $3
	fi
	if [ -f build/linux/test ]; then
		message "linux test"
		testvar=1
		build/linux/test $2 $3
	fi
	if [[ $testvar != 1 ]]; then
		message "no test executable found"
		exit 1
	fi
	exit 0
fi

if [[ $1 == package ]]; then
	message "package"
	# clean rebuild
	./linux.sh clean
	./linux.sh cmake all
	./linux.sh build
	# clean if repackaging
	rm -rf packages/lexlib_documentation_$version.zip
	rm -rf packages/lexlib_linux_$version.tar.gz
	rm -rf packages/lexlib_mingw_i686_$version.zip
	rm -rf packages/lexlib_mingw_x86_64_$version.zip
	# create dirs
	mkdir -p packages
	mkdir -p build/tmp
	mkdir -p build/tmp/lexlib_linux_$version/lib
	mkdir -p build/tmp/lexlib_mingw_i686_$version/lib
	mkdir -p build/tmp/lexlib_mingw_x86_64_$version/lib
	mkdir -p build/tmp/lexlib_linux_$version/include
	mkdir -p build/tmp/lexlib_mingw_i686_$version/include
	mkdir -p build/tmp/lexlib_mingw_x86_64_$version/include
	mkdir -p build/tmp/lexlib_documentation_$version
	# copy stuff
	cp -r include/lexlib build/tmp/lexlib_linux_$version/include/
	cp -r include/lexlib build/tmp/lexlib_mingw_i686_$version/include/
	cp -r include/lexlib build/tmp/lexlib_mingw_x86_64_$version/include/
	cp build/linux/liblexlib.a build/tmp/lexlib_linux_$version/lib/
	cp build/mingw_i686/liblexlib.a build/tmp/lexlib_mingw_i686_$version/lib/
	cp build/mingw_x86_64/liblexlib.a build/tmp/lexlib_mingw_x86_64_$version/lib/
	cp README.md LICENSE* build/tmp/lexlib_linux_$version/
	cp README.md LICENSE* build/tmp/lexlib_mingw_i686_$version/
	cp README.md LICENSE* build/tmp/lexlib_mingw_x86_64_$version/
	cp -r documentation/* build/tmp/lexlib_documentation_$version/
	# tar/zip
	cd build/tmp
	tar -czvf ../../packages/lexlib_linux_$version.tar.gz lexlib_linux_$version
	zip -vr ../../packages/lexlib_mingw_i686_$version.zip lexlib_mingw_i686_$version
	zip -vr ../../packages/lexlib_mingw_x86_64_$version.zip lexlib_mingw_x86_64_$version
	zip -vr ../../packages/lexlib_documentation_$version.zip lexlib_documentation_$version
	exit 0
fi

if [[ $1 == clean ]]; then
	message "clean"
	rm -rf build
	exit 0
fi

message "options: cmake | build | install | uninstall | test | benchmark | package | clean"
exit 1
