// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_hpp
#define lexlib_hpp

/* C++ header
	a wrapper to make use of C++'s namespaces.
	WARNING lexlib c++ header is experimental and not stable
*/

#include "lexlib/time.h"
#include<lexlib/lexlib.h>

#ifdef __cplusplus

namespace lexlib {
	namespace cfile {
		// maps a file to memory.
		// NOTE don't call free with the returned memory, call lexlib::mem:unmap().
		// returns NULL on error.
		inline void *map(FILE *file){return lexlibCFileMap(file);}

		// get the mode in which the file was opened.
		// might return LEXLIB_RD, LEXLIB_WR or LEXLIB_RDWR with LEXLIB_CREATE and/or LEXLIB_APPEND or'd, 0 on error
		inline uint8_t mode(FILE *file){return lexlibCFileMode(file);}

		// gets the size of a file in bytes.
		// on error returns -1.
		inline int64_t size(FILE *file){return lexlibCFileSize(file);}

		// if the file is smaller than expected LEXLIB_FILETYPE_UNKNOWN is returned.
		// file should be opened in byte mode.
		// doesn't mess with the filepos.
		inline uint16_t type(FILE *file){return lexlibCFileType(file);}
	}

	// math stuff
	namespace math {
		// ratio of the circumference of a circle to its diameter
		const double PI = 3.1415926535897932384626433;
		// ratio of the circumference of a circle to its diameter but float
		const float PIf = 3.1415926535897932384626433f;

		// clamps a double to a min and max
		inline double clamp(double num, double min, double max){
			return (num > max) ? max : (num < min) ? min : num;
		}

		// clamps a float to a min and max
		inline float clampf(float num, float min, float max){
			return (num > max) ? max : (num < min) ? min : num;
		}

		// clamps a integer to a min and max
		inline int clampi(int num, int min, int max){
			return (num > max) ? max : (num < min) ? min : num;
		}

		// clamps a unsigned integer to a min and max
		inline unsigned int clampu(unsigned int num, unsigned int min, unsigned int max){
			return (num > max) ? max : (num < min) ? min : num;
		}

		// converts radians to degrees
		inline double deg(double rads){return rads * (180.0 / PI);}
		// converts radians to degrees
		inline float degf(float rads){return rads * (180.0f / PIf);}

		// converts degrees to radians
		inline double rad(double degrees){return degrees * PI / 180.0;}
		// converts degrees to radians using floats
		inline float radf(float degrees){return degrees * PIf / 180.0f;}

		// calculates base to the power of exp
		inline unsigned int powu(unsigned int base, unsigned int exp){return lexlibPowu(base, exp);}

		// float comparission
		inline int floatcmp(float a, float b, float epsilon){return lexlibFloatCmp(a, b, epsilon);}
	}

	namespace mem {
		// unmaps memory mapped by lexlib; used for mapped files.
		inline void unmap(void *mem, size_t size){lexlibMemUnmap(mem, size);}

		// reverses the byte order of a 16bit type.
		inline uint16_t reverse16(uint16_t val){return lexlibMemReverse16(val);}
		// reverses the byte order of a 32bit type.
		inline uint32_t reverse32(uint32_t val){return lexlibMemReverse32(val);}
		// reverses the byte order of a 64bit type.
		inline uint64_t reverse64(uint64_t val){return lexlibMemReverse64(val);}
	}

	namespace os {
		inline unsigned int random(){return lexlibRandom();}
	}

	// cstings manipulation
	namespace str {
		// create a new cstring allocated on the heap.
		// on error returns NULL (failed to allocate memory).
		inline char *create(const char *str){return lexlibStrNew(str);}

		// creates a new heap allocated and null terminated string from concatenating 2 strings.
		// note that strings to be concatenated should be null terminated.
		// on error returns NULL (failed to allocate memory).
		inline char *cat(const char *str1, const char *str2){return lexlibStrCat(str1, str2);}

		// compute the string length (null terminator ignored).
		// supports ASCII, UTF-8.
		// returns the length of the string, -1 if string contains invalid/incomplete chars.
		inline int len(const char *str){return lexlibStrLen(str);}

		// compute the string size in memory (null terminator included).
		inline size_t size(const char *str){return lexlibStrSize(str);}

		// creates a new heap allocated copy of a string.
		// the string passed should be null terminated; the string returned will be null terminated.
		// on error returns NULL (failed to allocate memory).
		inline char *copy(const char *str){return lexlibStrCopy(str);}

		// read a file into a null terminated heap allocated string.
		// returns a valid string on success, NULL on error.
		inline char *file(const char *filename){return lexlibStrFile(filename);}
	}

	namespace time {
		// get the real time
		namespace real {
			inline uint64_t nanos(){return lexlibTimeNanos();}
			inline uint64_t micros(){return lexlibTimeMicros();}
			inline uint64_t millis(){return lexlibTimeMillis();}
			inline uint64_t seconds(){return lexlibTimeSeconds();}
		}
		// get the cpu time consumed by the thread.
		namespace thread {
			inline uint64_t nanos(){return lexlibThrdNanos();}
			inline uint64_t micros(){return lexlibThrdMicros();}
			inline uint64_t millis(){return lexlibThrdMillis();}
			inline uint64_t seconds(){return lexlibThrdSeconds();}
		}
		// get the cpu time consumed by the process
		namespace process {
			inline uint64_t nanos(){return lexlibProcNanos();}
			inline uint64_t micros(){return lexlibProcMicros();}
			inline uint64_t millis(){return lexlibProcMillis();}
			inline uint64_t seconds(){return lexlibProcSeconds();}
		}

		class Timer {
		private:
			LexlibTimer timer;
		public:
			// creates a new Timer with a initial time.
			// is started at creation.
			inline Timer(uint64_t ms){timer = lexlibTimerNew(ms);}
			// sets the time of the timer
			inline void set(uint64_t ms){lexlibTimerSet(&this->timer, ms);}
			// add time to a running timer
			inline void add(uint64_t ms){lexlibTimerAdd(&this->timer, ms);}
			// starts/restarts a timer
			inline void start(){lexlibTimerStart(&this->timer);}
			// updates the state of the timer
			inline void update(){lexlibTimerUpdate(&this->timer);}
			// checks if a timer is done, implicitly calls Timer::update
			inline bool done(){return lexlibTimerDone(&this->timer);}
			// blocks the thread until the timer is done
			inline void untilDone(){lexlibTimerUntilDone(&this->timer);}
			// checks if the timer has finished, does not call Timer::update.
			inline bool finished(){return lexlibTimerFinished(&this->timer);}
		};
	}
}

#else
#	warning lexlib.hpp included in a not C++ TU, use lexlib.h
#endif

#endif
