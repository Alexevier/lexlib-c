// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_h
#define lexlib_math_h

#include<float.h>
#include<math.h>
#include"math/rect.h"
#include"math/vec2.h"
#include"math/vec3.h"
#include"math/vec4.h"
#include"math/vec2i.h"
#include"math/vec3i.h"
#include"math/vec4i.h"
#include"common.h"

// math "constants"
#define LEXLIB_PI  3.1415926535897932384626433
#define LEXLIB_PIf 3.1415926535897932384626433f

// clamps a value to a min and max //
LEXLIB_INLINE double lexlibClamp(double num, double min, double max){
	return (num > max) ? max : (num < min) ? min : num;
}
LEXLIB_INLINE float lexlibClampf(float num, float min, float max){
	return (num > max) ? max : (num < min) ? min : num;
}
LEXLIB_INLINE int lexlibClampi(int num, int min, int max){
	return (num > max) ? max : (num < min) ? min : num;
}
LEXLIB_INLINE unsigned int lexlibClampu(unsigned int num, unsigned int min, unsigned int max){
	return (num > max) ? max : (num < min) ? min : num;
}
#define LEXLIB_CLAMP(num, min, max) ((num) > (max) ? (max) : (num) < (min) ? (min) : (num))

// converts radians to degrees
LEXLIB_INLINE double lexlibDeg(double rads){
	return rads * (180.0 / LEXLIB_PI);
}
LEXLIB_INLINE float lexlibDegf(float rads){
	return rads * (180.0f / LEXLIB_PIf);
}

// converts degrees to radians //
LEXLIB_INLINE double lexlibRad(double degrees){
	return degrees * LEXLIB_PI / 180.0;
}
LEXLIB_INLINE float lexlibRadf(float degrees){
	return degrees * LEXLIB_PIf / 180.0f;
}

// calculates base to the power of exp //
LEXLIB_INLINE unsigned int lexlibPowu(unsigned int base, unsigned int exp){
	unsigned int result = 1;
	while(exp != 0){
		result *= base;
		--exp;
	}
	return result;
}

// float comparission //
LEXLIB_INLINE int lexlibFloatCmp(float a, float b, float epsilon){
	if(a == b)
		return LEXLIB_TRUE;

	float aAbs = fabsf(a);
	float bAbs = fabsf(b);
	float diff = fabsf(a - b);

	if(a == 0 || b == 0 || (aAbs + bAbs < FLT_MIN))
		return diff < (epsilon * FLT_MIN);
	return diff / fmin(aAbs + bAbs, FLT_MAX) < epsilon;
}

#endif
