// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_macros_h
#define lexlib_macros_h

// stringify
#define LEXLIB_STRING(X) LEXLIB_STRING_STEP(X)

#define LEXLIB_STRING_STEP(X) #X

#endif
