/*
Copyright (C) 2023-2024 alexevier <alexevier@proton.me>

This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.
*/

#ifndef lexlib_h
#define lexlib_h

/* LexLib
aLexevier's Library

Linux and windows are officially supported.

> naming
	struct     : Lexlib[Name]
	function   : lexlib[Name]
	type       : Lexlib[Name]
	enum       : Lexlib[Name]
	enum value : LEXLIB_[NAME]
	macro      : LEXLIB_[NAME]

	some exeptions may be made for macros and use the functions naming.
	structs are not typedef'd by default (check typedef.h).
	anything with a leading _ is not stable or not part of the intended interface.
*/

#include"cfile.h"
#include"char.h"
#include"color.h"
#include"common.h"
#include"defines.h"
#include"filesystem.h"
#include"image.h"
#include"io.h"
/*#include"lexlib.h"*/
#include"macros.h"
#include"math.h"
#include"mem.h"
#include"os.h"
#include"path.h"
#include"simd.h"
#include"str.h"
#include"string.h"
#include"time.h"
#include"vec.h"
#include"vector.h"

/* get the lexlib version in format: X.X.X */
LEXLIBFN const char *lexlibVersion(void);

#endif
