// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_time_h
#define lexlib_time_h

#include<stdint.h>
#include"common.h"

// accesing its internals is supported but should not be modified
// uses real time and millisecond resolution
struct LexlibTimer {
	uint64_t time;
	uint64_t left;
	uint64_t last;
};

// gets real time.
LEXLIBFN uint64_t lexlibTimeNanos(void);
LEXLIBFN uint64_t lexlibTimeMicros(void);
LEXLIBFN uint64_t lexlibTimeMillis(void);
LEXLIBFN uint64_t lexlibTimeSeconds(void);

// gets the cpu time consumed by the thread.
LEXLIBFN uint64_t lexlibThrdNanos(void);
LEXLIBFN uint64_t lexlibThrdMicros(void);
LEXLIBFN uint64_t lexlibThrdMillis(void);
LEXLIBFN uint64_t lexlibThrdSeconds(void);

// gets the cpu time consumed by the process.
LEXLIBFN uint64_t lexlibProcNanos(void);
LEXLIBFN uint64_t lexlibProcMicros(void);
LEXLIBFN uint64_t lexlibProcMillis(void);
LEXLIBFN uint64_t lexlibProcSeconds(void);

// creates a new LexlibTimer with a initial time.
// is started at creation.
LEXLIBFN struct LexlibTimer lexlibTimerNew(uint64_t ms);

// sets the time of existing timer
LEXLIBFN void lexlibTimerSet(struct LexlibTimer* timer, uint64_t ms);

// add time to a running timer
LEXLIBFN void lexlibTimerAdd(struct LexlibTimer* timer, uint64_t ms);

// starts/restarts a timer
LEXLIBFN void lexlibTimerStart(struct LexlibTimer* timer);

// updates the state of a timer
LEXLIBFN void lexlibTimerUpdate(struct LexlibTimer* timer);

// checks if a timer is done, implicitly calls lexlibTimerUpdate
LEXLIBFN LexlibBool lexlibTimerDone(struct LexlibTimer* timer);

// blocks the thread until the timer is done
LEXLIBFN void lexlibTimerUntilDone(struct LexlibTimer* timer);

// checks if the timer has finished, does not call lexlibTimerUpdate.
LEXLIBFN LexlibBool lexlibTimerFinished(const struct LexlibTimer* timer);

#endif
