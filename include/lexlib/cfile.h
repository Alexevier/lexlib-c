// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_cfile_h
#define lexlib_cfile_h

#include<stddef.h>
#include<stdint.h>
#include<stdio.h>
#include"common.h"

// maps a file to memory.
// NOTE don't call free with the returned memory, call lexlibMemUnmap().
// returns NULL on error.
LEXLIBFN void *lexlibCFileMap(FILE *file);

// get the mode in which the file was opened.
// might return LEXLIB_RD, LEXLIB_WR or LEXLIB_RDWR with LEXLIB_CREATE and/or LEXLIB_APPEND or'd, 0 on error
LEXLIBFN uint8_t lexlibCFileMode(FILE *file);

// gets the size of a file in bytes.
// on error returns -1.
LEXLIBFN int64_t lexlibCFileSize(FILE *file);

// if the file is smaller than expected LEXLIB_FILETYPE_UNKNOWN is returned.
// file should be opened in byte mode.
// doesn't mess with the filepos.
LEXLIBFN uint16_t lexlibCFileType(FILE *file);

#endif
