// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_str_h
#define lexlib_str_h

#include<stdint.h>
#include<stddef.h>
#include"common.h"

// create a new cstring allocated on the heap.
// on error returns NULL (failed to allocate memory).
LEXLIBFN char *lexlibStrNew(const char *str);

// creates a new heap allocated and null terminated string from concatenating 2 strings.
// note that strings to be concatenated should be null terminated.
// on error returns NULL (failed to allocate memory).
LEXLIBFN char *lexlibStrCat(const char *str1, const char *str2);

/* append a string to another.
	str gets reallocated and the returned pointer is the string.
	on error NULL is returned and str is unmodified. */
char *lexlibStrAppend(char *str, const char *str2);

/* compare 2 strings.
	returns:
		true (1): strings are equal.
		false (0): strings are not equal.
	if lengths are different or 0 immediately returns false.
	if a string contains a invalid char false is also returned.
*/
LEXLIBFN LexlibBool lexlibStrCompare(const char *LEXLIB_RESTRICT str1, const char *LEXLIB_RESTRICT str2);

// compute the string length (null terminator ignored).
// supports ASCII, UTF-8.
// returns the length of the string, -1 if string contains invalid/incomplete chars.
LEXLIBFN int lexlibStrLen(const char *str);

// compute the string size in memory (null terminator included).
LEXLIBFN size_t lexlibStrSize(const char *str);

// creates a new heap allocated copy of a string.
// the string passed should be null terminated; the string returned will be null terminated.
// on error returns NULL (failed to allocate memory).
LEXLIBFN char *lexlibStrCopy(const char *str);

// read a file into a null terminated heap allocated string.
// returns a valid string on success, NULL on error.
LEXLIBFN char *lexlibStrFile(const char *filename);

/* format a new cstring.
	uses stdc formatting.
	the returned string is heap allocated and null terminated.
	returns: NULL if fails to format or allocate memory. */
LEXLIBFN char *lexlibStrFormat(const char *fmt, ...);

// creates a string with a correctly formatted path. (unix: / ; windows: \)
// on error returns NULL (failed to allocate memory).
LEXLIB_DEPRECATED
LEXLIBFN char *lexlibStrPathNew(const char *str);

// adds a element to the end of a formatted path.
// returns 0 on success, LEXLIB_INVALID_VALUE or LEXLIB_OUT_OF_MEMORY on error.
LEXLIB_DEPRECATED
LEXLIBFN uint8_t lexlibStrPathPush(char **str, const char *add);

// removes the last element.
// returns 0 on success, LEXLIB_OUT_OF_MEMORY or LEXLIB_INVALID_OPERATION on error.
LEXLIB_DEPRECATED
LEXLIBFN uint8_t lexlibStrPathPop(char **str);

// converts the path as if it was a dir (aka: path/to/dir/), in short adds a / to the end. (\ on windows).
// returns 0 on success, LEXLIB_INVALID_VALUE or LEXLIB_OUT_OF_MEMORY on error.
LEXLIB_DEPRECATED
LEXLIBFN uint8_t lexlibStrPathAsDir(char **str);

// if the str has a / or \ at the end it will be removed and count as if was referencing a file.
// returns 0 on success, LEXLIB_INVALID_VALUE or LEXLIB_OUT_OF_MEMORY on error.
LEXLIB_DEPRECATED
LEXLIBFN uint8_t lexlibStrPathAsFile(char **str);

#endif
