/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_filesystem_h
#define lexlib_filesystem_h

#include"common.h"
#include<stddef.h>
#include<stdint.h>

/* experimental */
struct LexlibFsList {
	size_t count;
	struct LexlibFsListElement_ {
		char *name;
		uint8_t type;
	} *element;
};

/* create a directory.
	returns:
	- LEXLIB_OK (0): directory succesfully created.
	- LEXLIB_EXIST: directory already exists.
	- LEXLIB_INVALID_PATH: a directory in the middle is mising or is not a directory.
	- LEXLIB_INVALID_OPERATION: the creation was not permited by the os. */
LEXLIBFN uint8_t lexlibFsCreateDir(const char *path);

/* get the absolute path.
	the returned pointer needs to be deallocated using free().
	returns NULL on error. */
LEXLIBFN char *lexlibFsAbsolute(const char *path);

/* get the size in bytes of a file.
	return -1 on error. */
LEXLIBFN int64_t lexlibFsSize(const char *path);

LEXLIB_EXPERIMENTAL
LEXLIBFN uint8_t lexlibFsList(struct LexlibFsList *list, const char *path);

LEXLIB_EXPERIMENTAL
LEXLIBFN void lexlibFsListFree(struct LexlibFsList *list);

#endif
