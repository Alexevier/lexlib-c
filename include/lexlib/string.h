/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_string_h
#define lexlib_string_h

#include<stddef.h>
#include"common.h"
#include"char.h"
#include"vec.h"

/* lexlib strings provide a better string support from the typical
	"char *" that C has included.

	for this documentation CString refers to raw c string (char *).
	to use a LexlibString as a CString call lexlibStringRaw() or lexlibStringRawMut().
	strings are not optimized to be used as buffers, for that use LexlibVec and then call
	lexlibStringFromVec()

	features:
	- getting the length is O(1).
	- simple utf-8 support.
	- clear interface.
	- compatible with CString.
*/

/* it should never be reallocated or freed direcly.
	always call lexlibString*** functions. */
typedef struct LexlibString *LexlibString;

/* create a new LexlibString from a string literal.
	returns: NULL if it failed to allocate the memory. */
LEXLIBFN LexlibString lexlibStringNew(const char *str);

/* delete a LexlibString.
	does nothing if a NULL pointer is passed. */
LEXLIBFN void lexlibStringDelete(LexlibString string);

/* create a copy of a LexlibString. */
LEXLIBFN LexlibString lexlibStringCopy(const LexlibString string);

/* create a new LexlibString from a CString.
	returns: NULL if it failed to allocate the memory. */
LEXLIBFN LexlibString lexlibStringFromC(const char *cstring);

/* create a new LexlibString from a LexlibVec.
	the returned string might contain invalid chars, call lexlibStringValid().
	returns: NULL if it failed to allocate the memory. */
LEXLIBFN LexlibString lexlibStringFromVec(const LexlibVec(char) vec);

/* get a utf-8 char from a LexlibString. (index starts at 0)
	return 0 if the index is out of bounds. */
LEXLIBFN LexlibChar lexlibStringChar(const LexlibString string, size_t index);

/* append a string to another.
	returns:
	- nonNULL: stringAdd was appended to string and string was deleted; use the new returned one.
	- NULL: failed to allocate the memory; the original string is not modified. */
LEXLIBFN LexlibString lexlibStringAppend(LexlibString string, const LexlibString stringAdd);

/* append a CString to a LexlibString.
	returns:
	- nonNULL: str was appended to string and string was deleted; use the new returned one.
	- NULL: failed to allocate the memory; the original string is not modified. */
LEXLIBFN LexlibString lexlibStringAppendStr(LexlibString string, const char *str);

/* compare two LexlibStrings.
	returns true (1) if they are equal, false (0) otherwise. */
LEXLIBFN LexlibBool lexlibStringCompare(const LexlibString lstring, const LexlibString rstring);

/* compare a LexlibString with a CString.
	returns true (1) if they are equal, false (0) otherwise. */
LEXLIBFN LexlibBool lexlibStringCompareStr(const LexlibString string, const char *cstring);

/* compute the actual string length (null terminator ignored) */
LEXLIBFN size_t lexlibStringLen(const LexlibString string);

/* get the size in bytes of the string (null terminator included). */
LEXLIBFN size_t lexlibStringSize(const LexlibString string);

/* check if all characters in the string are valid.
	returns: true if they are, false otherwise. */
LEXLIBFN LexlibBool lexlibStringValid(const LexlibString string);

/* check if the string only has ascii characters. */
LEXLIBFN LexlibBool lexlibStringIsAscii(const LexlibString string);

/* tries to fix a string. */
LEXLIB_EXPERIMENTAL
LEXLIBFN LexlibString lexlibStringFix(LexlibString string);

/* get a pointer to the string chraracter data. */
LEXLIBFN const char *lexlibStringRaw(const LexlibString string);

/* get a pointer to the string modifiable character data. */
LEXLIBFN char *lexlibStringRawMut(LexlibString string);

#endif
