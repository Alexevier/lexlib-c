/* Copyright 2023 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_path_h
#define lexlib_path_h

#include"common.h"
#include<stdint.h>

/* creates a string with a correctly formatted path.
	unix: /
	windows: \
	on error returns NULL (failed to allocate memory) */
LEXLIBFN char *lexlibPathNew(const char *str);

/* adds a element to the end of a formatted path.
	returns:
	- LEXLIB_OK (0): success.
	- LEXLIB_INVALID_VALUE: a string had a len of 0.
	- LEXLIB_OUT_OF_MEMORY: failed to reallocate the path */
LEXLIBFN uint8_t lexlibPathAdd(char **path, const char *str);

/* remove the last element of the path.
	returns:
	- LEXLIB_OK (0): success.
	- LEXLIB_INVALID_OPERATION: path has only one element.
	- LEXLIB_OUT_OF_MEMORY: failed to reallocate string.*/
LEXLIBFN uint8_t lexlibPathPop(char **path);

/* check if a path is absolute or relative.
	windows drive letters must be uppercase. */
LEXLIBFN LexlibBool lexlibPathAbsolute(const char *path);

/* makes the path to be referencing a dir; adds a '/' to the end.
	returns:
	- LEXLIB_OK (0): success.
	- LEXLIB_OUT_OF_MEMORY: failed to allocate the extra byte :(. */
LEXLIBFN uint8_t lexlibPathAsDir(char **path);

/* make the path to be referencing a file; removes '/' from the end.
	return:
	- LEXLIB_OK (0): success.
	- LEXLIB_OUT_OF_MEMORY: failed to deallocate the extra byte :(. */
LEXLIBFN uint8_t lexlibPathAsFile(char **path);

/* get the file extension.
	returns:
	- a pointer to the path where the extension starts (.).
	- NULL: no file extension. */
LEXLIBFN const char *lexlibPathExtension(const char *path);

#endif
