// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_defines_h
#define lexlib_defines_h

// bool : represents a true or false type explicitly
typedef unsigned char LexlibBool;
#define LEXLIB_TRUE  1
#define LEXLIB_FALSE 0

// misc
#define LEXLIB_NONE 0x00

// errors : limited to uint8_t range.
#define LEXLIB_OK 0x00
#define LEXLIB_FILE 0xA0
#define LEXLIB_DIRECTORY 0xA1
#define LEXLIB_INVALID_VALUE 0xB0
#define LEXLIB_INVALID_OPERATION 0xB1
#define LEXLIB_INVALID_REQUEST 0xB2
#define LEXLIB_INVALID_ARG 0xB3
#define LEXLIB_INVALID_DIR 0xB4
#define LEXLIB_INVALID_LEN 0xB5
#define LEXLIB_INVALID_MSG 0xB6
#define LEXLIB_INVALID_SIZE 0xB7
#define LEXLIB_INVALID_FILE 0xB8
#define LEXLIB_INVALID_PATH 0xB9
#define LEXLIB_INVALID_NAME 0xBA
#define LEXLIB_INVALID_TYPE 0xBB
#define LEXLIB_INVALID_CHAR 0xBC
#define LEXLIB_INVALID_DATA 0xBD
#define LEXLIB_CORRUPT_DATA 0xC0
#define LEXLIB_EOF 0xC1
#define LEXLIB_EXIST 0xFF
#define LEXLIB_CANT_OPEN 0xC3
#define LEXLIB_CANT_CLOSE 0xC4
#define LEXLIB_CANT_READ 0xC5
#define LEXLIB_CANT_WRITE 0xC6
#define LEXLIB_PARTIAL_READ 0xC7
#define LEXLIB_PARTIAL_WRITE 0xC8
#define LEXLIB_RESET 0xE0
#define LEXLIB_ABORT 0xE1
#define LEXLIB_BUSY 0xE2
#define LEXLIB_DENIED 0xE3
#define LEXLIB_CANCEL 0xE4
#define LEXLIB_BROKEN 0xE5
#define LEXLIB_BLOCKING 0xE6
#define LEXLIB_UNAVAILABLE 0xE7
#define LEXLIB_INTERRUPTED 0xE8
#define LEXLIB_LIMIT 0xE9
#define LEXLIB_NOT_FOUND 0xEA
#define LEXLIB_UNSUPORTED 0xF0
#define LEXLIB_OUT_OF_DISK 0xFD
#define LEXLIB_OUT_OF_MEMORY 0xFE
#define LEXLIB_ERROR 0xFF

// access permissions
#define LEXLIB_RD 0x01
#define LEXLIB_WR 0x02
#define LEXLIB_RDWR 0x03
#define LEXLIB_APPEND 0x04
#define LEXLIB_CREATE 0x08
#define LEXLIB_OVERWRITE 0x10

// different file types recognised by lexlib.
enum LexlibFileType {
	LEXLIB_FILETYPE_UNKNOWN = 0,
	LEXLIB_FILETYPE_BMP,
	LEXLIB_FILETYPE_JPG,
	LEXLIB_FILETYPE_PNG,
	LEXLIB_FILETYPE_SCRIPT,
};

#endif
