/* Copyright 2023-2024 alexevier <alexevier@proton.me>
licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_os_h
#define lexlib_os_h

#include<stdint.h>
#include"common.h"

// sleeps the thread for the specified number of seconds.
LEXLIBFN void lexlibSleep(unsigned int seconds);

// sleeps the thread for the specified number of milliseconds.
LEXLIBFN void lexlibSleepMs(unsigned int millis);

// gets a pseudorandom number from the OS that is cryptographically secure.
// if 0 is returned it might mean that it failed, its recommended to retry.
LEXLIBFN unsigned int lexlibRandom(void);

// gets the physical ram size.
// returns the size of the ram in bytes, 0 in case of error.
LEXLIBFN uint64_t lexlibRamSize(void);

/* get the absolute path of the current executable as a cstring.
	the returned string is heap allocated so it need to be freed with free().
	NOTE: available only on windows and linux.
	returns NULL on error. */
LEXLIBFN char *lexlibCurrentExecutablePath(void);

#endif
