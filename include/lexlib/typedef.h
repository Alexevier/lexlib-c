// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_typedef_h
#define lexlib_typedef_h

// color.h
typedef struct LexlibColor LexlibColor;
typedef struct LexlibColorF LexlibColorF;
typedef struct LexlibColor16 LexlibColor16;

// image.h
typedef struct LexlibImage LexlibImage;

// math.h
typedef struct LexlibRect LexlibRect;
typedef struct LexlibVec4 LexlibVec4;
typedef struct LexlibVec3 LexlibVec3;
typedef struct LexlibVec2 LexlibVec2;
typedef struct LexlibVec4i LexlibVec4i;
typedef struct LexlibVec3i LexlibVec3i;
typedef struct LexlibVec2i LexlibVec2i;

// time.h
typedef struct LexlibTimer LexlibTimer;

#endif
