/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_vector_h
#define lexlib_vector_h

/* generic vector via macros :) */

#include<stddef.h>
#include"common.h"

#define LEXLIB_VECTOR(V, T)\
	struct V {\
		size_t len;\
		size_t cap;\
		T *data;\
	}

#define LEXLIB_VECTOR_DECL(FN, V, T)\
	LEXLIB_VECTOR_DECL_NEW(FN##New, V)\
	LEXLIB_VECTOR_DECL_FREE(FN##Free, V)\
	LEXLIB_VECTOR_DECL_PUSH(FN##Push, V, T)\
	LEXLIB_VECTOR_DECL_PUSH_PTR(FN##PushPtr, V, T)\
	LEXLIB_VECTOR_DECL_POP(FN##Pop, V)\
	LEXLIB_VECTOR_DECL_REMOVE(FN##Remove, V, T)

#define LEXLIB_VECTOR_IMPL(FN, V, T)\
	LEXLIB_VECTOR_IMPL_NEW(FN##New, V)\
	LEXLIB_VECTOR_IMPL_FREE(FN##Free, V)\
	LEXLIB_VECTOR_IMPL_PUSH(FN##Push, V, T)\
	LEXLIB_VECTOR_IMPL_PUSH_PTR(FN##PushPtr, V, T)\
	LEXLIB_VECTOR_IMPL_POP(FN##Pop, V)\
	LEXLIB_VECTOR_IMPL_REMOVE(FN##Remove, V, T)

#define LEXLIB_VECTOR_DECL_NEW(FN, V)\
	/* create a new vector with capacity. */\
	/* in case of failure V.data will be NULL. */\
	LEXLIBFN V FN(size_t capacity);

#define LEXLIB_VECTOR_DECL_FREE(FN, V)\
	/* free a vector's memory. */\
	LEXLIBFN void FN(V *vector);

#define LEXLIB_VECTOR_DECL_PUSH(FN, V, T)\
	/* push obj into vector. */ \
	/* returns 0 on success, nonzero otherwise. */\
	LEXLIBFN int FN(V *vector, T obj);

#define LEXLIB_VECTOR_DECL_PUSH_PTR(FN, V, T)\
	/* push obj into vector.
		takes a pointer to the object but it will get copied.
		returns 0 on success, nonzero otherwise. */\
	LEXLIBFN int FN(V *vector, T *obj);

#define LEXLIB_VECTOR_DECL_POP(FN, V)\
	/* pop the last element of the vector. */\
	LEXLIBFN void FN(V *vector);

#define LEXLIB_VECTOR_DECL_REMOVE(FN, V, T)\
	/* remove an element in the vector. */\
	int FN(V *vector, size_t idx);

#define LEXLIB_VECTOR_IMPL_NEW(FN, V)\
	V FN(size_t capacity){\
		V vector = {0,capacity,NULL};\
		\
		if(capacity){\
			vector.data = malloc(capacity * sizeof(*vector.data));\
			if(!vector.data)\
				vector.cap = 0;\
		}\
		\
		return vector;\
	}

#define LEXLIB_VECTOR_IMPL_FREE(FN, V)\
	void FN(V *vector){\
		free(vector->data);\
	}

#define LEXLIB_VECTOR_IMPL_PUSH(FN, V, T)\
	int FN(V *vector, T obj){\
		if(!vector->data){\
			vector->data = malloc(sizeof(T));\
			if(!vector->data)\
				return 1;\
			vector->cap = 1;\
		}\
		\
		if(vector->len >= vector->cap){\
			vector->cap += 2;\
			\
			T *newmem = realloc(vector->data, vector->cap * sizeof(T));\
			if(!newmem){\
				vector->cap -= 2;\
				return 1;\
			}\
			vector->data = newmem;\
		}\
		\
		vector->data[vector->len] = obj;\
		vector->len++;\
		\
		return 0;\
	}

#define LEXLIB_VECTOR_IMPL_PUSH_PTR(FN, V, T)\
	int FN(V *vector, T *obj){\
		if(!vector->data){\
			vector->data = malloc(sizeof(T));\
			if(!vector->data)\
				return 1;\
			vector->cap = 1;\
		}\
		\
		if(vector->len >= vector->cap){\
			vector->cap += 2;\
			\
			T *newmem = realloc(vector->data, vector->cap * sizeof(T));\
			if(!newmem){\
				vector->cap -= 2;\
				return 1;\
			}\
			vector->data = newmem;\
		}\
		\
		vector->data[vector->len] = *obj;\
		vector->len++;\
		\
		return 0;\
	}

#define LEXLIB_VECTOR_IMPL_POP(FN, V)\
	void FN(V *vector){\
		if(vector->len != 0)\
			vector->len--;\
	}

#define LEXLIB_VECTOR_IMPL_REMOVE(FN, V, T)\
	int FN(V *vector, size_t idx){\
		if(!vector->data)\
			return 1;\
		if(idx >= vector->len)\
			return 1;\
		\
		memmove(vector->data+idx, vector->data+idx+1, (vector->len-idx-1) * sizeof(T));\
		vector->len--;\
		return 0;\
	}


#endif
