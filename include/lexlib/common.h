/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_common_h
#define lexlib_common_h

/* this header is not documented since its not
	suposed to be used externally of lexlib. */

#include"defines.h"

#ifdef __cplusplus
#	define LEXLIB_RESTRICT
#	define LEXLIB_EXTERN extern "C"
#else
#	define LEXLIB_RESTRICT restrict
#	define LEXLIB_EXTERN extern
#endif

#ifdef __GNUC__
#	define LEXLIB_INLINE static inline __attribute__((always_inline))
#	define LEXLIB_DEPRECATED __attribute__((deprecated))
#	define LEXLIB_FALLTHROUGH __attribute__((fallthrough))
#	define LEXLIB_ALIGN(X) __attribute__((aligned(X)))
#	define LEXLIB_UNUSED __attribute__((unused))
#	define LEXLIB_EXPERIMENTAL __attribute__((warning("experimental")))
#	define LEXLIB_WARNING(X) __attribute__((warning(X)))
#else
#	define LEXLIB_INLINE static inline
#	define LEXLIB_DEPRECATED
#	define LEXLIB_FALLTHROUGH
#	define LEXLIB_ALIGN(X)
#	define LEXLIB_UNUSED
#	define LEXLIB_EXPERIMENTAL
#	define LEXLIB_WARNING(X)
#endif

#define LEXLIBFN LEXLIB_EXTERN

#endif
