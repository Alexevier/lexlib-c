#ifndef lexlib_io_h
#define lexlib_io_h

/* lexlib io
	provides a abstract interface to a data stream,
	be a file or buffer. */

#include<stddef.h>
#include<stdint.h>
#include"common.h"
#include"string.h"

/* LexlibIO opaque struct */
struct LexlibIO;
/* LexlibIO opaque struct pointer */
typedef struct LexlibIO *LexlibIO;

/* open a file as a lexlibIO.
	if mode is 0 it will be opened in read only mode.
	the LEXLIB_APPEND and LEXLIB_CREATE flags are ignored.
	returns: NULL on error. */
LEXLIBFN LexlibIO lexlibIOfile(const char *filename, uint8_t flags);

/* open a buffer in memory to be read/written.
	the memory is not copied and must live as long the io is not closed. */
LEXLIBFN LexlibIO lexlibIOmem(void *mem, size_t size);

/* open a buffer in memory to be read.
	the memory is not copied and must live as long the io is not closed. */
LEXLIBFN LexlibIO lexlibIOmemConst(const void *mem, size_t size);

/* read bytes from io and put them in buffer.
	returns: the read byte count. */
LEXLIBFN size_t lexlibIOread(LexlibIO io, void *buffer, size_t bytes);

/* write bytes into io from buffer.
	if buffer is NULL, 0 will be written.
	returns: the written byte count. */
LEXLIBFN size_t lexlibIOwrite(LexlibIO io, const void *buffer, size_t bytes);

/* read a utf8 char from io.
	returns:
	- (other value): valid utf-8 char.
	- LEXLIB_CHAR_NULL: the readed character would have been invalid.
	- LEXLIB_CHAR_EOF: end of the data stream. */
LEXLIBFN LexlibChar lexlibIOreadChar(LexlibIO io);

/* read a char from io.
	returns:
	- (other value): the char.
	- EOF: end of the data stream. */
LEXLIBFN int lexlibIOreadCharc(LexlibIO io);

/* read a line from io.
	stops at a: null char, endline or end of io.
	returns:
	- (nonzero): a LexlibString.
	- NULL: nothing to read or out of memory. */
LEXLIBFN LexlibString lexlibIOreadLine(LexlibIO io);

/* read a line from io.
	stops at a: null char, endline or end of io.
	returns:
	- (nonzero): null terminated string.
	- NULL: nothing to read or out of memory. */
LEXLIBFN char *lexlibIOreadLineStr(LexlibIO io);

/* set the position indicator of the io.
	returns:
	- LEXLIB_OK (0): position set.
	- LEXLIB_INVALID_OPERATION: the new position is past the end. */
LEXLIBFN uint8_t lexlibIOsetpos(LexlibIO io, uint64_t pos);

/* get the position indicator of the io */
LEXLIBFN uint64_t lexlibIOgetpos(LexlibIO io);

/* flushes the io */
LEXLIBFN void lexlibIOflush(LexlibIO io);

/* close a lexlibIO stream. */
LEXLIBFN void lexlibIOclose(LexlibIO io);

#endif
