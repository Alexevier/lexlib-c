// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

// wrapper around SIMD (Single Instruction Multiple Data)
// NOTE currently only for internal lexlib use.

#ifndef lexlib_simd_h
#define lexlib_simd_h

#ifdef LEXLIB_CONFIG_SIMD
#	define LEXLIB_SIMD LEXLIB_CONFIG_SIMD
#endif

#ifndef LEXLIB_SIMD
#	define LEXLIB_SIMD 1
#endif

#if LEXLIB_SIMD

#ifndef LEXLIB_SIMD_MATH
#	define LEXLIB_SIMD_MATH 1
#endif

#ifdef __MMX__
#	include<mmintrin.h>
#	define LEXLIB_MMX

#	define LexlibSimd64i __m64

#	define lexlibSimd64i16Set(D, C, B, A) _mm_set_pi16(D, C, B, A)
#	define lexlibSimd64i16Set1(val) _mm_set1_pi16(val)

#	define lexlibSimd64i16ShiftLeft(var, cnt) _mm_slli_pi16(var, cnt)
#	define lexlibSimd64i16ShiftRight(var, cnt) _mm_srli_pi16(var, cnt)

#	define lexlibSimd64i16Add(a, b) _mm_add_pi16(a, b)
#	define lexlibSimd64i16MulLow(a, b) _mm_mullo_pi16(a, b)
#endif

#ifdef __SSE__
#	include<xmmintrin.h>
#	define LEXLIB_SSE

#	define LexlibSimd128f __m128

#	define lexlibSimd128fLoad(p) _mm_load_ps(p)
#	define lexlibSimd128fStore(dst, val) _mm_store_ps(dst, val)
#	define lexlibSimd128fSet1(val) _mm_set_ps1(val)

#	define lexlibSimd128fAdd(a, b) _mm_add_ps(a, b)
#	define lexlibSimd128fSub(a, b) _mm_sub_ps(a, b)
#	define lexlibSimd128fMul(a, b) _mm_mul_ps(a, b)
#	define lexlibSimd128fDiv(a, b) _mm_div_ps(a, b)
#endif

#ifdef __SSE2__
#	include<emmintrin.h>
#	define LEXLIB_SSE2

#	define LexlibSimd128i __m128i

#	define lexlibSimd128iLoad32(ptr) _mm_loadu_si32(ptr)
#endif

#ifdef __ARM_NEON
#	include<arm_neon.h>
#	define LEXLIB_NEON
#endif

#endif /* LEXLIB_SIMD */
#endif /* lexlib_simd_h */
