#ifndef lexlib_rect_h
#define lexlib_rect_h

#include"../common.h"

struct LexlibRect {
	float x;
	float y;
	float w;
	float h;
};


// performs
// 	res.w = rect.x + rect.w;
// 	res.h = rect.y + rect.h;
LEXLIB_EXPERIMENTAL
LEXLIB_INLINE void lexlibRectPointify(struct LexlibRect *result, const struct LexlibRect *rect){
	result->x = rect->x;
	result->y = rect->y;
	result->w = rect->x + rect->w;
	result->h = rect->y + rect->h;
}

// divide all elements by scalar
LEXLIB_EXPERIMENTAL
LEXLIB_INLINE void lexlibRectDivides(struct LexlibRect *result, const struct LexlibRect *rect, const float scalar){
	result->x = rect->x / scalar;
	result->y = rect->y / scalar;
	result->w = rect->w / scalar;
	result->h = rect->h / scalar;
}

/* check if 2 rects intersect. */
LEXLIB_EXPERIMENTAL
LEXLIB_INLINE LexlibBool lexlibRectIntersect(const struct LexlibRect *a, const struct LexlibRect *b){
	if(a->x < (b->x+b->w))
		if((a->x + a->w) > b->x)
			if(a->y < (b->y+b->h))
				if((a->y + a->h) > b->y)
					return LEXLIB_TRUE;
	return LEXLIB_FALSE;
}

#endif
