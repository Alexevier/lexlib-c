// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_vec2i_h
#define lexlib_math_vec2i_h

#include"../common.h"

struct LexlibVec2i {
	int x;
	int y;
};

#define LEXLIB_VEC2I_ZERO ((struct LexlibVec2i){0, 0})

LEXLIB_INLINE void lexlibVec2iAdd(struct LexlibVec2i *res, const struct LexlibVec2i *a, const struct LexlibVec2i *b){
	res->x = a->x + b->x;
	res->y = a->y + b->y;
}

LEXLIB_INLINE void lexlibVec2iSub(struct LexlibVec2i *res, const struct LexlibVec2i *a, const struct LexlibVec2i *b){
	res->x = a->x - b->x;
	res->y = a->y - b->y;
}

LEXLIB_INLINE void lexlibVec2iMul(struct LexlibVec2i *res, const struct LexlibVec2i *a, const struct LexlibVec2i *b){
	res->x = a->x * b->x;
	res->y = a->y * b->y;
}

LEXLIB_INLINE void lexlibVec2iDiv(struct LexlibVec2i *res, const struct LexlibVec2i *a, const struct LexlibVec2i *b){
	res->x = a->x / b->x;
	res->y = a->y / b->y;
}

LEXLIB_INLINE void lexlibVec2iAdds(struct LexlibVec2i *res, const struct LexlibVec2i *a, int s){
	res->x = a->x + s;
	res->y = a->y + s;
}

LEXLIB_INLINE void lexlibVec2iSubs(struct LexlibVec2i *res, const struct LexlibVec2i *a, int s){
	res->x = a->x - s;
	res->y = a->y - s;
}

LEXLIB_INLINE void lexlibVec2iMuls(struct LexlibVec2i *res, const struct LexlibVec2i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
}

LEXLIB_INLINE void lexlibVec2iDivs(struct LexlibVec2i *res, const struct LexlibVec2i *a, int s){
	res->x = a->x / s;
	res->y = a->y / s;
}

LEXLIB_INLINE void lexlibVec2iScale(struct LexlibVec2i *res, const struct LexlibVec2i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
}

LEXLIB_INLINE int lexlibVec2iDot(const struct LexlibVec2i *a, const struct LexlibVec2i *b){
	return a->x * b->x + a->y * b->y;
}

LEXLIB_INLINE void lexlibVec2iNegate(struct LexlibVec2i *res, const struct LexlibVec2i *vec){
	res->x = -vec->x;
	res->y = -vec->y;
}

#endif
