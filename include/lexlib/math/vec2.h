// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_vec2_h
#define lexlib_math_vec2_h

#include<math.h>
#include"../common.h"

struct LexlibVec2 {
	float x;
	float y;
};

#define LEXLIB_VEC2_ZERO ((struct LexlibVec2){0.0f, 0.0f})

LEXLIB_INLINE void lexlibVec2Add(struct LexlibVec2 *result, const struct LexlibVec2 *a, const struct LexlibVec2 *b){
	result->x = a->x + b->x;
	result->y = a->y + b->y;
}

LEXLIB_INLINE void lexlibVec2Sub(struct LexlibVec2 *result, const struct LexlibVec2 *a, const struct LexlibVec2 *b){
	result->x = a->x - b->x;
	result->y = a->y - b->y;
}

LEXLIB_INLINE void lexlibVec2Mul(struct LexlibVec2 *result, const struct LexlibVec2 *a, const struct LexlibVec2 *b){
	result->x = a->x * b->x;
	result->y = a->y * b->y;
}

LEXLIB_INLINE void lexlibVec2Div(struct LexlibVec2 *result, const struct LexlibVec2 *a, const struct LexlibVec2 *b){
	result->x = a->x / b->x;
	result->y = a->y / b->y;
}

LEXLIB_INLINE void lexlibVec2Adds(struct LexlibVec2 *result, const struct LexlibVec2 *a, float b){
	result->x = a->x + b;
	result->y = a->y + b;
}

LEXLIB_INLINE void lexlibVec2Subs(struct LexlibVec2 *result, const struct LexlibVec2 *a, float b){
	result->x = a->x - b;
	result->y = a->y - b;
}

LEXLIB_INLINE void lexlibVec2Muls(struct LexlibVec2 *result, const struct LexlibVec2 *a, float b){
	result->x = a->x * b;
	result->y = a->y * b;
}

LEXLIB_INLINE void lexlibVec2Divs(struct LexlibVec2 *result, const struct LexlibVec2 *a, float b){
	result->x = a->x / b;
	result->y = a->y / b;
}

LEXLIB_INLINE void lexlibVec2Scale(struct LexlibVec2 *result, const struct LexlibVec2 *a, float scalar){
	result->x = a->x * scalar;
	result->y = a->y * scalar;
}

LEXLIB_INLINE float lexlibVec2Dot(const struct LexlibVec2 *a, const struct LexlibVec2 *b){
	return a->x * b->x + a->y * b->y;
}

LEXLIB_INLINE float lexlibVec2Mag(const struct LexlibVec2 *vec){
	return sqrtf(vec->x * vec->x + vec->y * vec->y);
}

LEXLIB_INLINE void lexlibVec2Norm(struct LexlibVec2 *result, const struct LexlibVec2 *vec){
	float mag = lexlibVec2Mag(vec);
	if(mag == 0.0f){
		result->x = 0.0f;
		result->y = 0.0f;
		return;
	}
	lexlibVec2Divs(result, vec, mag);
}

LEXLIB_INLINE void lexlibVec2Neg(struct LexlibVec2 *result, const struct LexlibVec2 *vec){
	result->x = -vec->x;
	result->y = -vec->y;
}

LEXLIB_INLINE void lexlibVec2Rotate(struct LexlibVec2 *result, const struct LexlibVec2 *vec, float radians){
	const float x = vec->x;
	const float y = vec->y;
	result->x = x * cos(radians) - y * sin(radians);
	result->y = x * sin(radians) + y * cos(radians);
}

#endif
