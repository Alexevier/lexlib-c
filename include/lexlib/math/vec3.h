// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_vec3_h
#define lexlib_math_vec3_h

#include<math.h>
#include"../common.h"

struct LexlibVec3 {
	float x;
	float y;
	float z;
};

#define LEXLIB_VEC3_ZERO ((struct LexlibVec3){0.0f, 0.0f, 0.0f})

LEXLIB_INLINE void lexlibVec3Add(struct LexlibVec3 *result, const struct LexlibVec3 *a, const struct LexlibVec3 *b){
	result->x = a->x + b->x;
	result->y = a->y + b->y;
	result->z = a->z + b->z;
}

LEXLIB_INLINE void lexlibVec3Sub(struct LexlibVec3 *result, const struct LexlibVec3 *a, const struct LexlibVec3 *b){
	result->x = a->x - b->x;
	result->y = a->y - b->y;
	result->z = a->z - b->z;
}

LEXLIB_INLINE void lexlibVec3Mul(struct LexlibVec3 *result, const struct LexlibVec3 *a, const struct LexlibVec3 *b){
	result->x = a->x * b->x;
	result->y = a->y * b->y;
	result->z = a->z * b->z;
}

LEXLIB_INLINE void lexlibVec3Div(struct LexlibVec3 *result, const struct LexlibVec3 *a, const struct LexlibVec3 *b){
	result->x = a->x / b->x;
	result->y = a->y / b->y;
	result->z = a->z / b->z;
}

LEXLIB_INLINE void lexlibVec3Adds(struct LexlibVec3 *result, const struct LexlibVec3 *a, float b){
	result->x = a->x + b;
	result->y = a->y + b;
	result->z = a->z + b;
}

LEXLIB_INLINE void lexlibVec3Subs(struct LexlibVec3 *result, const struct LexlibVec3 *a, float b){
	result->x = a->x - b;
	result->y = a->y - b;
	result->z = a->z - b;
}

LEXLIB_INLINE void lexlibVec3Muls(struct LexlibVec3 *result, const struct LexlibVec3 *a, float b){
	result->x = a->x * b;
	result->y = a->y * b;
	result->z = a->z * b;
}

LEXLIB_INLINE void lexlibVec3Divs(struct LexlibVec3 *result, const struct LexlibVec3 *a, float b){
	result->x = a->x / b;
	result->y = a->y / b;
	result->z = a->z / b;
}

LEXLIB_INLINE void lexlibVec3Scale(struct LexlibVec3 *result, const struct LexlibVec3 *a, float scalar){
	result->x = a->x * scalar;
	result->y = a->y * scalar;
	result->z = a->z * scalar;
}

LEXLIB_INLINE float lexlibVec3Dot(const struct LexlibVec3 *a, const struct LexlibVec3 *b){
	return a->x * b->x + a->y * b->y + a->z * b->z;
}

LEXLIB_INLINE float lexlibVec3Mag(const struct LexlibVec3 *vec){
	return sqrtf(vec->x * vec->x + vec->y * vec->y + vec->z * vec->z);
}

LEXLIB_INLINE void lexlibVec3Norm(struct LexlibVec3 *result, const struct LexlibVec3 *vec){
	float mag = lexlibVec3Mag(vec);
	if(mag == 0.0f){
		result->x = 0.0f;
		result->y = 0.0f;
		result->z = 0.0f;
		return;
	}
	lexlibVec3Divs(result, vec, mag);
}

LEXLIB_INLINE void lexlibVec3Neg(struct LexlibVec3 *result, const struct LexlibVec3 *vec){
	result->x = -vec->x;
	result->y = -vec->y;
	result->z = -vec->z;
}

#endif
