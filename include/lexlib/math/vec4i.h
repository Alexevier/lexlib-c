// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_vec4i_h
#define lexlib_math_vec4i_h

#include"../common.h"

struct LexlibVec4i {
	int x;
	int y;
	int z;
	int w;
};

#define LEXLIB_VEC4I_ZERO ((struct LexlibVec4i){0, 0, 0, 0})

LEXLIB_INLINE void lexlibVec4iAdd(struct LexlibVec4i *res, const struct LexlibVec4i *a, const struct LexlibVec4i *b){
	res->x = a->x + b->x;
	res->y = a->y + b->y;
	res->z = a->z + b->z;
	res->w = a->w + b->w;
}

LEXLIB_INLINE void lexlibVec4iSub(struct LexlibVec4i *res, const struct LexlibVec4i *a, const struct LexlibVec4i *b){
	res->x = a->x - b->x;
	res->y = a->y - b->y;
	res->z = a->z - b->z;
	res->w = a->w - b->w;
}

LEXLIB_INLINE void lexlibVec4iMul(struct LexlibVec4i *res, const struct LexlibVec4i *a, const struct LexlibVec4i *b){
	res->x = a->x * b->x;
	res->y = a->y * b->y;
	res->z = a->z * b->z;
	res->w = a->w * b->w;
}

LEXLIB_INLINE void lexlibVec4iDiv(struct LexlibVec4i *res, const struct LexlibVec4i *a, const struct LexlibVec4i *b){
	res->x = a->x / b->x;
	res->y = a->y / b->y;
	res->z = a->z / b->z;
	res->w = a->w / b->w;
}

LEXLIB_INLINE void lexlibVec4iAdds(struct LexlibVec4i *res, const struct LexlibVec4i *a, int s){
	res->x = a->x + s;
	res->y = a->y + s;
	res->z = a->z + s;
	res->w = a->w + s;
}

LEXLIB_INLINE void lexlibVec4iSubs(struct LexlibVec4i *res, const struct LexlibVec4i *a, int s){
	res->x = a->x - s;
	res->y = a->y - s;
	res->z = a->z - s;
	res->w = a->w - s;
}

LEXLIB_INLINE void lexlibVec4iMuls(struct LexlibVec4i *res, const struct LexlibVec4i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
	res->z = a->z * s;
	res->w = a->w * s;
}

LEXLIB_INLINE void lexlibVec4iDivs(struct LexlibVec4i *res, const struct LexlibVec4i *a, int s){
	res->x = a->x / s;
	res->y = a->y / s;
	res->z = a->z / s;
	res->w = a->w / s;
}

LEXLIB_INLINE void lexlibVec4iScale(struct LexlibVec4i *res, const struct LexlibVec4i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
	res->z = a->z * s;
	res->w = a->w * s;
}

LEXLIB_INLINE int lexlibVec4iDot(const struct LexlibVec4i *a, const struct LexlibVec4i *b){
	return a->x * b->x + a->y * b->y + a->z * b->z + a->w * b->w;
}

LEXLIB_INLINE void lexlibVec4iNegate(struct LexlibVec4i *res, const struct LexlibVec4i *vec){
	res->x = -vec->x;
	res->y = -vec->y;
	res->z = -vec->z;
	res->w = -vec->w;
}

#endif
