// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_math_vec3i_h
#define lexlib_math_vec3i_h

#include"../common.h"

struct LexlibVec3i {
	int x;
	int y;
	int z;
};

#define LEXLIB_VEC3I_ZERO ((struct LexlibVec3i){0, 0, 0})

LEXLIB_INLINE void lexlibVec3iAdd(struct LexlibVec3i *res, const struct LexlibVec3i *a, const struct LexlibVec3i *b){
	res->x = a->x + b->x;
	res->y = a->y + b->y;
	res->z = a->z + b->z;
}

LEXLIB_INLINE void lexlibVec3iSub(struct LexlibVec3i *res, const struct LexlibVec3i *a, const struct LexlibVec3i *b){
	res->x = a->x - b->x;
	res->y = a->y - b->y;
	res->z = a->z - b->z;
}

LEXLIB_INLINE void lexlibVec3iMul(struct LexlibVec3i *res, const struct LexlibVec3i *a, const struct LexlibVec3i *b){
	res->x = a->x * b->x;
	res->y = a->y * b->y;
	res->z = a->z * b->z;
}

LEXLIB_INLINE void lexlibVec3iDiv(struct LexlibVec3i *res, const struct LexlibVec3i *a, const struct LexlibVec3i *b){
	res->x = a->x / b->x;
	res->y = a->y / b->y;
	res->z = a->z / b->z;
}

LEXLIB_INLINE void lexlibVec3iAdds(struct LexlibVec3i *res, const struct LexlibVec3i *a, int s){
	res->x = a->x + s;
	res->y = a->y + s;
	res->z = a->z + s;
}

LEXLIB_INLINE void lexlibVec3iSubs(struct LexlibVec3i *res, const struct LexlibVec3i *a, int s){
	res->x = a->x - s;
	res->y = a->y - s;
	res->z = a->z - s;
}

LEXLIB_INLINE void lexlibVec3iMuls(struct LexlibVec3i *res, const struct LexlibVec3i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
	res->z = a->z * s;
}

LEXLIB_INLINE void lexlibVec3iDivs(struct LexlibVec3i *res, const struct LexlibVec3i *a, int s){
	res->x = a->x / s;
	res->y = a->y / s;
	res->z = a->z / s;
}

LEXLIB_INLINE void lexlibVec3iScale(struct LexlibVec3i *res, const struct LexlibVec3i *a, int s){
	res->x = a->x * s;
	res->y = a->y * s;
	res->z = a->z * s;
}

LEXLIB_INLINE int lexlibVec3iDot(const struct LexlibVec3i *a, const struct LexlibVec3i *b){
	return a->x * b->x + a->y * b->y + a->z * b->z;
}

LEXLIB_INLINE void lexlibVec3iNegate(struct LexlibVec3i *res, const struct LexlibVec3i *vec){
	res->x = -vec->x;
	res->y = -vec->y;
	res->z = -vec->z;
}

#endif
