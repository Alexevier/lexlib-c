/* Copyright 2023 alexevier <alexevier@proton.me>
	licensed under the zlib license <https://www.zlib.net/zlib_license.html> */

#ifndef lexlib_char_h
#define lexlib_char_h

/* UTF-8 chars
	support for utf-8 characters
*/

#include<stdint.h>
#include"common.h"

/* utf-8 character. */
typedef uint32_t LexlibChar;
/* null char. */
static const LexlibChar LEXLIB_CHAR_NULL = 0x00000000;
/* a char representing the end-of-file */
static const LexlibChar LEXLIB_CHAR_EOF = 0xFFFFFFFF;

/* create a new LexlibChar. */
LEXLIBFN LexlibChar lexlibCharNew(const char chr[]);

/* creates a valid utf-8 char from a ascii char. */
LEXLIBFN LexlibChar lexlibCharFromAscii(char ascii);

/* creates a valid utf-8 char from raw bytes
	returns 0 on error */
LEXLIBFN LexlibChar lexlibCharFromBytes(uint8_t byte1, uint8_t byte2, uint8_t byte3, uint8_t byte4);

/* creates a valid utf-8 char from raw memory;
	reads only the necessary bytes.
	returns 0 on error */
LEXLIBFN LexlibChar lexlibCharFromMem(const void *mem);

/* convert a LexlibChar to ascii.
	returns 0 if the char can't be converted to ascii. */
LEXLIBFN char lexlibCharIntoAscii(LexlibChar chr);

/* writes the bytes of the Char into bytes[4] */
LEXLIBFN void lexlibCharIntoBytes(LexlibChar chr, uint8_t bytes[4]);

/* check if the Char is ascii. */
LEXLIBFN LexlibBool lexlibCharIsAscii(LexlibChar chr);

/* get how many bytes the char requires. 1-4 */
LEXLIBFN uint8_t lexlibCharSize(LexlibChar chr);

/* check if a LexlibChar is valid.
	returns false if not. */
LEXLIBFN LexlibBool lexlibCharValid(LexlibChar chr);

#endif
