// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_mem_h
#define lexlib_mem_h

#include<stdint.h>
#include<stddef.h>
#include"common.h"

// unmaps memory mapped by lexlib; used for mapped files.
LEXLIBFN void lexlibMemUnmap(void* mem, size_t size);

/* compare two memory buffers.
	returns true (1) if they are equal, false (0) otherwise. */
LEXLIBFN LexlibBool lexlibMemCompare(const void *LEXLIB_RESTRICT mem1, const void *LEXLIB_RESTRICT mem2, size_t size);

// reverses the byte order of a 16bit type.
LEXLIB_INLINE uint16_t lexlibMemReverse16(uint16_t val){
	return
		((val & 0x00FF) << 8) |
		((val & 0xFF00) >> 8);
}

// reverses the byte order of a 32bit type.
LEXLIB_INLINE uint32_t lexlibMemReverse32(uint32_t val){
	return
		((val & 0x000000FF) << 24) |
		((val & 0x0000FF00) << 8 ) |
		((val & 0x00FF0000) >> 8 ) |
		((val & 0xFF000000) >> 24);
}

// reverses the byte order of a 64bit type.
LEXLIB_INLINE uint64_t lexlibMemReverse64(uint64_t val){
	return
		((val & 0x00000000000000FF) << 56) |
		((val & 0x000000000000FF00) << 40) |
		((val & 0x0000000000FF0000) << 24) |
		((val & 0x00000000FF000000) << 8 ) |
		((val & 0x000000FF00000000) >> 8 ) |
		((val & 0x0000FF0000000000) >> 24) |
		((val & 0x00FF000000000000) >> 40) |
		((val & 0xFF00000000000000) >> 56);
}

#endif
