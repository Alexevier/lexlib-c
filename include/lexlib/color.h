// Copyright 2023 alexevier <alexevier@proton.me>
// licensed under the zlib license <https://www.zlib.net/zlib_license.html>

#ifndef lexlib_color_h
#define lexlib_color_h

#include<stdint.h>
#include"common.h"

/* NOTE s
	lexlib color expects unmultiplied colors.
	support for LexlibColor16 is more limited since is not used that often.

> blending
	for blend modes that exist in SDL2 and lexlib the same behavior can be expected.

> blend modes
	LEXLIB_NONE: res.rgba = src.rgba
	LEXLIB_ADD : res.rgb = dst.rgb + (src.rgb * src.a) ; res.a = dst.a
	LEXLIB_SUB : res.rgb = dst.rgb - (src.rgb * src.a) ; res.a = dst.a
	LEXLIB_MUL : res.rgb = (src.rgb * dst.rgb) + (dst.rgb * (1 - src.a)) ; res.a = (src.a * dst.a) + (dst.a * (1 - src.a))
	LEXLIB_MOD : res.rgb = src.rgb * dst.rgb; res.a = dst.a
	LEXLIB_BLEND : res.rgb = (src.rgb * src.a) + (dst.rgb * (1 - src.a)); res.a = dst.a * (1 - src.a)

> alpha
	for operations where is not desired for the alpha channel to take a effect it
	should be set to the max of that color, constants are available.

> floating point color
	all lexlib color operations with a color that is represented as float
	is expected to be clamped to the range 0..1 and all colors returned are
	arleady clamped.
*/

// 8 bits per color
struct LexlibColor {
	uint8_t r;
	uint8_t g;
	uint8_t b;
	uint8_t a;
};

// floating point color
struct LexlibColorF {
	float r;
	float g;
	float b;
	float a;
};

// 16 bits per color
struct LexlibColor16 {
	uint16_t r;
	uint16_t g;
	uint16_t b;
	uint16_t a;
};

enum LexlibColorProfile {
	LEXLIB_GRAY = 0x01u,
	LEXLIB_GRAYA,
	LEXLIB_RGB,
	LEXLIB_RGBA,
	LEXLIB_RGB555,
	LEXLIB_RGB565,
};

// BlendModes
#define LEXLIB_NONE  0x00
#define LEXLIB_ADD   0x01u
#define LEXLIB_SUB   0x02u
#define LEXLIB_MUL   0x04u
#define LEXLIB_DIV   0x08u
#define LEXLIB_MOD   0x10u
#define LEXLIB_BLEND 0x80u

// color limits
#define LEXLIB_COLOR_MIN 0x00u
#define LEXLIB_COLOR_MAX 0xFFu
#define LEXLIB_COLORF_MIN 0.0f
#define LEXLIB_COLORF_MAX 1.0f
#define LEXLIB_COLOR16_MIN 0x0000u
#define LEXLIB_COLOR16_MAX 0xFFFFu

// performs a color blend depending on the mode.
// if mode is invalid LEXLIB_ADD is performed.
LEXLIBFN struct LexlibColor lexlibColorBlend(struct LexlibColor dst, struct LexlibColor src, uint8_t mode);
LEXLIBFN struct LexlibColorF lexlibColorFBlend(struct LexlibColorF dst, struct LexlibColorF src, uint8_t mode);
LEXLIBFN struct LexlibColor16 lexlibColor16Blend(struct LexlibColor16 dst, struct LexlibColor16 src, uint8_t mode);

// convert to gray scale, premultiplies the alpha.
LEXLIBFN uint8_t lexlibColorGray(struct LexlibColor color);
LEXLIBFN float   lexlibColorFGray(struct LexlibColorF color);
LEXLIBFN uint16_t lexlibColor16Gray(struct LexlibColor16 color);

// convert to gray scale preserves the alpha.
LEXLIBFN struct LexlibColor lexlibColorGrayAlpha(struct LexlibColor color);
LEXLIBFN struct LexlibColorF lexlibColorFGrayAlpha(struct LexlibColorF color);
LEXLIBFN struct LexlibColor16 lexlibColor16GrayAlpha(struct LexlibColor16 color);

// premultiply alpha.
LEXLIBFN struct LexlibColor lexlibColorPremultiply(struct LexlibColor color);
LEXLIBFN struct LexlibColorF lexlibColorFPremultiply(struct LexlibColorF color);
LEXLIBFN struct LexlibColor16 lexlibColor16Premultiply(struct LexlibColor16 color);

// clamps a LexlibColorF to be in the range  0..1
LEXLIBFN struct LexlibColorF lexlibColorFClamp(struct LexlibColorF color);

// convertions from Color
LEXLIBFN struct LexlibColorF lexlibColorToF(struct LexlibColor color);
LEXLIBFN struct LexlibColor16 lexlibColorTo16(struct LexlibColor color);

// convertions from ColorF
LEXLIBFN struct LexlibColor lexlibColorFTo8(struct LexlibColorF color);
LEXLIBFN struct LexlibColor16 lexlibColorFTo16(struct LexlibColorF color);

// convertions from Color16
LEXLIBFN struct LexlibColor lexlibColor16To8(struct LexlibColor16 color);
LEXLIBFN struct LexlibColorF lexlibColor16ToF(struct LexlibColor16 color);

// some constants
#define LEXLIB_COLOR_BLACK   ((struct LexlibColor){0x00, 0x00, 0x00, 0xFF})
#define LEXLIB_COLOR_WHITE   ((struct LexlibColor){0xFF, 0xFF, 0xFF, 0xFF})
#define LEXLIB_COLOR_RED     ((struct LexlibColor){0xFF, 0x00, 0x00, 0xFF})
#define LEXLIB_COLOR_GREEN   ((struct LexlibColor){0x00, 0xFF, 0x00, 0xFF})
#define LEXLIB_COLOR_BLUE    ((struct LexlibColor){0x00, 0x00, 0xFF, 0xFF})
#define LEXLIB_COLOR_YELLOW  ((struct LexlibColor){0xFF, 0xFF, 0x00, 0xFF})
#define LEXLIB_COLOR_MAGENTA ((struct LexlibColor){0xFF, 0x00, 0xFF, 0xFF})
#define LEXLIB_COLOR_CYAN    ((struct LexlibColor){0x00, 0xFF, 0xFF, 0xFF})

#endif
