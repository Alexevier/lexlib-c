# LexLib

a**Lex**evier's **Lib**rary<br>
is a simple C/Rust library with stuff libc is missing and other useful things without breaking compatibility with standard C or Rust.

also works on C++ without hassle.

## features
+ os abstractions for linux and windows.
+ abstract i/o.
+ utf-8 strings.
+ raw C strings manipulation.
+ image manipulation.
+ color manipulation.
+ simd vector math.
+ small math things.
+ time stuff.
+ vector (growable array).

# Rust

**IMPORTANT** rust binding development is on halt.

for C functions that have a equivalent or better options in the Rust stdlib are not expected to be wrapped around a safe interface.<br>

some features (like vector math) are not available yet in the rust side.

# Documentation

C: in the header files.

Rust: https://docs.rs/lexlib/latest/lexlib/

# Dependencies

+ libc
+ win32api (kernel32)

# License
Licensed under the zlib license.

# Changelog

## 2.2.0 : the IO & UTF8 update
+ added io.h.
+ added char.h.
+ added string.h.
+ opaque type LexlibIO.
+ opaque type LexlibString.
+ type LexlibChar.
+ deprecated lexlibImageLoadBmp().
+ deprecated lexlibImageLoadBmpMem().
+ changed lexlibImageSaveBmp().
+ changed lexlibImageLoadBmp().
+ removed lexlibImageSaveBmpEx().
+ removed lexlibImageLoadBmpMem().
+ lexlibVersion().
+ lexlibIOfile().
+ lexlibIOmem().
+ lexlibIOmemconst().
+ lexlibIOread().
+ lexlibIOwrite().
+ lexlibIOreadLine().
+ lexlibIOreadLineStr().
+ lexlibIOsetpos().
+ lexlibIOgetpos().
+ lexlibIOflush().
+ lexlibIOclose().
+ lexlibCharNew().
+ lexlibCharFromAscii().
+ lexlibCharFromBytes().
+ lexlibCharIntoAscii().
+ lexlibCharIntoBytes().
+ lexlibCharIsAscii().
+ lexlibCharSize().
+ lexlibCharValid().
+ lexlibStringNew().
+ lexlibStringDelete().
+ lexlibStringCopy().
+ lexlibStringFromC().
+ lexlibStringFromVec().
+ lexlibStringChar().
+ lexlibStringAppend().
+ lexlibStringAppendStr().
+ lexlibStringCompare().
+ lexlibStringCompareStr().
+ lexlibStringLen().
+ lexlibStringSize().
+ lexlibStringValid().
+ lexlibStringIsAscii().
+ lexlibStringRaw().
+ lexlibStringRawMut().
+ lexlibMemCompare().
+ lexlibPathExtension().

## 2.1.0
+ struct LexlibRect.
+ lexlibPathNew().
+ lexlibPathAdd().
+ lexlibPathPop().
+ lexlibPathAbsolute().
+ lexlibPathAsDir().
+ lexlibPathAsFile().
+ lexlibStrCompare().
+ deprecated lexlibStrPathNew().
+ deprecated lexlibStrPathPush().
+ deprecated lexlibStrPathPop().
+ deprecated lexlibStrPathAsDir().
+ deprecated lexlibStrPathAsFile().

## 2.0.1
+ fix LEXLIB_VEC2I/3I_ZERO.
+ fix cfile mode in windows.
+ LexlibVec(T).
+ lexlibStrNew().

## 2.0.0
+ changed license to zlib.
+ rust bindings and wrappers.
+ gray_scale with alpha support.
+ structs are not typedef'd anymore.
+ SIMD support.
+ reworked Vec4 math.
+ reworked image loading.
+ LexlibColorFlt -> LexlibColorF.
+ LexlibImage.depth -> LexlibImage.bpc.
+ lexlibImagePixelGet() -> lexlibImagePixel().
+ lexlibImagePixel16Get() -> lexlibImagePixel16().
+ lexlibMillis() -> lexlibTimeMillis().
+ lexlibSeconds() -> lexlibTimeSeconds().
+ lexlibStrcat() -> lexlibStrCat().
+ added typedef.h.
+ added vec.h (growable array).
+ removed path.
+ removed aabb.
+ removed LexlibBytes.
+ removed lexlibFileToString().
+ removed lexlibStringToFile().
+ removed lexlibFileBytes().
+ removed lexlibPrintTimed().
+ removed lexlibVersion().
+ removed LEXLIB_UNREACHABLE.
+ LexlibBool.
+ lexlibStrFile().
+ LEXLIB_STRING().
+ lexlibCFileMap().
+ lexlibCFileMode().
+ lexlibCFileSize().
+ lexlibCFileType().
+ lexlibColorGrayAlpha().
+ lexlibTimeNanos().
+ lexlibTimeMicros().
+ lexlibThrdNanos().
+ lexlibThrdMicros().
+ lexlibThrdMillis().
+ lexlibThrdSeconds().
+ lexlibProcNanos().
+ lexlibProcMicros().
+ lexlibProcMillis().
+ lexlibProcSeconds().
+ lexlibTimerAdd().
+ LEXLIB_CLAMP().
+ lexlibRad().
+ lexlibDeg():
+ lexlibFloatCmp().
+ lexlibMemUnmap().
+ lexlibMemReverse16().
+ lexlibMemReverse32().
+ lexlibMemReverse64().
+ lexlibImageCopy().
+ lexlibImageFillArea().
+ lexlibImageLoadBmpMem().
+ lexlibVecNew().
+ lexlibVecDelete().
+ lexlibVecGet().
+ lexlibVecPush().
+ lexlibVecPop().
+ lexlibVecInsert().
+ lexlibVecRemove().
+ lexlibVecResize().
+ lexlibVecFind().
+ lexlibVecSwap().
+ lexlibVecClear().
+ lexlibVecLen().
+ lexlibVecCap().

## 1.5.0
+ lexlibFileBytes();
+ lexlibPowu();
+ lexlibColor16Blend();
+ lexlibImagePixelSet();
+ lexlibImagePixelGet();
+ lexlibImagePixel16Set();
+ lexlibImagePixel16Get();
+ lexlibImageProfileChange();
+ lexlibImageLoadBmp();
+ lexlibImageSaveBmp();
+ lexlibImageSaveBmpEx();
+ LexlibImage.data is officially native to the machine endianness.
+ fixed strPath buffer overflows.
+ fixed imageFlip X buffer overflow.
+ fixed linkedList memleaks.

## 1.4.0
+ function lexlibStrCopy().
+ removed path.h from default includes.
+ renamed LEXLIB_C_EXTERN to LEXLIB_EXTERN.
+ functions lexlibClamp().
+ struct LexlibColor8.
+ struct LexlibColor16.
+ struct LexlibColorFtl.
+ functions lexlibColorBlend().
+ functions lexlibColorGray().
+ functions lexlibColorPremultiply().
+ functions lexlibColorTo8().
+ functions lexlibColorTo16().
+ functions lexlibColorToFlt().
+ function lexlibColorFltClamp().

## 1.3.1
+ deprecation attribute.
+ removed PKGBUILD from package.
+ fixed a scope problem with clang.

## 1.3.0
+ LexlibImage.
+ support for loading and saving pngs.
+ lexlibStrPath.
+ LexlibPath is deprecated in favor of lexlibStrPath.
+ changed all _Bool for uint8_t.

## For the full Changelog check CHANGELOG.md at the project root.
